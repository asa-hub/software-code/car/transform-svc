FROM azul/zulu-openjdk-alpine:11.0.6
LABEL MAINTAINER kiyanjalabs@Kiyanja.org

RUN mkdir -p /srv/asac/app/hubasac/transform-svc/lib /srv/asac/app/hubasac/transform-svc/scripts /srv/asac/app/hubasac/transform-svc/config \
    mkdir -p /var/log/hubasac

ADD ./scripts* /srv/asac/app/hubasac/transform-svc/scripts
ADD ./targer/config* /srv/asac/app/hubasac/transform-svc/config
ADD ./target/lib* /srv/asac/app/hubasac/transform-svc/lib

#COPY ./lib/postgresql-42.2.26.jre7.jar ./lib/postgresql-42.2.26.jre7.jar
COPY ./target/hubasactransform.properties /srv/asac/app/hubasac/transform-svc/hubasactransform.properties
COPY ./target/validate_hubasac.jar /srv/asac/app/hubasac/transform-svc/validate_hubasac.jar

WORKDIR /srv/asac/app/hubasac/transform-svc

#ENTRYPOINT ["java","-classpath",".:validate_hubasac.jar:./lib/postgresql-42.2.26.jre7.jar","com.vindata.asac.server.ASACEntityValidator","$@"]

CMD ["chmod", "+x", "/srv/asac/app/hubasac/transform-svc/scripts/runTransformer.sh"]

ENTRYPOINT ["/bin/sh","/srv/asac/app/hubasac/transform-svc/scripts/runTransformer.sh"]


