#! /bin/bash
echo ""
echo "Welcome to the sevice  cron job for Asac Hub Data Validator"
echo ""
svcdir="/srv/asac/app/hubasac/transform-svc"
logdir="/var/log/hubasac"
svcname="validateData"
echo ""
#--------please comment the next 20 lines ou if you intend to pass external parameters ------------
#
# jobdir=${1}
# jobname=${2}
# logdir=${3}
#
# if [ ${#} -ne 3 ]; then
#        echo "runHubAsacJob: usage: project_folder job_name log_folder"
#        exit 2
# fi
#----------End of commented lines

echo "`date`: Starting Job $svcname" 1>> ${logdir}/$svcname.log
#$jobdir/$jobname.sh 1>> $logdir/$jobname.log 2>&1
cd $svcdir/scripts
echo "... starting the Asac Hub Data validation service ...."
/bin/sh $svcname.sh 1 | tee $logdir/$svcname.log 2>&1
JobExitStatus=${?}

if [ ${JobExitStatus} -eq 0 ]; then
        echo "`date`: Job $svcdir.$svcname ended successfully" 1>> $logdir/$svcname.log
else
        echo "`date`: Job $jobdir.$jobname ended in error" 1>> $logdir/$jobname.log
fi
exit ${JobExitStatus}
