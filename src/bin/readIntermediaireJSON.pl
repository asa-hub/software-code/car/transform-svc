#!/usr/bin/perl -w

my $filename = "./db/body_intermediaire.txt";
use lib qw(..);

use JSON qw();

my $retval=`export PGPASSWORD='postgres'`;
my $dbname="hubasac";

my $json_text = do {
	open (my $json_fh, "<:encoding(UTF-8)", $filename) or die("Can't open \"$filename\": $!\n");
	local $/;
	<$json_fh>
};

my $json=JSON->new;
my $data=$json->decode($json_text);

for (@{$data})
{
	my $codeInt = $_->{code_intermediaire_dna};
	my $numCont = $_->{num_contribuable};
	my $typeInt = $_->{type_intermediaire};
	my $nomInt  = $_->{nom_intermediaire};
	my $phone   = $_->{telephone};
	my $ville   = $_->{ville};
	my $bpostal = $_->{boite_postale};
	my $statut  = $_->{statut_intermediaire};
	my $ddebut  = $_->{date_debut_suspension};
	my $dfin    = $_->{date_fin_suspension};

	my $isert = "insert into intermediaire (code_intermediaire_dna, num_contribuable, type_intermediaire, nom_intermediaire, telephone, ville, boite_postale, statut_intermediaire) ";
	$isert .= "values(";
	if (defined($codeInt)) 
	{
		$isert.="\'$codeInt\',";
	}
	else 
	{
		$isert.="\'\',";
	}

	if (defined($numCont)) 
	{
		$isert.="\'$numCont\',";
	}
	else 
	{
		$isert.="\'\',";
	}

	if (defined($typeInt)) 
	{
		$isert.="\'$typeInt\',";
	}
	else 
	{
		$isert.="\'\',";
	}

	if (defined($nomInt)) 
	{
		$isert.="\'$nomInt\',";
	}
	else 
	{
		$isert.="\'\',";
	}

	if (defined($phone)) 
	{
		$isert.="\'$phone\',";
	}
	else 
	{
		$isert.="\'\',";
	}

	if (defined($ville)) 
	{
		$isert.="\'$ville\',";
	}
	else 
	{
		$isert.="\'\',";
	}

	if (defined($postal)) 
	{
		$isert.="\'$postal\',";
	}
	else 
	{
		$isert.="\'\',";
	}

	if (defined($statut)) 
	{
		$isert.="\'$statut\'";
	}
	else 
	{
		$isert.="\'\'";
	}

	$isert.=")";

	my @retval = `psql -h localhost -U postgres -d $dbname -c "$isert"`;
	#print "insert done for [$isert] retval = [$retval]\n";

	if (defined($ddebut))
	{
		my $dateInStrng = "update intermediaire set date_debut_suspension = '$ddebut' where code_intermediaire_dna = '$codeInt'";
		my @dateIns = `psql -h localhost -U postgres -d $dbname -c "$dateInStrng"`;
		print "\t Date debut update [$ddebut] using [$dateInStrng]\n";
	}

	if (defined($dfin))
	{
		my $dateInStrng = "update intermediaire set date_fin_suspension = '$dfin' where code_intermediaire_dna = '$codeInt'";
		my @dateIns = `psql -h localhost -U postgres -d $dbname -c "$dateInStrng"`;
		print "\t Date fin update [$dfin] using [$dateInStrng]\n";
	}

}
