#!/usr/bin/perl -w
#
my $retval=`export PGPASSWORD='postgres'`;

my $dbname="hubasac";

print "Checking for the existence of the code_assureur on the adresse table...\n";
my @tmpArr=`psql -h localhost -U postgres -d $dbname -c "\\d+ adresse;" | grep -i code_assureur`;
my $numEntries = scalar(@tmpArr);
if ($numEntries == 0)
{
	print "NO entries found \n";
	my @retArr = `psql -h localhost -U postgres -d $dbname -c "alter table adresse add code_assureur varchar(128)"`;
	my @stmpArr=`psql -h localhost -U postgres -d $dbname -c "\\d+ adresse;" | grep -i code_assureur`;
	$numEntries = scalar(@stmpArr);
}

my $tel=699029000;
# after verifying/setting the column for code assureur
if ($numEntries > 0)
{
	my @assureurArr=`psql -h localhost -U postgres -d $dbname -c "select aid, code_assureur, nom from ref_assureur order by nom"`;
	foreach $a (@assureurArr)
	{
		chomp($a);
		#print "processing = [$a]\n";
		my ($aid, $div1, $code, $div2, $name) = split(' ', $a);
		if (defined($aid) && length($aid) > 0 && $aid =~ /^\d+$/)
		{
			#print "Processing assureur info for [$name], with code = [$code], aid = [$aid]\n";
			my @cntArr = `psql -h localhost -U postgres -d $dbname -c "select count(aid) from adresse where code_assureur = '$code'"`;
			foreach $c (@cntArr)
			{
				chomp($c);

				# remove the spaces..
				$c =~ s/\s+//;
				if ($c !~ /count/ && $c =~ /^\d+$/)
				{
					print "checking existance of $name : avail = [$c]\n";
					my $navail = int($c);
					# if there is no entry in the adresse table for this code_assureur, then 
					if ( $navail > 0)
					{
						print "Entry for [$name] already exists in the adresse DB \n";
					}
					else
					{
						print "Entry for [$name] DOES NOT exist in the adresse DB. Inserting \n";
						my $isert = "insert into adresse (rue, ville, pays, email, telephone, code_assureur) values('Avenue kennedy', 'Yaounde', 'Cameroun', 'benaloga\@bbanalytics.com', '$tel', '$code')";
						my @irval = `psql -h localhost -U postgres -d $dbname -c "$isert"`;

						$tel += 1;
					}
				}
			}
		}
	}
}
