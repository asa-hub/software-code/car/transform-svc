#!/bin/sh

if [ "${JAVA_HOME}" ]
then
	echo "Using java from $JAVA_HOME"
else
	echo "JAVA_HOME not set ..."
	echo "export JAVA_HOME=/usr/java/j2sdk1 for bash"
	echo "setenv JAVA_HOME /usr/java/j2sdk1. ... for tcsh"
	exit 0
fi 

POSTGRES_JAR=com/vindata/asac
JAVA=${JAVA_HOME}/bin/java

JARFILE=${JAVA_HOME}/lib/dt.jar:hubasac_vindata.jar:${POSTGRES_JAR}/lib/postgresql-42.2.26.jre7.jar

echo "${JAVA} -classpath .:${JARFILE} com.vindata.asac.server.ASACEntityValidator $@"
${JAVA} -classpath .:${JARFILE} com.vindata.asac.server.ASACEntityValidator $@
