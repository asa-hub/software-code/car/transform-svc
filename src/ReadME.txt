This read me explains the system as it functions currently.
It is designed to work alone, reading data from the database,
Qualifying the data based on missing information, and saving
the data state in the database.

The system installs with a main class called ASACEntityValidator, 
which in turn calls all the other classes, Assure, Vehicule, Remorque,
Attestation, Intermediare, Vente and Sinistre for validation.

The ASACEntityValidator currently executes a Thread, that sleeps every 60 secs, 
After initialisation, the thread can be stopped via the stopControl function and
started using the startControl function.

The system was compiled on JAVA 7, and this is required for the system to work

