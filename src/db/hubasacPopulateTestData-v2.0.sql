-------ASSURE------
INSERT INTO assure (code_assure, code_assureur,  qualite, nom, prenom, profession, date_naissance, num_contribuable, ville, rue, boite_postale, telephone, email, 
numero_permis, categorie_permis, date_delivrance, permis_delivre_par, nom_prenom_conducteur, date_naissance_conducteur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('20220202TEST', '101', 'PERSONNE_PHYSIQUE', 'Aloga', 'Bena', 'Ingenieur', '1965-02-06', 'M0123456789J', 'Makenene', 'Av. aloga Ndjock', 'BP 1', '611223344', 
'benaloga@kindingnde.org', '1986PL123','A', '1986-02-06', 'Stadt Bayern', 'Bena Aloga','1975-02-06', 0, now(), null, null, '' );

INSERT INTO assure (code_assure, code_assureur,  qualite, nom, prenom, profession, date_naissance, num_contribuable, ville, rue, boite_postale, telephone, email, 
numero_permis, categorie_permis, date_delivrance, permis_delivre_par, nom_prenom_conducteur, date_naissance_conducteur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('20220203TEST', '102', 'PERSONNE_MORALE', 'Kiyanja', '', 'SARL', '2020-02-06', 'M0123456799J', 'Yaounde', 'Bastos', 'BP 2', '693236236', 
'contact@kinyanja.org', '1986PL223','C', '1986-02-06', 'MINISTERE DES TRANSPORTS', 'Jean Temga','1975-02-06', 0, now(), null, null, '' );

INSERT INTO assure (code_assure, code_assureur,  qualite, nom, prenom, profession, date_naissance, num_contribuable, ville, rue, boite_postale, telephone, email, 
numero_permis, categorie_permis, date_delivrance, permis_delivre_par, nom_prenom_conducteur, date_naissance_conducteur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('20220204TEST', '103', 'PERSONNE_PHYSIQUE', 'Atchom', 'Eric', 'Medecin', '1985-02-06', 'M0123456788J', 'Baham', 'Boulevard Republique', 'BP 3', '611223355', 
'atchom@bbitanalytics.com', '1986PL323','B', '1976-02-06', 'MINISTERE DES TRANSPORTS', 'Teto Jules','1980-02-06', 0, now(), null, null, '' );

INSERT INTO assure (code_assure, code_assureur,  qualite, nom, prenom, profession, date_naissance, num_contribuable, ville, rue, boite_postale, telephone, email, 
numero_permis, categorie_permis, date_delivrance, permis_delivre_par, nom_prenom_conducteur, date_naissance_conducteur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('20220205TEST', '104', 'PERSONNE_PHYSIQUE', 'Mouafo', 'Jean', 'Consultant', '1992-02-06', 'M0123456788K', 'Bandjoun', 'Boulevard Saint Michel', 'BP 4', '611223356', 
'mouafo@test.com', '1986PL324','B', '1978-02-06', 'MINISTERE DES TRANSPORTS', 'Fonkoua Jacques','1981-02-06', 0, now(), null, null, '' );

INSERT INTO assure (code_assure, code_assureur,  qualite, nom, prenom, profession, date_naissance, num_contribuable, ville, rue, boite_postale, telephone, email, 
numero_permis, categorie_permis, date_delivrance, permis_delivre_par, nom_prenom_conducteur, date_naissance_conducteur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('20220206TEST', '105', 'PERSONNE_PHYSIQUE', 'Tonga', 'Jordan', 'Boucher', '1992-02-06', 'M0123456789L', 'Bafoussam', 'Boulevard Saint Marc', 'BP 5', '611223357', 
'mouafo@test1.com', '1986PL325','B', '1978-02-06', 'MINISTERE DES TRANSPORTS', 'Fonkoua Jules','1981-02-06', 0, now(), null, null, '' );

INSERT INTO assure (code_assure, code_assureur,  qualite, nom, prenom, profession, date_naissance, num_contribuable, ville, rue, boite_postale, telephone, email, 
numero_permis, categorie_permis, date_delivrance, permis_delivre_par, nom_prenom_conducteur, date_naissance_conducteur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('20220207TEST', '106', 'PERSONNE_PHYSIQUE', 'KIMANA', 'Ferry', 'Danseuse', '1993-02-06', 'M0123456789P', 'Bamenda', 'Boulevard Sainte Marthe', 'BP 6', '611223358', 
'mouafo@test2.com', '1986PL326','B', '1979-02-06', 'MINISTERE DES TRANSPORTS', 'Fonkoua Jeanne','1982-02-06', 0, now(), null, null, '' );


-----VEHICULES------
INSERT INTO vehicule (code_assure, code_assureur, marque, modele,  date_premiere_mise_circulation, double_commande, immatriculation, chassis, usage, charge_utile, 
puissance_fiscale, remorque, nombre_portes, immatriculation_remorque, source_energie, nombre_de_places, cylindree, 
poids_total_autorise_en_charge, responsabilite_civile, type_engin, utilitaire,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES('20220202TEST', '101', 'MAZDA', 'CX-7', '2011-06-02', '0', 'CE714LU', 'CH1234567S', 'personel', '1.8T', '10', '0', '4', '', 'diesel', '5', '6', '2','0', '0', '0',0, now(), null, null, '');


INSERT INTO vehicule (code_assure, code_assureur, marque, modele,  date_premiere_mise_circulation, double_commande, immatriculation, chassis, usage, charge_utile, 
puissance_fiscale, remorque, nombre_portes, immatriculation_remorque, source_energie, nombre_de_places, cylindree, 
poids_total_autorise_en_charge, responsabilite_civile, type_engin, utilitaire,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES('20220203TEST', '102', 'MAZDA', 'CX-5', '2011-06-02', '0', 'CE715LU', 'CH1234568S', 'personel', '1.8T', '10', '0', '4', '', 'essence', '5', '6', '2','0', '0', '0',0, now(), null, null, '');


INSERT INTO vehicule (code_assure, code_assureur, marque, modele,  date_premiere_mise_circulation, double_commande, immatriculation, chassis, usage, charge_utile, 
puissance_fiscale, remorque, nombre_portes, immatriculation_remorque, source_energie, nombre_de_places, cylindree, 
poids_total_autorise_en_charge, responsabilite_civile, type_engin, utilitaire,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES('20220204TEST', '103', 'MAZDA', 'CX-9', '2011-06-02', '0', 'CE716LU', 'CH1234569S', 'personel', '1.8T', '10', '0', '4', '', 'diesel', '5', '6', '2','0', '0', '0',0, now(), null, null, '');


-----ATTESTATION----
INSERT INTO attestation (numero_attestation, numero_police, date_emission, date_effet, date_echeance, couleur, statut, zone_circulation, immatriculation, code_assure, code_assureur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, remorque, commentaires)
VALUES('101*A*00500', 'POL1234', '2022-09-14', '2022-09-14', '2023-09-14', 'rouge', 'vendu', 'A', 'CE714LU', '20220202TEST', '101', 0, now(), null, null, 'REM123', '');

INSERT INTO attestation (numero_attestation, numero_police, date_emission, date_effet, date_echeance, couleur, statut, zone_circulation, immatriculation, code_assure, code_assureur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, remorque, commentaires)
VALUES('102*C*00501', 'POL1235', '2022-09-15', '2022-09-15', '2023-09-15', 'bleu', 'vendu', 'B', 'CE715LU', '20220203TEST', '102', 0, now(), null, null, 'REM456', '');

INSERT INTO attestation (numero_attestation, numero_police, date_emission, date_effet, date_echeance, couleur, statut, zone_circulation, immatriculation, code_assure, code_assureur, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, remorque, commentaires)
VALUES('103*B*00502', 'POL1236', '2022-09-16', '2022-09-16', '2023-09-16', 'noir', 'vendu', 'C', 'CE716LU', '20220204TEST', '103', 0, now(), null, null, 'REM789', '');


------REMORQUE----
INSERT INTO remorque (code_assure, code_assureur, immatriculation_remorque, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) VALUES ('20220202TEST', '101', 'NW714LU', 0, now(), null, null, '');

INSERT INTO remorque (code_assure, code_assureur, immatriculation_remorque, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) VALUES ('20220203TEST', '102', 'NW713LU', 0, now(), null, null, '');

INSERT INTO remorque (code_assure, code_assureur, immatriculation_remorque, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) VALUES ('20220204TEST', '103', 'NW712LU', 0, now(), null, null, '');


-----VENTE-----
INSERT INTO vente (numero_attestation, immatriculation, code_assure, code_assureur, code_intermediaire_dna, prime_nette_rc, dta, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES ('101*A*00500', 'CE714LU', '20220202TEST', '101', null,'100000', '10000', 0, now(), null, null, '');

INSERT INTO vente (numero_attestation, immatriculation, code_assure, code_assureur, code_intermediaire_dna, prime_nette_rc, dta, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES ('102*C*00501', 'CE715LU', '20220203TEST', '102', null,'200000', '20000', 0, now(), null, null, '');

INSERT INTO vente (numero_attestation, immatriculation, code_assure, code_assureur, code_intermediaire_dna, prime_nette_rc, dta, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES ('103*B*00502', 'CE716LU', '20220204TEST', '103', null,'300000', '30000', 0, now(), null, null, '');


-----INTERMEDIAIRE-------
INSERT INTO intermediaire (code_intermediaire_dna, num_contribuable, type_assureur, nom_intermediaire, telephone, ville, boite_postale,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('AGC123456', 'M0123456788K', 'courtier', 'NYOKASS', '+23761122335', 'MAKENENE', 'P.O.BOX1234', 0, now(), null, null, '');

INSERT INTO intermediaire (code_intermediaire_dna, num_contribuable, type_assureur, nom_intermediaire, telephone, ville, boite_postale,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('INT12346', 'N01234567891J', 'agent', 'NYOKASS2', '+23761122336', 'KINDING NDE', 'P.O.BOX1235', 0, now(), null, null, '');

INSERT INTO intermediaire (code_intermediaire_dna, num_contribuable, type_assureur, nom_intermediaire, telephone, ville, boite_postale,  c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires)
VALUES('INT12347', 'N01234567892J', 'courtier', 'NYOKASS3', '+23761122337', 'NYOKON', 'P.O.BOX1236', 0, now(), null, null, '');


-----SINISTRE-----
INSERT INTO sinistre (code_assureur, reference, type_dommage, cause_sinistre, numero_attestation, date_survenance, date_declaration, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES ('101','REF1', 'parbrise', 'vol', '101*A*00500', '2022-09-09','2022-09-12', 0, now(), null, null, '');

INSERT INTO sinistre (code_assureur, reference, type_dommage, cause_sinistre, numero_attestation, date_survenance, date_declaration, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES ('102','REF2', 'accident', 'domage', '102*C*00501', '2022-09-19','2022-09-22', 0, now(), null, null, '');

INSERT INTO sinistre (code_assureur, reference, type_dommage, cause_sinistre, numero_attestation, date_survenance, date_declaration, c_status, c_date_creation, c_date_mis_a_jour, c_date_transfer, commentaires) 
VALUES ('103','REF3', 'dommage total', 'arrache', '103*B*00502', '2022-09-29','2022-09-02', 0, now(), null, null, '');
