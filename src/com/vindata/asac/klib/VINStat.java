package com.vindata.asac.klib;

import java.awt.Color;
import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class VINStat 
{
	private static TimeZone serverTZ;
	private static String destination = "";
	private static String current_user;

    public static final int SUCCESS 					= 0;
    public static final int FAILURE 					= -100;

    public static final int TABLE_ENTRY_AVAILABLE 		= -101;
    public static final int INVALID_ID 					= -102;
    public static final int TABLE_ENTRY_NOT_AVAILABLE 	= -103;
    public static final int APPOINTMENT_OVERLAP 		= -104;
    public static final int MEDICAL_RECORDS_AVAILABLE 	= -105;
    public static final int TABLE_ENTRY_REVERTED 		= -106;
    public static final int UNDERSTOCK_ERROR 			= -107;
    public static final int EXPIRED 					= -108;
    public static final int OPERATION_DENIED 			= -109;
	public static final int INVALID_GROUP				= -110;
	public static final int WRONG_GROUP					= -111;
	public static final int INVALID_SHIFT				= -112;
	public static final int CIP_NUMBER_AVAILABLE		= -113;
	public static final int SALE_INVALID				= -114;

    public static ResourceBundle RBundle = null;

    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE d MMM yyyy H:mm:ss a");
    public static SimpleDateFormat simpleFRDateFormat = new SimpleDateFormat("EEE d MMM yyyy H:mm:ss a", Locale.FRANCE);
    public static SimpleDateFormat simpleDayFormat = new SimpleDateFormat("EEE d MMM yyyy", Locale.FRANCE);
    public static SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("H:mm:ss a");
    public static SimpleDateFormat dailyDateFormat = new SimpleDateFormat("EEE MMM d, yyyy");
    public static SimpleDateFormat columnDateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.FRANCE);
    public static SimpleDateFormat licenseDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
    public static SimpleDateFormat appmtDateFormat = new SimpleDateFormat("MMM d, yyyy - H:mm:ss a");
    public static SimpleDateFormat logDateFormat = new SimpleDateFormat("dd-MM-yyyy - H:mm:ss a");
    public static SimpleDateFormat textPrintDateFormat = new SimpleDateFormat("dd/MM/yyyy H:mm a");
    public static SimpleDateFormat rotationDateFormat 	= new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
    public static SimpleDateFormat monthYearDateFormat 	= new SimpleDateFormat("MMM/yyyy");
    public static SimpleDateFormat monthYearDateFormatFr 	= new SimpleDateFormat("MMM/yyyy", Locale.FRANCE);
    public static SimpleDateFormat monthDateFormat 		= new SimpleDateFormat("MMM");
    public static SimpleDateFormat yearDateFormat 		= new SimpleDateFormat("yyyy");
    public static SimpleDateFormat expDateFormat 		= new SimpleDateFormat("MM/yyyy", Locale.FRANCE);
    public static SimpleDateFormat monDateFormat 		= new SimpleDateFormat("MM", Locale.FRANCE);
    public static SimpleDateFormat barcodeFormat 		= new SimpleDateFormat("dd/MM/yy");
    public static SimpleDateFormat fileDateFormat 		= new SimpleDateFormat("dd/MM/yyyy");
    public static SimpleDateFormat commandDateFormat 	= new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat filenameDateFormat   = new SimpleDateFormat("ddMMMyyyy", Locale.US);
    public static SimpleDateFormat referenceDateFormat  = new SimpleDateFormat("ddMMyyyy", Locale.US);

    public static String Email_RegEx = "^[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\\.[a-zA-Z]{2,4}$";
    public static String WEB_Date_Format = "^\\d{1,2}-\\d{1,2}-\\d{4}$";
    public static String non_negative_integer = "(\\d){1,20}";
    public static String non_negative_floating_point = "(\\d){1,20}\\.(\\d){1,20}";
    public static String non_negative_number = "(\\d){1,20}\\(.\\d){1,20}";

	public static String fileDBName = "/srv/hubasac/integration/transform/hubasactransform.properties";

	public static void setServerCalendar(Calendar cal)
	{
		serverTZ = cal.getTimeZone();
	}
	public static Calendar getServerCalendar()
	{
		Calendar cal = new GregorianCalendar();
		if (serverTZ != null)
		{
			cal.setTimeZone(serverTZ);
		}

		return cal;
	}

	public static String remove_quotes(String myStrng)
	{
		String replace_strng = ".";
		String pattern = "'";
		boolean fnd = false;

		Pattern p = Pattern.compile(pattern);	
		Matcher m = p.matcher(myStrng);
		StringBuffer sb = new StringBuffer();

		int pos = 0;	
		while (m.find())
		{
			m.appendReplacement(sb, replace_strng);
		}

		m.appendTail(sb);

		return sb.toString();
	}
	public static String insert_quotes(String drugname)
	{
		String replace_strng = "'";
		String pattern = "\\.";
		boolean fnd = false;

		Pattern p = Pattern.compile(pattern);	
		Matcher m = p.matcher(drugname);
		StringBuffer sb = new StringBuffer();

		int pos = 0;	
		while (m.find())
		{
			m.appendReplacement(sb, replace_strng);
		}

		m.appendTail(sb);

		return sb.toString();
	}
	public static String DIVIDER 									= "&";

	public static String MISSING_ASSURE_CODE 						= "MISSING_ASSURE_CODE";
	public static String MISSING_ASSUREUR_CODE 						= "MISSING_ASSUREUR_CODE";
	public static String MISSING_QUALITE 							= "MISSING_QUALITE";
	public static String MISSING_NAME 								= "MISSING_NOM";
	public static String MISSING_PROFESSION 						= "MISSING_PROFESSION";
	public static String MISSING_NAISSANCE 							= "MISSING_DATE_NAISSANCE";
	public static String MISSING_NUMERO_PERMIS 						= "MISSING_NUMERO_PERMIS";
	public static String MISSING_CATEGORIE_PERMIS 					= "MISSING_CATEGORIE_PERMIS";
	public static String MISSING_DATE_DELIVERANCE 					= "MISSING_DATE_DELIVERANCE";
	public static String MISSING_NOM_CONDUCTEUR 					= "MISSING_NOM_CONDUCTEUR";
	public static String MISSING_NAISSANCE_CONDUCTEUR 				= "MISSING_NAISSANCE_CONDUCTEUR";

	public static String MISSING_NUMERO_CONTRIBUABLE 				= "MISSING_NUMERO_CONTRIBUABLE";
	public static String MISSING_MISE_CIRCULATION 					= "MISSING_PREMIERE_DATE_MISE_EN_CIRCULATION";
	public static String MISSING_IMMATRICULATION 					= "MISSING_IMMATRICULATION";
	public static String MISSING_USAGE 								= "MISSING_USAGE";
	public static String MISSING_PUISSANCE_FISCALE 					= "MISSING_PUISSANCE_FISCALE";
	public static String MISSING_REMORQUE 							= "MISSING_REMORQUE";
	public static String MISSING_IMMATRICULATION_REMORQUE 			= "MISSING_IMMATRICULATION_REMORQUE";
	public static String MISSING_POIDS 								= "MISSING_POIDS";
	public static String MISSING_CHARGE_UTILE 						= "MISSING_CHARGE_UTILE";
	public static String MISSING_NUMERO_ATTESTATION 				= "MISSING_NUMERO_ATTESTATION";
	public static String MISSING_NUMERO_POLICE 						= "MISSING_NUMERO_POLICE";
	public static String MISSING_DATE_EMISSION 						= "MISSING_DATE_EMISSION";
	public static String MISSING_DATE_EFFET 						= "MISSING_DATE_EFFET";
	public static String MISSING_DATE_ECHEANCE 						= "MISSING_DATE_ECHEANCE";
	public static String MISSING_COULEUR 							= "MISSING_COULEUR";
	public static String MISSING_STATUT 							= "MISSING_STATUT";
	public static String MISSING_ZONE_CIRCULATION 					= "MISSING_ZONE_CIRCULATION";
	public static String MISSING_TYPE_ASSUREUR 						= "MISSING_TYPE_ASSUREUR";
	public static String MISSING_IDENTIFIANT_DNA 					= "MISSING_IDENTIFIANT_DNA";
	public static String MISSING_NOM_INTERMEDIAIRE 					= "MISSING_NOM_INTERMEDIAIRE";
	public static String MISSING_TELEPHONE 							= "MISSING_TELEPHONE";
	public static String MISSING_VILLE 								= "MISSING_VILLE";
	public static String MISSING_PRIME_NETTE_RC 					= "MISSING_PRIME_NETTE_RC";
	public static String MISSING_REFERENCE 							= "MISSING_REFERENCE";
	public static String MISSING_TYPE_DOMMAGE 						= "MISSING_TYPE_DOMMAGE";
	public static String MISSING_CAUSE_SINISTRE 					= "MISSING_CAUSE_SINISTRE";
	public static String MISSING_DATE_SURVENANCE 					= "MISSING_DATE_SURVENANCE";
	public static String MISSING_DATE_DECLARATION 					= "MISSING_DATE_DECLARATION";

	public static String MISSING_CHASSIS 							= "MISSING_CHASSIS";
	public static String MISSING_SOURCE_ENERGIE 					= "MISSING_SOURCE_ENERGIE";
	public static String MISSING_NUMBER_PLACES 						= "MISSING_NUMBER_PLACES";
	public static String MISSING_NUMBER_CYLINDER 					= "MISSING_NUMBER_CYLINDER";
	public static String MISSING_DOUBLE_COMMANDES 					= "MISSING_DOUBLE_COMMANDES";
	public static String MISSING_RESPONSABILITE_CIVILE 				= "MISSING_RESPONSABILITE_CIVILE";
	public static String MISSING_UTILITAIRE 						= "MISSING_UTILITAIRE";
	public static String MISSING_TYPE_ENGIN 						= "MISSING_TYPE_ENGIN";

	public static String INVALID_ASSURE_CODE 						= "INVALID_ASSURE_CODE";
	public static String INVALID_ASSUREUR_CODE 						= "INVALID_ASSUREUR_CODE";
	public static String INVALID_IMMATRICULATION 					= "INVALID_IMMATRICULATION";
	public static String INVALID_DATE_SURVENANCE 					= "INVALID_DATE_SURVENANCE";
	public static String INVALID_DATE_DECLARATION 					= "INVALID_DATE_DECLARATION";
	public static String INVALID_DECLARATION_SURVENANCE 			= "INVALID_DECLARATION_AVANT_SURVENANCE";
	public static String INVALID_NUMERO_ATTESTATION 				= "INVALID_NUMERO_ATTESTATION";
	public static String INVALID_INTERMEDIAIRE_CODE 				= "INVALID_INTERMEDIAIRE_CODE";
	public static String INTERMEDIAIRE_STATUS_SUSPENDED 			= "INTERMEDIAIRE_STATUS_SUSPENDED";
	public static String INTERMEDIAIRE_STATUS_CADUC 				= "INTERMEDIAIRE_STATUS_CADUC";
	public static String INTERMEDIAIRE_SALES_STATUS_SUSPENDED 		= "INTERMEDIAIRE_SALES_STATUS_SUSPENDED";
	public static String ATTESTATION_MISSING_VENTE 					= "ATTESTATION_MISSING_VENTE";

	public static String FUTURE_DATE_EMISSION 						= "FUTURE_DATE_EMISSION";
	public static String FUTURE_DATE_EFFET 							= "FUTURE_DATE_EFFET";
	public static String FUTURE_DATE_ECHEANCE 						= "FUTURE_DATE_ECHEANCE";
	public static String FUTURE_DATE_NAISSANCE 						= "FUTURE_DATE_NAISSANCE";

    public static final int CONTROL_STATUS_INITIALIZATION 			= 0;
    public static final int CONTROL_STATUS_PENDING_TRANSFORMATION 	= 1;
    public static final int CONTROL_STATUS_PENDING_TRANSFER 		= 2;
    public static final int CONTROL_STATUS_PENDING_VALIDATION 		= -2;
    public static final int CONTROL_STATUS_MESSAGE_SENT 			= 101;

    public static final int STATUS_INVALID 							= -100;
    public static final int STATUS_VALID 							= 100;

	public static final String STATUT_VENDUE			= "VENDUE";
	public static final String STATUT_EN_STOCK			= "EN_STOCK";
	public static final String STATUT_MISE_AU_REBUS		= "MISE_AU_REBUS";
	public static final String STATUT_ANNULEE			= "ANNULEE";
	public static final String STATUT_RESILIEE			= "RESILIEE";
	public static final String STATUT_SUSPENDUE			= "SUSPENDUE";
	public static final String STATUT_REMISE_EN_VIGUEUR	= "REMISE_EN_VIGUEUR";

	public static final String COULEUR_MARRON			= "MARRON";
	public static final String COULEUR_VERTE			= "VERTE";
	public static final String COULEUR_BLEUE			= "BLEUE";

	public static final String INSURANCE_TYPE_COURTIER			= "COURTIER";
	public static final String INSURANCE_TYPE_AGENT_GENERAL		= "AGENT_GENERAL";
	public static final String INSURANCE_TYPE_BUREAU_DIRECT		= "BUREAU_DIRECT";

	public static final String TRANSFORMATION_ABBREV			= "TRFM";
	public static final String ASSURE_ABBREV					= "ASSU";
	public static final String VEHICLE_ABBREV					= "VEH";
	public static final String VENTE_ABBREV						= "VEN";
	public static final String ATTESTATION_ABBREV				= "ATT";
	public static final String INTERMEDIAIRE_ABBREV				= "INT";
	public static final String REMORQUE_ABBREV					= "REM";
	public static final String SINISTRE_ABBREV					= "SIN";

	public static final String STATUS_VALID_STRING				= "VALID";
	public static final String STATUS_ERROR_STRING				= "ERROR";
	public static final String STATUS_PENDING_STRING			= "PENDING";

}
