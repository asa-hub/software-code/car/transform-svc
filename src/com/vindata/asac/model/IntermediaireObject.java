package com.vindata.asac.model;

import java.util.*;

public class IntermediaireObject 
{
	private int 	iid;
	private String 	code_intermediaire_dna;
	private String 	num_contribuable;
	private String  type_intermediaire;
	private String  nom_intermediaire;
	private String  telephone;
	private String  ville;
	private String  boite_postale;
	private String  statut_intermediaire;
	private Date 	date_debut_suspension;
	private Date 	date_fin_suspension;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public IntermediaireObject() {}
	public void setIid(int _entry)
	{
		this.iid = _entry;
	}
	public int getIid()
	{
		return this.iid;
	}
	public void setCodeIntermediaireDNA(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_intermediaire_dna = _entry.trim();
	}
	public String getCodeIntermediaireDNA()
	{
		return this.code_intermediaire_dna;
	}
	public void setNumContribuable(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.num_contribuable = _entry.trim();
	}
	public String getNumContribuable()
	{
		return this.num_contribuable;
	}
	public void setTypeIntermediaire(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.type_intermediaire = _entry.trim();
	}
	public String getTypeIntermediaire()
	{
		return this.type_intermediaire;
	}
	public void setNomIntermediaire(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.nom_intermediaire = _entry.trim();
	}
	public String getNomIntermediaire()
	{
		return this.nom_intermediaire;
	}
	public void setTelephone(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.telephone = _entry.trim();
	}
	public String getTelephone()
	{
		return this.telephone;
	}
	public void setVille(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.ville = _entry.trim();
	}
	public String getVille()
	{
		return this.ville;
	}
	public void setBoitePostale(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.boite_postale = _entry.trim();
	}
	public String getBoitePostale()
	{
		return this.boite_postale;
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}
	public void setStatutIntermediaire(String _entry)
	{
		this.statut_intermediaire = _entry;
	}
	public String getStatutIntermediaire()
	{
		return this.statut_intermediaire;
	}
	public void setDateDebutSuspension(Date _entry)
	{
		this.date_debut_suspension = _entry;
	}
	public Date getDateDebutSuspension()
	{
		return this.date_debut_suspension;
	}
	public void setDateFinSuspension(Date _entry)
	{
		this.date_fin_suspension = _entry;
	}
	public Date getDateFinSuspension()
	{
		return this.date_fin_suspension;
	}
	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
}
