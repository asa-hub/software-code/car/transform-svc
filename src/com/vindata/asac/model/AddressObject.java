package com.vindata.asac.model;

import java.util.*;

public class AddressObject 
{
	private int aid;
	private String code_assureur;
	private String boite_postale;
	private String rue;
	private String ville;
	private String pays;
	private String telephone;
	private String email;
	private String web;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public AddressObject() {}
	public void setAid(int aid)
	{
		this.aid = aid;
	}
	public int getAid()
	{
		return this.aid;
	}
	public void setCodeAssureur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assureur = _entry.trim();
	}
	public String getCodeAssureur()
	{
		return this.code_assureur;
	}
	public void setBoitePostale(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.boite_postale = _entry.trim();
	}
	public String getBoitePostale()
	{
		return this.boite_postale;
	}
	public void setRue(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.rue = _entry.trim();
	}
	public String getRue()
	{
		return this.rue;
	}
	public void setVille(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.ville = _entry.trim();
	}
	public String getVille()
	{
		return this.ville;
	}
	public void setPays(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.pays = _entry.trim();
	}
	public String getPays()
	{
		return this.pays;
	}
	public void setTelephone(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.telephone = _entry.trim();
	}
	public String getTelephone()
	{
		return this.telephone;
	}
	public void setEmail(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.email = _entry.trim();
	}
	public String getEmail()
	{
		return this.email;
	}
	public void setWeb(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.web = _entry.trim();
	}
	public String getWeb()
	{
		return this.web;
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}
	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
}
	
