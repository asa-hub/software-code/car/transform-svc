package com.vindata.asac.model;

import java.util.*;

public class AssureObject 
{
	private int aid;
	private String code_assure;
	private String code_assureur;
	private String qualite;
	private String nom;
	private String prenom;
	private String profession;
	private Date date_naissance;
	private String num_contribuable;
	private String ville;
	private String rue;
	private String boite_postale;
	private String telephone;
	private String email;
	private String numero_permis;
	private String categorie_permis;
	private Date date_deliverance;
	private String permis_delivre_par;
	private String nom_prenom_conducteur;
	private Date date_naissance_conducteur;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public AssureObject() {}
	public void setAid(int aid)
	{
		this.aid = aid;
	}
	public int getAid()
	{
		return this.aid;
	}
	public void setCodeAssure(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assure = _entry.trim();
	}
	public String getCodeAssure()
	{
		return this.code_assure;
	}
	public void setCodeAssureur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assureur = _entry.trim();
	}
	public String getCodeAssureur()
	{
		return this.code_assureur;
	}
	public void setQualite(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.qualite = _entry.trim();
	}
	public String getQualite()
	{
		return this.qualite;
	}
	public void setNom(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.nom = _entry.trim();
	}
	public String getNom()
	{
		return this.nom;
	}
	public void setPrenom(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.prenom = _entry.trim();
	}
	public String getPrenom()
	{
		return this.prenom;
	}
	public void setProfession(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.profession = _entry.trim();
	}
	public String getProfession()
	{
		return this.profession;
	}
	public void setDateNaissance(Date _entry)
	{
		this.date_naissance = _entry;
	}
	public Date getDateNaissance()
	{
		return this.date_naissance;
	}
	public void setNumContribuable(String _entry)
	{
		this.num_contribuable = _entry;
	}
	public String getNumContribuable()
	{
		return this.num_contribuable;
	}
	public void setVille(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.ville = _entry.trim();
	}
	public String getVille()
	{
		return this.ville;
	}
	public void setRue(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.rue = _entry.trim();
	}
	public String getRue()
	{
		return this.rue;
	}
	public void setBoitePostale(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.boite_postale = _entry.trim();
	}
	public String getBoitePostale()
	{
		return this.boite_postale;
	}
	public void setTelephone(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.telephone = _entry.trim();
	}
	public String getTelephone()
	{
		return this.telephone;
	}
	public void setEmail(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.email = _entry.trim();
	}
	public String getEmail()
	{
		return this.email;
	}
	public void setNumeroPermis(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.numero_permis = _entry.trim();
	}
	public String getNumeroPermis()
	{
		return this.numero_permis;
	}
	public void setCategoriePermis(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.categorie_permis = _entry.trim();
	}
	public String getCategoriePermis()
	{
		return this.categorie_permis;
	}
	public void setDateDeliverance(Date _entry)
	{
		this.date_deliverance = _entry;
	}
	public Date getDateDeliverance()
	{
		return this.date_deliverance;
	}
	public void setPermisDelivrePar(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.permis_delivre_par = _entry.trim();
	}
	public String getPermisDelivrePar()
	{
		return this.permis_delivre_par;
	}
	public void setNomPrenomConducteur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.nom_prenom_conducteur = _entry.trim();
	}
	public String getNomPrenomConducteur()
	{
		return this.nom_prenom_conducteur;
	}
	public void setDateNaissanceConducteur(Date _entry)
	{
		this.date_naissance_conducteur = _entry;
	}
	public Date getDateNaissanceConducteur()
	{
		return this.date_naissance_conducteur;
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}
	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
}
