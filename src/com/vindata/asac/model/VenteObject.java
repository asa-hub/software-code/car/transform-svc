package com.vindata.asac.model;

import java.util.*;

public class VenteObject 
{
	private int 	vid;
	private String 	numero_attestation;
	private String 	immatriculation;
	private String 	code_assure;
	private String 	code_assureur;
	private String 	code_intermediaire;
	private int 	prime_nette_rc;
	private int 	dta;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public VenteObject() {}
	public void setVid(int _entry)
	{
		this.vid = _entry;
	}
	public int getVid()
	{
		return this.vid;
	}
	public void setNumeroAttestation(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.numero_attestation = _entry.trim();
	}
	public String getNumeroAttestation()
	{
		return this.numero_attestation;
	}
	public void setImmatriculation(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.immatriculation = _entry.trim();
	}
	public String getImmatriculation()
	{
		return this.immatriculation;
	}
	public void setCodeAssure(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assure = _entry.trim();
	}
	public String getCodeAssure()
	{
		return this.code_assure;
	}
	public void setCodeAssureur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assureur = _entry.trim();
	}
	public String getCodeAssureur()
	{
		return this.code_assureur;
	}
	public void setCodeIntermediaire(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_intermediaire = _entry.trim();
	}
	public String getCodeIntermediaire()
	{
		return this.code_intermediaire;
	}
	public void setPrimeNetteRC(int _entry)
	{
		this.prime_nette_rc = _entry;
	}
	public int getPrimeNetteRC()
	{
		return this.prime_nette_rc;
	}
	public void setDta(int _entry)
	{
		this.dta = _entry;
	}
	public int getDta()
	{
		return this.dta;
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}
	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
}
