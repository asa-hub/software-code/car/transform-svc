package com.vindata.asac.model;

import java.util.*;

public class VehiculeObject 
{
	private int vid;
	private String code_assure;
	private String code_assureur;
	private String marque;
	private String model;
	private Date premiere_date_mise_circulation;
	private String immatriculation;
	private String chassis;
	private String usage;
	private String charge_utile;
	private double puissance_fiscale;
	private String remorque;
	private int nombre_portes;
	private String immatriculation_remorque;
	private String source_energie;
	private int nombre_de_places;
	private int cylindre;
	private int double_commande;
	private int responsabilite_civile;
	private int utilitaire;
	private int type_engin;
	private double poids;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public VehiculeObject() {}
	public void setVid(int _entry)
	{
		this.vid = _entry;
	}
	public int getVid()
	{
		return this.vid;
	}
	public void setCodeAssure(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assure = _entry.trim();
	}
	public String getCodeAssure()
	{
		return this.code_assure;
	}
	public void setCodeAssureur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assureur = _entry.trim();
	}
	public String getCodeAssureur()
	{
		return this.code_assureur;
	}
	public void setMarque(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.marque = _entry.trim();
	}
	public String getMarque()
	{
		return this.marque;	
	}
	public void setModel(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.model = _entry.trim();
	}
	public String getModel()
	{
		return this.model;	
	}
	public void setPremiereDateMiseCirculation(Date _entry)
	{
		this.premiere_date_mise_circulation = _entry;
	}
	public Date getPremiereDateMiseCirculation()
	{
		return this.premiere_date_mise_circulation;	
	}
	public void setImmatriculation(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.immatriculation = _entry.trim();
	}
	public String getImmatriculation()
	{
		return this.immatriculation;	
	}
	public void setChassis(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.chassis = _entry.trim();
	}
	public String getChassis()
	{
		return this.chassis;	
	}
	public void setUsage(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.usage = _entry.trim();
	}
	public String getUsage()
	{
		return this.usage;	
	}
	public void setChargeUtile(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.charge_utile = _entry.trim();
	}
	public String getChargeUtile()
	{
		return this.charge_utile;	
	}
	public void setPuissanceFiscale(double _entry)
	{
		this.puissance_fiscale = _entry;
	}
	public double getPuissanceFiscale()
	{
		return this.puissance_fiscale;	
	}
	public void setRemorque(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.remorque = _entry.trim();
	}
	public String getRemorque()
	{
		return this.remorque;	
	}
	public void setNombrePortes(int _entry)
	{
		this.nombre_portes = _entry;
	}
	public int getNombrePortes()
	{
		return this.nombre_portes;	
	}
	public void setImmatriculationRemorque(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.immatriculation_remorque = _entry.trim();
	}
	public String getImmatriculationRemorque()
	{
		return this.immatriculation_remorque;	
	}
	public void setSourceEnergie(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.source_energie = _entry.trim();
	}
	public String getSourceEnergie()
	{
		return this.source_energie;	
	}
	public void setNombreDePlaces(int _entry)
	{
		this.nombre_de_places = _entry;
	}
	public int getNombreDePlaces()
	{
		return this.nombre_de_places;	
	}
	public void setCylindre(int _entry)
	{
		this.cylindre = _entry;
	}
	public int getCylindre()
	{
		return this.cylindre;	
	}
	public void setDoubleCommande(int _entry)
	{
		this.double_commande = _entry;
	}
	public int getDoubleCommande()
	{
		return this.double_commande;	
	}
	public void setResponsabiliteCivile(int _entry)
	{
		this.responsabilite_civile = _entry;
	}
	public int getResponsabiliteCivile()
	{
		return this.responsabilite_civile;	
	}
	public void setUtilitaire(int _entry)
	{
		this.utilitaire = _entry;
	}
	public int getUtilitaire()
	{
		return this.utilitaire;	
	}
	public void setTypeEngin(int _entry)
	{
		this.type_engin = _entry;
	}
	public int getTypeEngin()
	{
		return this.type_engin;	
	}
	public void setPoids(double _entry)
	{
		this.poids = _entry;
	}
	public double getPoids()
	{
		return this.poids;	
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}
	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
}
