package com.vindata.asac.model;

import java.util.*;
import java.util.regex.*;

public class SinistreObject 
{
	private int 	sid;
	private String 	code_assureur;
	private String 	reference;
	private String 	type_dommage;
	private String 	cause_sinistre;
	private String 	numero_attestation;
	private Date 	date_survenance;
	private Date 	date_declaration;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public SinistreObject(){}
	public void setSid(int _entry)
	{
		this.sid = _entry;
	}
	public int getSid()
	{
		return this.sid;
	}
	public void setCodeAssureur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assureur = _entry.trim();
	}
	public String getCodeAssureur()
	{
		return this.code_assureur;
	}
	public void setReference(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.reference = _entry.trim();
	}
	public String getReference()
	{
		return this.reference;
	}
	public void setTypeDommage(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.type_dommage = _entry.trim();
	}
	public String getTypeDommage()
	{
		return reset_quotes(type_dommage, false);
	}
	public String getTypeDommageDB()
	{
		return reset_quotes(type_dommage, true);
	}
	public void setCauseSinistre(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.cause_sinistre = _entry.trim();
	}
	public String getCauseSinistre()
	{
		if (this.cause_sinistre != null && this.cause_sinistre.trim().length() > 0) return reset_quotes(this.cause_sinistre, false);
		return null;
	}
	public String getCauseSinistreDB()
	{
		if (this.cause_sinistre != null && this.cause_sinistre.trim().length() > 0) return reset_quotes(this.cause_sinistre, false);
		return null;
	}
	public void setNumeroAttestation(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.numero_attestation = _entry.trim();
	}
	public String getNumeroAttestation()
	{
		return this.numero_attestation;
	}
	public void setDateSurvenance(Date _entry)
	{
		this.date_survenance = _entry;
	}
	public Date getDateSurvenance()
	{
		return this.date_survenance;
	}
	public void setDateDeclaration(Date _entry)
	{
		this.date_declaration = _entry;
	}
	public Date getDateDeclaration()
	{
		return this.date_declaration;
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}
	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
	private String reset_quotes(String entry_strng, boolean saving)
	{

		String pattern = "'";
		String replace_strng = "\\.";

		if (saving == false) 
		{
			pattern = "\\.";
			replace_strng = "'";
		}

		Pattern p = Pattern.compile(pattern);	
		Matcher m = p.matcher(entry_strng);
		StringBuffer sb = new StringBuffer();

		int pos = 0;	
		while (m.find())
		{
			m.appendReplacement(sb, replace_strng);
		}

		m.appendTail(sb);

		return sb.toString();
	}
}
