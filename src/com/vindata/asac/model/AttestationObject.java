package com.vindata.asac.model;

import java.util.*;

public class AttestationObject 
{
	private int aid;
	private String numero_attestation;
	private String numero_police;
	private Date date_emission;
	private Date date_effet;
	private Date date_echeance;
	private String couleur;
	private String statut;
	private String zone_circulation;
	private String immatriculation;
	private String remorque;
	private String code_assure;
	private String code_assureur;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public AttestationObject(){}
	public void setAid(int _entry)
	{
		this.aid = _entry;
	}
	public int getAid()
	{
		return this.aid;
	}
	public void setNumeroAttestation(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.numero_attestation = _entry.trim();
	}
	public String getNumeroAttestation()
	{
		return this.numero_attestation;
	}
	public void setNumeroPolice(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.numero_police = _entry.trim();
	}
	public String getNumeroPolice()
	{
		return this.numero_police;
	}
	public void setDateEmission(Date _entry)
	{
		this.date_emission = _entry;
	}
	public Date getDateEmission()
	{
		return this.date_emission;
	}
	public void setDateEffet(Date _entry)
	{
		this.date_effet = _entry;
	}
	public Date getDateEffet()
	{
		return this.date_effet;
	}
	public void setDateEcheance(Date _entry)
	{
		this.date_echeance = _entry;
	}
	public Date getDateEcheance()
	{
		return this.date_echeance;
	}
	public void setCouleur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.couleur = _entry.trim();
	}
	public String getCouleur()
	{
		return this.couleur;
	}
	public void setStatut(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.statut = _entry.trim();
	}
	public String getStatut()
	{
		return this.statut;
	}
	public void setZoneCirculation(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.zone_circulation = _entry.trim();
	}
	public String getZoneCirculation()
	{
		return this.zone_circulation;
	}
	public void setImmatriculation(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.immatriculation = _entry.trim();
	}
	public String getImmatriculation()
	{
		return this.immatriculation;
	}
	public void setRemorque(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.remorque = _entry.trim();
	}
	public String getRemorque()
	{
		return this.remorque;
	}
	public void setCodeAssure(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assure = _entry.trim();
	}
	public String getCodeAssure()
	{
		return this.code_assure;
	}
	public void setCodeAssureur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assureur = _entry.trim();
	}
	public String getCodeAssureur()
	{
		return this.code_assureur;
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}

	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
}
