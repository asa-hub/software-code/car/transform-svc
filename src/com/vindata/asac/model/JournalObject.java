package com.vindata.asac.model;

import java.util.*;

public class JournalObject 
{
	private int jid;
	private String reference;
	private String description;
	private String status;
	private Date date_inscription;

	public JournalObject() {}
	public void setJid(int jid)
	{
		this.jid = jid;
	}
	public int getJid()
	{
		return this.jid;
	}
	public void setReference(String reference)
	{
		this.reference = reference;
	}
	public String getReference()
	{
		return this.reference;
	}
	public void setDescription(String _entry)
	{
		this.description = _entry;
	}
	public String getDescription()
	{
		return this.description;
	}
	public void setStatus(String _entry)
	{
		this.status = _entry;
	}
	public String getStatus()
	{
		return this.status;
	}
	public void setDateInscription(Date _entry)
	{
		this.date_inscription = _entry;
	}
	public Date getDateInscription()
	{
		return this.date_inscription;
	}
}
