package com.vindata.asac.model;

import java.util.*;

public class RemorqueObject 
{
	private int rid;
	private String code_assure;
	private String code_assureur;
	private String immatriculation_remorque;

	private int control_status;
	private String comments;

	private Date date_creation;
	private Date date_mise_jour;
	private Date date_transfer;

	public RemorqueObject() {}
	public void setRid(int _id)
	{
		this.rid = _id;
	}
	public int getRid()
	{
		return this.rid;
	}
	public void setCodeAssure(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assure = _entry.trim();
	}
	public String getCodeAssure()
	{
		return this.code_assure;
	}
	public void setCodeAssureur(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.code_assureur = _entry.trim();
	}
	public String getCodeAssureur()
	{
		return this.code_assureur;
	}
	public void setImmatriculationRemorque(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.immatriculation_remorque = _entry.trim();
	}
	public String getImmatriculationRemorque()
	{
		return this.immatriculation_remorque;
	}
	public void setControlStatus(int _entry)
	{
		this.control_status = _entry;
	}
	public int getControlStatus()
	{
		return this.control_status;
	}
	public void setComments(String _entry)
	{
		if (_entry != null && _entry.trim().length() > 0) this.comments = _entry.trim();
	}
	public String getComments()
	{
		return this.comments;
	}
	public void setDateCreation(Date _entry)
	{
		this.date_creation = _entry;
	}
	public Date getDateCreation()
	{
		return this.date_creation;
	}
	public void setDateMiseJour(Date _entry)
	{
		this.date_mise_jour = _entry;
	}
	public Date getDateMiseJour()
	{
		return this.date_mise_jour;
	}
	public void setDateTransfer(Date _entry)
	{
		this.date_transfer = _entry;
	}
	public Date getDateTransfer()
	{
		return this.date_transfer;
	}
}
