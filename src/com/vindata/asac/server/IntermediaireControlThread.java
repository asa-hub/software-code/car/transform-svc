package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class IntermediaireControlThread extends Thread 
{
	private AsacDAOImpl asacSvc = null;
    private LogMaster logMaster = null;
	public IntermediaireControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
        logMaster = LogMaster.getInstance();
	}
	public void run()
	{
		//String qstrng = "Select * from intermediaire where c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\'";
		String qstrng = "Select * from intermediaire where "; 
		qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
		List<IntermediaireObject> intermediaireArr = asacSvc.queryIntermediaire(qstrng);
		if (intermediaireArr != null && intermediaireArr.size() > 0)
		{
			for(int l=0; l<intermediaireArr.size(); l++)
			{
				IntermediaireObject obj = (IntermediaireObject)intermediaireArr.get(l);
				if (obj != null)
				{
					String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.INTERMEDIAIRE_ABBREV + "_" + obj.getIid();
					JournalObject jnrObj = new JournalObject();
					jnrObj.setReference(journalRef);

					ArrayList errorArr = new ArrayList();
					if (isValid(obj, errorArr))
					{
						jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
						obj.setComments("");
					}
					else
					{
						if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);

							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
							jnrObj.setDescription(obj.getComments());
							//LogMaster.logActivities("INVALID ENTRY\tINTERMEDIAIRE\t"+obj.getCodeIntermediaireDNA()+ "\'\t\'" + obj.getNumContribuable()+"\'\t\'" + obj.getNomIntermediaire()+"\'\t\'" + obj.getComments() + "\'");
						}
					}

					obj.setDateMiseJour(Calendar.getInstance().getTime());
					int retval = asacSvc.updateIntermediaire(obj);
					if (retval != VINStat.SUCCESS)
					{
						LogMaster.logActivities("UPDATE FAILED FOR INTERMEDIAIRE ["+obj.getNumContribuable()+"]");
					}
					else
					{
						//LogMaster.logActivities("UPDATE SUCCEEDED FOR INTERMEDIAIRE ["+obj.getNumContribuable()+"]");
						if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournalIntermediaire(obj);
					}

				}
			}
		}
	}
	private boolean isValid(IntermediaireObject intermediaireObj, ArrayList errorArr)
	{
		return true;
	}
	/*
	private boolean isValidOLD(IntermediaireObject intermediaireObj, ArrayList errorArr)
	{
		boolean valid = false;
		String errorStrng = VINStat.DIVIDER;
		if (intermediaireObj != null)
		{
			if (intermediaireObj.getIdentifiantDNA() <= 0)
			{
				if (intermediaireObj.getTypeAssureur() == null  || intermediaireObj.getTypeAssureur().trim().length() == 0 || 
					(intermediaireObj.getTypeAssureur().toUpperCase().contains(VINStat.INSURANCE_TYPE_COURTIER) == false &&
					 intermediaireObj.getTypeAssureur().toUpperCase().contains(VINStat.INSURANCE_TYPE_AGENT_GENERAL) == false))
				{
					valid = true;	
					errorStrng += VINStat.MISSING_IDENTIFIANT_DNA;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (intermediaireObj.getTypeAssureur() == null || intermediaireObj.getTypeAssureur().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_TYPE_ASSUREUR;
				errorStrng += VINStat.DIVIDER;
			}

			if (intermediaireObj.getNomIntermediaire() == null || intermediaireObj.getNomIntermediaire().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_NOM_INTERMEDIAIRE;
				errorStrng += VINStat.DIVIDER;
			}

			if (intermediaireObj.getTelephone() == null || intermediaireObj.getTelephone().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_TELEPHONE;
				errorStrng += VINStat.DIVIDER;
			}

			if (intermediaireObj.getVille() == null || intermediaireObj.getVille().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_VILLE;
				errorStrng += VINStat.DIVIDER;
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	*/
}



