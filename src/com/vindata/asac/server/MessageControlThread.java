package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import javax.mail.*;
import javax.mail.internet.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class MessageControlThread extends Thread 
{
	private String mypassword;
	private String fromAddress;

	private String filename = "/var/hubasac/config/hubasac.properties";

	private String smtpHostName;
	private String smtpPort;
	private String fileEncoding;
	private String mailMessage;
	private String mailSubject;

	private AsacDAOImpl asacSvc = null;
    private LogMaster logMaster = null;
	public MessageControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
        logMaster = LogMaster.getInstance();
	}
	public void run()
	{
		while (true)
		{
			Calendar now = Calendar.getInstance();
			int currHour = now.get(Calendar.HOUR_OF_DAY);

			try
			{
				// check errors on assure table
				checkAssureErrors();

				// check errors on vehicule table
				//checkVehiculeErrors();

				Thread.sleep(3600000); // sleep and rest for 1hour
			}
			catch(Exception e)
			{
			}
		}
	}
    private MimeMessage getMimeMessage()
    {
	    MimeMessage mimeMessage = null;

		try
		{
			File myfile = new File(filename);
			if (myfile.exists() == false)
			{
				myfile = new File("./config/hubasac.properties");
				if (myfile.exists() == false)
				{
					System.err.println("Error DB properties file not found! Check path /usr/vindata/config/db_properties");
					return null;
				}
			}
			
			FileInputStream infile = new FileInputStream(myfile);
			Properties prop = new Properties();
			prop.load(infile);

			Properties properties = new Properties();
			properties.put("mail.smtp.host", prop.getProperty("mail.smtp.host", "smtp.strato.de"));	
			properties.put("mail.smtp.port", prop.getProperty("mail.smtp.port", "465"));	
			properties.put("mail.smtp.auth", prop.getProperty("mail.smtp.auth", "true"));
			properties.put("mail.smtp.socketFactory.port", prop.getProperty("mail.socketFactory.port", "465"));
			properties.put("mail.smtp.socketFactory.class", prop.getProperty("mail.smtp.sockerFactory.class", "javax.net.ssl.SSLSocketFactory"));	

			String myEmail  =  prop.getProperty("mail.email.address", "sysadmin@bbitanalytics.com");
			String myPasswd =  prop.getProperty("mail.password", 	 "@Bajj15032011");

			javax.mail.Session session = javax.mail.Session.getInstance(properties, 
				new javax.mail.Authenticator(){
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(fromAddress, mypassword);
					}
			});

			// set FROM address in the MimeMessage
			mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(fromAddress));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

        return mimeMessage;
    }
	private void checkAssureErrors()
	{
		try
		{
			// 1. get all erroneous assurees order by their insurance company
			String qstrng = "Select * from assure where c_status = \'" + VINStat.STATUS_INVALID + "\' group by code_assureur order by c_date_creation desc";
			List<AssureObject> objArr = asacSvc.queryAssure(qstrng);
			if (objArr != null && objArr.size() > 0)
			{
				for(int l=0; l<objArr.size(); l++)
				{
					// check for each code_assureur....
					AssureObject obj = (AssureObject)objArr.get(l);
					if (obj != null)
					{
						// check whether this error message had already been notified 
						// 1. get the last message sent for this insurance company
						AssureObject lastMesgObj = null;
						String mesgQStrng = "Select * from journal_assure ja where ja.jaid = (select max(ja2.jaid) from journal_assure ja2 where ja2.code_assureur = \'" + obj.getCodeAssureur() + "\' and ja2.c_status = \'" + VINStat.CONTROL_STATUS_MESSAGE_SENT + "\')";
						List<AssureObject> mesgArr = asacSvc.queryJournalAssure(mesgQStrng);
						if (mesgArr != null && mesgArr.size() > 0)
						{
							lastMesgObj = (AssureObject)mesgArr.get(0);	
						}
						
						if (lastMesgObj == null || (lastMesgObj.getDateMiseJour().getTime() < obj.getDateMiseJour().getTime()))
						{
							String lastQStrng = "Select * from assure where c_status = \'" + VINStat.STATUS_INVALID + "\' and code_assureur = \'" + obj.getCodeAssureur() + "\'";
							if (lastMesgObj != null)
							{
								java.sql.Timestamp sts = new java.sql.Timestamp(lastMesgObj.getDateMiseJour().getTime());
								lastQStrng += " and c_date_mis_a_jour > \'" + sts + "\'";
							}

							List<AssureObject> messArr = asacSvc.queryAssure(lastQStrng);
							if (messArr != null && messArr.size() > 0) sendMessage(messArr);
						}
					}
				}
			}
		}
		catch(Exception ee)
		{
        	logMaster.logDebug(ee.getMessage(), ee);
		}
	}
	private void checkAttestationErrors()
	{
		try
		{
			// 1. get erroneous attestation per code_assureur..
			String qstrng = "Select * from attestation where c_status = \'" + VINStat.STATUS_INVALID + "\' group by code_assureur order by c_date_creation desc";
			List<AttestationObject> objArr = asacSvc.queryAttestation(qstrng);
			if (objArr != null && objArr.size() > 0)
			{
				for(int l=0; l<objArr.size(); l++)
				{
					AttestationObject obj = (AttestationObject)objArr.get(l);
					if (obj != null)
					{
						AttestationObject lastMesgObj = null;
						String mesgQStrng = "Select * from journal_attestation ja where ja.jaid = (select max(ja2.jaid) from journal_attestation ja2 where ja2.code_assureur = \'" + obj.getCodeAssureur() + "\' and ja2.c_status = \'" + VINStat.CONTROL_STATUS_MESSAGE_SENT + "\')";
						List<AttestationObject> mesgArr = asacSvc.queryJournalAttestation(mesgQStrng);
						if (mesgArr != null && mesgArr.size() > 0)
						{
							lastMesgObj = (AttestationObject)mesgArr.get(0);	
						}

						if (lastMesgObj == null || (lastMesgObj.getDateMiseJour().getTime() < obj.getDateMiseJour().getTime()))
						{
							String lastQStrng = "Select * from attestation where c_status = \'" + VINStat.STATUS_INVALID + "\' and code_assureur = \'" + obj.getCodeAssureur() + "\'";
							if (lastMesgObj != null)
							{
								java.sql.Timestamp sts = new java.sql.Timestamp(lastMesgObj.getDateMiseJour().getTime());
								lastQStrng += " and c_date_mis_a_jour > \'" + sts + "\'";
							}

							List<AttestationObject> messArr = asacSvc.queryAttestation(lastQStrng);
							if (messArr != null && messArr.size() > 0) sendMessage(messArr);
						}
					}
				}
			}
		}
		catch(Exception ee)
		{
        	logMaster.logDebug(ee.getMessage(), ee);
		}
	}
	// no check per assurance necessary, since they are not necessarily tied
	// to an insurance compnay
	/*
	private void checkIntermediaireErrors()
	{
		try
		{
			// 1. get erroneous assuree..
			String qstrng = "Select * from intermediaire where c_status = \'" + VINStat.STATUS_INVALID + "\' group by code_assureur";
			List<IntermediaireObject> objArr = asacSvc.queryIntermediaire(qstrng);
			if (objArr != null && objArr.size() > 0)
			{
				for(int l=0; l<objArr.size(); l++)
				{
					IntermediaireObject obj = (IntermediaireObject)objArr.get(l);
					if (obj != null)
					{
						// check whether this error message had already been notified 
						// 1. get the last message sent for this insurance company
						AssureObject lastMesgObj = null;
						String mesgQStrng = "Select * from journal_intermediaire ja where ja.jaid = (select max(ja2.jaid) from journal_intermediaire ja2 where ja2.code_assureur = \'" + obj.getCodeAssureur() + "\' and ja2.c_status = \'" + VINStat.CONTROL_STATUS_MESSAGE_SENT + "\')";
						List<AssureObject> mesgArr = asacSvc.queryJournalAssure(mesgQStrng);
						if (mesgArr != null && mesgArr.size() > 0)
						{
							lastMesgObj = (AssureObject)mesgArr.get(0);	
						}
						
						if (lastMesgObj == null || (lastMesgObj.getDateMiseJour().getTime() < obj.getDateMiseJour().getTime()))
						{
							String lastQStrng = "Select * from assure where c_status = \'" + VINStat.STATUS_INVALID + "\' and code_assureur = \'" + obj.getCodeAssureur() + "\'";
							if (lastMesgObj != null)
							{
								java.sql.Timestamp sts = new java.sql.Timestamp(lastMesgObj.getDateMiseJour().getTime());
								lastQStrng += " and c_date_mis_a_jour > \'" + sts + "\'";
							}

							List<AssureObject> messArr = asacSvc.queryAssure(lastQStrng);
							if (messArr != null && messArr.size() > 0) sendMessage(messArr);
						}
					}
				}
			}
		}
		catch(Exception ee)
		{
        	logMaster.logDebug(ee.getMessage(), ee);
		}
	}
	*/
	private void checkRemorqueErrors()
	{
		try
		{
			// 1. get erroneous assuree..
			String qstrng = "Select * from remorque where c_status = \'" + VINStat.STATUS_INVALID + "\' group by code_assureur order by c_date_creation desc";
			List<RemorqueObject> objArr = asacSvc.queryRemorque(qstrng);
			if (objArr != null && objArr.size() > 0)
			{
				for(int l=0; l<objArr.size(); l++)
				{
					RemorqueObject obj = (RemorqueObject)objArr.get(l);
					if (obj != null)
					{
						// check whether this error message had already been notified 
						// 1. get the last message sent for this insurance company
						RemorqueObject lastMesgObj = null;
						String mesgQStrng = "Select * from journal_remorque ja where ja.jrid = (select max(ja2.jrid) from journal_remorque ja2 where ja2.code_assureur = \'" + obj.getCodeAssureur() + "\' and ja2.c_status = \'" + VINStat.CONTROL_STATUS_MESSAGE_SENT + "\')";
						List<RemorqueObject> mesgArr = asacSvc.queryJournalRemorque(mesgQStrng);
						if (mesgArr != null && mesgArr.size() > 0)
						{
							lastMesgObj = (RemorqueObject)mesgArr.get(0);	
						}
						
						if (lastMesgObj == null || (lastMesgObj.getDateMiseJour().getTime() < obj.getDateMiseJour().getTime()))
						{
							String lastQStrng = "Select * from remorque where c_status = \'" + VINStat.STATUS_INVALID + "\' and code_assureur = \'" + obj.getCodeAssureur() + "\'";
							if (lastMesgObj != null)
							{
								java.sql.Timestamp sts = new java.sql.Timestamp(lastMesgObj.getDateMiseJour().getTime());
								lastQStrng += " and c_date_mis_a_jour > \'" + sts + "\'";
							}

							List<RemorqueObject> messArr = asacSvc.queryRemorque(lastQStrng);
							if (messArr != null && messArr.size() > 0) sendMessage(messArr);
						}
					}
				}
			}
		}
		catch(Exception ee)
		{
        	logMaster.logDebug(ee.getMessage(), ee);
		}
	}
	private void checkSinistreErrors()
	{
		try
		{
			// 1. get erroneous assuree..
			String qstrng = "Select * from sinistre where c_status = \'" + VINStat.STATUS_INVALID + "\' group by code_assureur order by c_date_creation desc";
			List<SinistreObject> objArr = asacSvc.querySinistre(qstrng);
			if (objArr != null && objArr.size() > 0)
			{
				for(int l=0; l<objArr.size(); l++)
				{
					SinistreObject obj = (SinistreObject)objArr.get(l);
					if (obj != null)
					{
						// check whether this error message had already been notified 
						// 1. get the last message sent for this insurance company
						SinistreObject lastMesgObj = null;
						String mesgQStrng = "Select * from journal_sinistre ja where ja.jsid = (select max(ja2.jsid) from journal_sinistre ja2 where ja2.code_assureur = \'" + obj.getCodeAssureur() + "\' and ja2.c_status = \'" + VINStat.CONTROL_STATUS_MESSAGE_SENT + "\')";
						List<SinistreObject> mesgArr = asacSvc.queryJournalSinistre(mesgQStrng);
						if (mesgArr != null && mesgArr.size() > 0)
						{
							lastMesgObj = (SinistreObject)mesgArr.get(0);	
						}
						
						if (lastMesgObj == null || (lastMesgObj.getDateMiseJour().getTime() < obj.getDateMiseJour().getTime()))
						{
							String lastQStrng = "Select * from sinistre where c_status = \'" + VINStat.STATUS_INVALID + "\' and code_assureur = \'" + obj.getCodeAssureur() + "\'";
							if (lastMesgObj != null)
							{
								java.sql.Timestamp sts = new java.sql.Timestamp(lastMesgObj.getDateMiseJour().getTime());
								lastQStrng += " and c_date_mis_a_jour > \'" + sts + "\'";
							}

							List<SinistreObject> messArr = asacSvc.querySinistre(lastQStrng);
							if (messArr != null && messArr.size() > 0) sendMessage(messArr);
						}
					}
				}
			}
		}
		catch(Exception ee)
		{
        	logMaster.logDebug(ee.getMessage(), ee);
		}
	}
	private void checkVehiculeErrors()
	{
		try
		{
			// 1. get erroneous assuree..
			String qstrng = "Select * from vehicule where c_status = \'" + VINStat.STATUS_INVALID + "\' group by code_assureur order by c_date_creation desc";
			List<VehiculeObject> objArr = asacSvc.queryVehicule(qstrng);
			if (objArr != null && objArr.size() > 0)
			{
				for(int l=0; l<objArr.size(); l++)
				{
					VehiculeObject obj = (VehiculeObject)objArr.get(l);
					if (obj != null)
					{
						// check whether this error message had already been notified 
						// 1. get the last message sent for this insurance company
						VehiculeObject lastMesgObj = null;
						String mesgQStrng = "Select * from journal_vehicule ja where ja.jvid = (select max(ja2.jvid) from journal_vehicule ja2 where ja2.code_assureur = \'" + obj.getCodeAssureur() + "\' and ja2.c_status = \'" + VINStat.CONTROL_STATUS_MESSAGE_SENT + "\')";
						List<VehiculeObject> mesgArr = asacSvc.queryJournalVehicule(mesgQStrng);
						if (mesgArr != null && mesgArr.size() > 0)
						{
							lastMesgObj = (VehiculeObject)mesgArr.get(0);	
						}
						
						if (lastMesgObj == null || (lastMesgObj.getDateMiseJour().getTime() < obj.getDateMiseJour().getTime()))
						{
							String lastQStrng = "Select * from vehicule where c_status = \'" + VINStat.STATUS_INVALID + "\' and code_assureur = \'" + obj.getCodeAssureur() + "\'";
							if (lastMesgObj != null)
							{
								java.sql.Timestamp sts = new java.sql.Timestamp(lastMesgObj.getDateMiseJour().getTime());
								lastQStrng += " and c_date_mis_a_jour > \'" + sts + "\'";
							}

							List<VehiculeObject> messArr = asacSvc.queryVehicule(lastQStrng);
							if (messArr != null && messArr.size() > 0) sendMessage(messArr);
						}
					}
				}
			}
		}
		catch(Exception ee)
		{
        	logMaster.logDebug(ee.getMessage(), ee);
		}
	}
	private void checkVenteErrors()
	{
		try
		{
			// 1. get erroneous assuree..
			String qstrng = "Select * from vente where c_status = \'" + VINStat.STATUS_INVALID + "\' group by code_assureur order by c_date_creation desc";
			List<VenteObject> objArr = asacSvc.queryVente(qstrng);
			if (objArr != null && objArr.size() > 0)
			{
				for(int l=0; l<objArr.size(); l++)
				{
					VenteObject obj = (VenteObject)objArr.get(l);
					if (obj != null)
					{
						// check whether this error message had already been notified 
						// 1. get the last message sent for this insurance company
						VenteObject lastMesgObj = null;
						String mesgQStrng = "Select * from journal_vente ja where ja.jvid = (select max(ja2.jvid) from journal_vente ja2 where ja2.code_assureur = \'" + obj.getCodeAssureur() + "\' and ja2.c_status = \'" + VINStat.CONTROL_STATUS_MESSAGE_SENT + "\')";
						List<VenteObject> mesgArr = asacSvc.queryJournalVente(mesgQStrng);
						if (mesgArr != null && mesgArr.size() > 0)
						{
							lastMesgObj = (VenteObject)mesgArr.get(0);	
						}
						
						if (lastMesgObj == null || (lastMesgObj.getDateMiseJour().getTime() < obj.getDateMiseJour().getTime()))
						{
							String lastQStrng = "Select * from vente where c_status = \'" + VINStat.STATUS_INVALID + "\' and code_assureur = \'" + obj.getCodeAssureur() + "\'";
							if (lastMesgObj != null)
							{
								java.sql.Timestamp sts = new java.sql.Timestamp(lastMesgObj.getDateMiseJour().getTime());
								lastQStrng += " and c_date_mis_a_jour > \'" + sts + "\'";
							}

							List<VenteObject> messArr = asacSvc.queryVente(lastQStrng);
							if (messArr != null && messArr.size() > 0) sendMessage(messArr);
						}
					}
				}
			}
		}
		catch(Exception ee)
		{
        	logMaster.logDebug(ee.getMessage(), ee);
		}
	}
	private void sendMessage(List messageArr)
	{
		String fileEncoding   = "iso-8859-1";

		try
		{
			AddressObject addrObj = null;
			mailSubject	= "Les entrees suivantes sont invalides : ";
			mailMessage = "";
			for (int i=0; i<messageArr.size(); i++)
			{
				Object errorObj = messageArr.get(i);
				if (errorObj instanceof AssureObject)
				{
					AssureObject assObj = (AssureObject)errorObj;
					if (addrObj == null)
					{
						String addressQStrng = "Select * from adresse where code_assureur = \'" + assObj.getCodeAssureur() + "\'";
						List<AddressObject> addrArr = asacSvc.queryAddress(addressQStrng);
						if (addrArr != null && addrArr.size() > 0)
						{
							addrObj = (AddressObject)addrArr.get(0);
						}
						else
						{
							LogMaster.logDebug("MessageControlThread:sendAssure: Missing address for this code_assureur ["+assObj.getCodeAssureur()+"]", null);
							return;
						}
					}

					assObj.setControlStatus(VINStat.CONTROL_STATUS_MESSAGE_SENT);
					asacSvc.addJournalAssure(assObj);

					mailMessage += " ----------------------------------------------------- \n";
					mailMessage += "[" + assObj.getNom() + " " + assObj.getPrenom() + "]\n";
					mailMessage += assObj.getComments() + "\n";
				}
				else
				if (errorObj instanceof AttestationObject)
				{
					AttestationObject assObj = (AttestationObject)errorObj;
					if (addrObj == null)
					{
						String addressQStrng = "Select * from adresse where code_assureur = \'" + assObj.getCodeAssureur() + "\'";
						List<AddressObject> addrArr = asacSvc.queryAddress(addressQStrng);
						if (addrArr != null && addrArr.size() > 0)
						{
							addrObj = (AddressObject)addrArr.get(0);
						}
						else
						{
							LogMaster.logDebug("MessageControlThread:sendAssure: Missing address for this code_assureur ["+assObj.getCodeAssureur()+"]", null);
							return;
						}
					}

					assObj.setControlStatus(VINStat.CONTROL_STATUS_MESSAGE_SENT);
					asacSvc.addJournalAttestation(assObj);

					mailMessage += " ----------------------------------------------------- \n";
					mailMessage	+= "Numero d'attestation invalide : [" + assObj.getNumeroAttestation() + "] \n";
					mailMessage += assObj.getComments() + "\n";
				}
				else
				if (errorObj instanceof IntermediaireObject)
				{
					/*
					 * no code_assureur here. so dont know where to send the message
					IntermediaireObject assObj = (IntermediaireObject)errorObj;
					mailSubject	= "Entree intermediaire invalide : [" + assObj.getNomIntermediaire() + "]";
					mailMessage += assObj.getComments();

					String addressQStrng = "Select * from adresse where code_assureur = \'" + assObj.getCodeAssureur() + "\'";
					List<AddressObject> addrArr = asacSvc.queryAddress(addressQStrng);
					if (addrArr != null && addrArr.size() > 0)
					{
						AddressObject adrObj = (AddressObject)addrArr.get(0);
						InternetAddress [] internetAddress = InternetAddress.parse(adrObj.getEmail(), false);

						MimeMessage mimeMessage = getMimeMessage();

						// set the receiver addr
						mimeMessage.setRecipients(Message.RecipientType.TO, internetAddress);

						// set the message content
						mimeMessage.setText(mailMessage);

						// convert subject to utf8 format explicitly
						mimeMessage.setSubject(new String(mailSubject.getBytes(fileEncoding), "utf-8"), "utf-8");

						(new SendEmailThread(mimeMessage)).start();
					}
					else
					{
						LogMaster.logDebug("MessageControlThread:sendAttestation: Missing address for this code_assureur ["+assObj.getCodeAssureur()+"]", null);
					}
					*/
				}
				else
				if (errorObj instanceof RemorqueObject)
				{
					RemorqueObject assObj = (RemorqueObject)errorObj;
					if (addrObj == null)
					{
						String addressQStrng = "Select * from adresse where code_assureur = \'" + assObj.getCodeAssureur() + "\'";
						List<AddressObject> addrArr = asacSvc.queryAddress(addressQStrng);
						if (addrArr != null && addrArr.size() > 0)
						{
							addrObj = (AddressObject)addrArr.get(0);
						}
						else
						{
							LogMaster.logDebug("MessageControlThread:sendAssure: Missing address for this code_assureur ["+assObj.getCodeAssureur()+"]", null);
							return;
						}
					}

					assObj.setControlStatus(VINStat.CONTROL_STATUS_MESSAGE_SENT);
					asacSvc.addJournalRemorque(assObj);

					mailMessage += " -----------------------------------------------------\n";
					mailMessage	+= "Entree invalide pour remorque : [" + assObj.getImmatriculationRemorque() + "] \n";
					mailMessage += assObj.getComments() + "\n";
				}
				else
				if (errorObj instanceof SinistreObject)
				{
					SinistreObject assObj = (SinistreObject)errorObj;
					if (addrObj == null)
					{
						String addressQStrng = "Select * from adresse where code_assureur = \'" + assObj.getCodeAssureur() + "\'";
						List<AddressObject> addrArr = asacSvc.queryAddress(addressQStrng);
						if (addrArr != null && addrArr.size() > 0)
						{
							addrObj = (AddressObject)addrArr.get(0);
						}
						else
						{
							LogMaster.logDebug("MessageControlThread:sendAssure: Missing address for this code_assureur ["+assObj.getCodeAssureur()+"]", null);
							return;
						}
					}

					assObj.setControlStatus(VINStat.CONTROL_STATUS_MESSAGE_SENT);
					asacSvc.addJournalSinistre(assObj);

					mailMessage += " ----------------------------------------------------- \n";
					mailMessage	+= "Reference Sinistre invalide : [" + assObj.getReference() + "] \n";
					mailMessage += assObj.getComments() + "\n";
				}
				else
				if (errorObj instanceof VehiculeObject)
				{
					VehiculeObject assObj = (VehiculeObject)errorObj;
					if (addrObj == null)
					{
						String addressQStrng = "Select * from adresse where code_assureur = \'" + assObj.getCodeAssureur() + "\'";
						List<AddressObject> addrArr = asacSvc.queryAddress(addressQStrng);
						if (addrArr != null && addrArr.size() > 0)
						{
							addrObj = (AddressObject)addrArr.get(0);
						}
						else
						{
							LogMaster.logDebug("MessageControlThread:sendAssure: Missing address for this code_assureur ["+assObj.getCodeAssureur()+"]", null);
							return;
						}
					}

					assObj.setControlStatus(VINStat.CONTROL_STATUS_MESSAGE_SENT);
					asacSvc.addJournalVehicule(assObj);

					mailMessage += " -----------------------------------------------------\n";
					mailSubject	= "Entree invalide pour vehicule : [" + assObj.getImmatriculation() + "]\n";
					mailMessage += assObj.getComments() + "\n";
				}
				else
				if (errorObj instanceof VenteObject)
				{
					VenteObject assObj = (VenteObject)errorObj;
					if (addrObj == null)
					{
						String addressQStrng = "Select * from adresse where code_assureur = \'" + assObj.getCodeAssureur() + "\'";
						List<AddressObject> addrArr = asacSvc.queryAddress(addressQStrng);
						if (addrArr != null && addrArr.size() > 0)
						{
							addrObj = (AddressObject)addrArr.get(0);
						}
						else
						{
							LogMaster.logDebug("MessageControlThread:sendAssure: Missing address for this code_assureur ["+assObj.getCodeAssureur()+"]", null);
							return;
						}
					}

					assObj.setControlStatus(VINStat.CONTROL_STATUS_MESSAGE_SENT);
					asacSvc.addJournalVente(assObj);

					mailMessage += " ----------------------------------------------------- \n";
					mailMessage	+= "Entree invalide pour vente avec attestation : [" + assObj.getImmatriculation() + "] \n";
					mailMessage += assObj.getComments() + "\n";
				}
			}

			if (addrObj != null && addrObj.getEmail() != null)
			{
				InternetAddress [] internetAddress = InternetAddress.parse(addrObj.getEmail(), false);
				MimeMessage mimeMessage = getMimeMessage();
				// set the receiver addr
				mimeMessage.setRecipients(Message.RecipientType.TO, internetAddress);
				// set the message content
				mimeMessage.setText(mailMessage);
				// convert subject to utf8 format explicitly
				mimeMessage.setSubject(new String(mailSubject.getBytes(fileEncoding), "utf-8"), "utf-8");

				(new SendEmailThread(mimeMessage)).start();
			}
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
}


