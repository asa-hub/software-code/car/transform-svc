package com.vindata.asac.server;

import javax.mail.*;
import javax.mail.internet.*;

public class SendEmailThread extends Thread 
{
	private MimeMessage mm 		= null;
	public SendEmailThread(MimeMessage mm)
	{
		this.mm = mm;
	}
	public void run()
	{
		try
		{
			Transport.send(mm);
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
}
