package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class AssureControlThread extends Thread 
{
	private AsacDAOImpl asacSvc = null;
	private Connection myConn 	= null;
	public AssureControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
	}
	public void run()
	{
		try
		{
			myConn = ConnectionPool.newConnection();

			//String qstrng = "Select * from assure where c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\'";
			String qstrng = "Select * from assure where "; 
			qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
			List<AssureObject> assureArr = asacSvc.queryAssure(qstrng, myConn);
			if (assureArr != null && assureArr.size() > 0)
			{
				for(int l=0; l<assureArr.size(); l++)
				{
					AssureObject obj = (AssureObject)assureArr.get(l);
					if (obj != null)
					{
						if (l == 0 || (l > 20 == true && l%20 == 0))
						{
							System.err.println("["+l+"/"+assureArr.size()+"] Processing ASSURE \t\'" + obj.getCodeAssure()+"\' time = ["+Calendar.getInstance().getTime()+"]");
						}

						String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.ASSURE_ABBREV + "_" + obj.getAid();

						ArrayList errorArr = new ArrayList();
						if (isValid(obj, errorArr))
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
							obj.setComments("");
						}
						else
						{
							if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
							{
								obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);

								Object error = errorArr.get(0);	
								if (error != null) obj.setComments(error.toString());
								//LogMaster.logActivities("INVALID ENTRY\tASSURE\t\'"+obj.getCodeAssure()+ "\'\t\'" + obj.getNom()+"\'\t\'" + obj.getPrenom()+"\'\t\'"+obj.getComments()+"\'");
							}
						}

						obj.setDateMiseJour(Calendar.getInstance().getTime());

						int retval = asacSvc.updateAssure(obj, myConn);
						if (retval != VINStat.SUCCESS)
						{
							LogMaster.logActivities("UPDATE FAILED FOR ASSURE ["+obj.getCodeAssure()+"]");
						}
						else
						{
							asacSvc.addJournalAssure(obj);
						}

					}
				}
			}
			else
			{
				LogMaster.logActivities("NO PENDING TRANSFORMATION ASSURE AVAILABLE");
			}

			myConn.close();	
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private boolean isValid(AssureObject assureObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (assureObj != null)
		{
			if (assureObj.getCodeAssureur() == null || assureObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}

			String qstrng = "Select count(a.aid) from ref_assureur a where a.code_assureur = \'" + assureObj.getCodeAssureur() + "\'";
			double availCnt  = asacSvc.getQueryCount(qstrng, myConn);
			if (availCnt == 0)
			{
				valid = false;	
				errorStrng += VINStat.INVALID_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	/*
	private boolean isValidOLD(AssureObject assureObj, ArrayList errorArr)
	{
		boolean valid = false;
		String errorStrng = VINStat.DIVIDER;
		if (assureObj != null)
		{
			if (assureObj.getCodeAssure() == null || assureObj.getCodeAssure().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}

			if (assureObj.getQualite() == null || assureObj.getQualite().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_QUALITE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				if (assureObj.getQualite().toLowerCase().contains("physique"))
				{
					if (assureObj.getDateNaissance() == null)
					{
						valid = true;	
						errorStrng += VINStat.MISSING_NAISSANCE;
						errorStrng += VINStat.DIVIDER;
					}
					else
					{
						Calendar today = Calendar.getInstance();
						if (assureObj.getDateNaissance().getTime() > today.getTimeInMillis())
						{
							valid = true;	
							errorStrng += VINStat.FUTURE_DATE_NAISSANCE;
							errorStrng += VINStat.DIVIDER;
						}
					}

					if (assureObj.getNumeroPermis() == null || assureObj.getNumeroPermis().trim().length() == 0)
					{
						valid = true;	
						errorStrng += VINStat.MISSING_NUMERO_PERMIS;
						errorStrng += VINStat.DIVIDER;
					}

					if (assureObj.getCategoriePermis() == null || assureObj.getCategoriePermis().trim().length() == 0)
					{
						valid = true;	
						errorStrng += VINStat.MISSING_CATEGORIE_PERMIS;
						errorStrng += VINStat.DIVIDER;
					}

					if (assureObj.getDateDeliverance() == null)
					{
						valid = true;	
						errorStrng += VINStat.MISSING_DATE_DELIVERANCE;
						errorStrng += VINStat.DIVIDER;
					}

					if (assureObj.getNomPrenomConducteur() == null || assureObj.getNomPrenomConducteur().trim().length() == 0)
					{
						valid = true;	
						errorStrng += VINStat.MISSING_NOM_CONDUCTEUR;
						errorStrng += VINStat.DIVIDER;
					}

					if (assureObj.getDateNaissanceConducteur() == null)
					{
						valid = true;	
						errorStrng += VINStat.MISSING_NAISSANCE_CONDUCTEUR;
						errorStrng += VINStat.DIVIDER;
					}

				}
				else
				{
				}
			}

			if (assureObj.getNom() == null || assureObj.getNom().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_NAME;
				errorStrng += VINStat.DIVIDER;
			}

			if (assureObj.getProfession() == null || assureObj.getProfession().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_PROFESSION;
				errorStrng += VINStat.DIVIDER;
			}

			if (assureObj.getNumContribuable() == null || assureObj.getNumContribuable().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_NUMERO_CONTRIBUABLE;
				errorStrng += VINStat.DIVIDER;
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	*/
}
