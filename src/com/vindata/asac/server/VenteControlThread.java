package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class VenteControlThread extends Thread 
{
	private AsacDAOImpl asacSvc = null;
	private Connection myConn   = null;
	public VenteControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
	}
	public void run()
	{
		try
		{
			myConn = ConnectionPool.newConnection();

			String qstrng = "Select * from vente where "; 
			qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
			List<VenteObject> venteArr = asacSvc.queryVente(qstrng, myConn);
			if (venteArr != null && venteArr.size() > 0)
			{
				for(int l=0; l<venteArr.size(); l++)
				{
					VenteObject obj = (VenteObject)venteArr.get(l);
					if (obj != null)
					{
						ArrayList errorArr = new ArrayList();
						if (isValid(obj, errorArr))
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
							obj.setComments("");
						}
						else
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							//LogMaster.logActivities("INVALID ENTRY\tVENTE\t\'"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getImmatriculation()+"\'\t\'" + obj.getCodeAssure()+"\'\t\'" + obj.getCodeAssureur() + "\'\t\'"+obj.getComments()+"\'");
						}

						obj.setDateMiseJour(Calendar.getInstance().getTime());
						int retval = asacSvc.updateVente(obj, myConn);
						if (retval != VINStat.SUCCESS)
						{
							LogMaster.logActivities("UPDATE FAILED FOR VENTE ["+obj.getCodeAssure()+"]");
						}
						else
						{
							//LogMaster.logActivities("UPDATE SUCCEEDED FOR VENTE ["+obj.getCodeAssure()+"]");
						}

						asacSvc.addJournalVente(obj);
					}
				}
			}
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private boolean isValid(VenteObject venteObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (venteObj != null)
		{
			AttestationObject attestObj = null;
			if (venteObj.getNumeroAttestation() == null || venteObj.getNumeroAttestation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_NUMERO_ATTESTATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String aQStrng = "Select * from attestation where numero_attestation = \'" + venteObj.getNumeroAttestation() + "\'";
				List attArr = asacSvc.queryAttestation(aQStrng);
				if (attArr == null || attArr.size() == 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_NUMERO_ATTESTATION;
					errorStrng += VINStat.DIVIDER;
				}
				else
				{
					attestObj = (AttestationObject)attArr.get(0);
				}
			}

			if (venteObj.getImmatriculation() == null || venteObj.getImmatriculation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_IMMATRICULATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String vehiculeQStrng = "Select count(vid) from vehicule where immatriculation = \'" + venteObj.getImmatriculation() + "\'";
				String remorqueQStrng = "Select count(rid) from remorque where immatriculation_remorque = \'" + venteObj.getImmatriculation() + "\'";
				if (asacSvc.getQueryCount(vehiculeQStrng) <= 0 && asacSvc.getQueryCount(remorqueQStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (venteObj.getCodeAssure() == null || venteObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + venteObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng, myConn) <= 0)
				{
					valid = false;	
					//System.err.println(codeValidStrng + " ====> INVALID_CODE_ASSURE ");
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (venteObj.getCodeAssureur() == null || venteObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String qstrng = "Select count(a.aid) from ref_assureur a where a.code_assureur = \'" + venteObj.getCodeAssureur() + "\'";
				double availCnt  = asacSvc.getQueryCount(qstrng, myConn);
				if (availCnt == 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (venteObj.getCodeIntermediaire() != null && venteObj.getCodeIntermediaire().trim().length() > 0)
			{
				String qstrng = "Select * from intermediaire where code_intermediaire_dna = \'" + venteObj.getCodeIntermediaire() + "\'";
				List intArr  = asacSvc.queryIntermediaire(qstrng);
				if (intArr == null || intArr.size() == 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_INTERMEDIAIRE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
				else
				{
					IntermediaireObject obj = (IntermediaireObject)intArr.get(0);
					if (obj != null && (obj.getStatutIntermediaire() != null && obj.getStatutIntermediaire().toLowerCase().indexOf("autori") == -1))
					{
						if (obj.getStatutIntermediaire().toLowerCase().indexOf("caduc") != -1)
						{
							valid = false;	
							errorStrng += VINStat.INTERMEDIAIRE_STATUS_CADUC;
							errorStrng += VINStat.DIVIDER;
						}
						else
						if (obj.getStatutIntermediaire().toLowerCase().indexOf("susp") != -1)
						{
							java.util.Date susp_start = obj.getDateDebutSuspension();	
							if (susp_start == null)
							{
								valid = false;	
								errorStrng += VINStat.INTERMEDIAIRE_STATUS_SUSPENDED;
								errorStrng += VINStat.DIVIDER;
							}
							else
							{
								java.util.Date sales_date = attestObj.getDateEmission();
								if (sales_date == null)
								{
									valid = false;	
									errorStrng += VINStat.INVALID_NUMERO_ATTESTATION;
									errorStrng += VINStat.DIVIDER;
								}
								else
								{
									if (sales_date.getTime() > susp_start.getTime())
									{
										java.util.Date susp_end = obj.getDateFinSuspension();	
										if (susp_end == null || sales_date.getTime() < susp_end.getTime())
										{	
											valid = false;	
											errorStrng += VINStat.INTERMEDIAIRE_STATUS_SUSPENDED;
											errorStrng += VINStat.DIVIDER;
										}
									}
								}
							}
						}
					}
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
}


