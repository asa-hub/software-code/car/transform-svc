package com.vindata.asac.server;

import java.io.*;
import java.text.*;
import java.util.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

/** 
 * Entity Validator Class
 *
 * Reads the entries from the different entry tables 
 * Controls them for correctness 
 * Make entries into the different tables 
 */ 
public class ASACEntityValidator 
{
	private AsacDAOImpl asacSvc = null;
	private boolean stopThread  = false;
	public ASACEntityValidator()
	{
		this.asacSvc = new AsacDAOImpl();
	}
	public void startControlThread()
	{
		while (!stopThread)
		{
			// controler le client ++
			System.err.println("1. Starting control Assure +++++++++++++ ");
			controlAssure();

			// controler le vehicule;
			System.err.println("2. Starting control Vehicule +++++++++++++ ");
			controlVehicule();

			// controler remorque;
			System.err.println("3. Starting control Remorque +++++++++++++ ");
			controlRemorque();

			// controler attestation;
			System.err.println("4. Starting control Attestation +++++++++++++ ");
			controlAttestation();

			// controler intermediaire;
			System.err.println("5. Starting control Intermediaire +++++++++++++ ");
			controlIntermediaire();

			// controler vente;
			System.err.println("6. Starting control Vente +++++++++++++ ");
			controlVente();

			// controler vente;
			System.err.println("7. Starting control Sinistre +++++++++++++ ");
			controlSinistre();
			/*
			*/

			// convert time to minutes 
			int sleepTimeInMin = this.getSleepTime();
			long val = sleepTimeInMin * 60 * 1000;

			System.err.println("Going to sleep for [" + val + "] ["+ Calendar.getInstance().getTime()+"]");
			LogMaster.logActivities("Going to sleep for [" + val + "] ["+ Calendar.getInstance().getTime()+"]");

			try
			{
				Thread.sleep(val); 
			}
			catch(Exception ee)
			{
				ee.printStackTrace();
			}	

			System.err.println("Waking up @ ["+Calendar.getInstance().getTime()+"]");
			LogMaster.logActivities("Waking up @ ["+ Calendar.getInstance().getTime()+"]");
		}
	}
	public void stopControl()
	{
		this.stopThread = true;
	}
	public void startControl()
	{
		this.stopThread = false;
		this.startControlThread();
	}
	private void controlAssure()
	{
		try
		{
			AssureControlThread tst = new AssureControlThread(asacSvc);
			tst.start();
			tst.join();
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private void controlVehicule()
	{
		try
		{
			VehiculeControlThread tst = new VehiculeControlThread(asacSvc);
			tst.start();
			tst.join();
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private void controlRemorque()
	{
		try
		{
			RemorqueControlThread tst = new RemorqueControlThread(asacSvc);
			tst.start();
			tst.join();
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	/*
	private void controlRemorque()
	{
		//String qstrng = "Select * from remorque where c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\'";
		String qstrng = "Select * from remorque where "; 
		qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
		List<RemorqueObject> remorqueArr = asacSvc.queryRemorque(qstrng);
		if (remorqueArr != null && remorqueArr.size() > 0)
		{
			for(int l=0; l<remorqueArr.size(); l++)
			{
				RemorqueObject obj = (RemorqueObject)remorqueArr.get(l);
				if (obj != null)
				{
					if (l == 0 || (l > 20 == true && l%20 == 0))
					{
						System.err.println("["+l+"/"+remorqueArr.size()+"] Processing REMORQUE\t\'" + obj.getImmatriculationRemorque()+"\'");
					}

					String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.REMORQUE_ABBREV + "_" + obj.getRid();
					JournalObject jnrObj = new JournalObject();
					jnrObj.setReference(journalRef);

					ArrayList errorArr = new ArrayList();
					if (isValidRemorque(obj, errorArr))
					{
						jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
						//LogMaster.logActivities("VALID ENTRY\tREMORQUE\t"+obj.getCodeAssure()+ "\'\t\'" + obj.getImmatriculationRemorque()+"\'");
						//System.err.println("VALID ENTRY\tREMORQUE\t"+obj.getCodeAssure()+ "\'\t\'" + obj.getImmatriculationRemorque()+"\'");
					}
					else
					{
						if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
							jnrObj.setDescription(obj.getComments());
							// LogMaster.logActivities("INVALID ENTRY\tREMORQUE\t"+obj.getCodeAssure()+ "\'\t\'" + obj.getImmatriculationRemorque()+"\'\t\'"+obj.getComments()+"\'");
						}
					}

					jnrObj.setDateInscription(Calendar.getInstance().getTime());
					obj.setDateMiseJour(Calendar.getInstance().getTime());
					int retval = asacSvc.updateRemorque(obj);
					if (retval != VINStat.SUCCESS)
					{
						//LogMaster.logActivities("UPDATE FAILED FOR REMORQUE ["+obj.getCodeAssure()+"]");
					}
					else
					{
						//LogMaster.logActivities("UPDATE SUCCEEDED FOR REMORQUE ["+obj.getCodeAssure()+"]");
						//System.err.println("UPDATE SUCCEEDED FOR REMORQUE ["+obj.getCodeAssure()+"]");
					}

					if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournal(jnrObj);
				}
			}
		}
		else
		{
			//LogMaster.logActivities("[" + qstrng + "] : NO UNCONTROLLED REMORQUE AVAILABLE");
			System.err.println("NO PENDING REMORQUE AVAILABLE");
		}
	}
	private void controlAttestation()
	{
		//String qstrng = "Select * from attestation where c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\'";
		String qstrng = "Select * from attestation where "; 
		qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
		List<AttestationObject> attestationArr = asacSvc.queryAttestation(qstrng);
		if (attestationArr != null && attestationArr.size() > 0)
		{
			for(int l=0; l<attestationArr.size(); l++)
			{
				AttestationObject obj = (AttestationObject)attestationArr.get(l);
				if (obj != null)
				{
					if (l == 0 || (l > 20 == true && l%20 == 0))
					{
						System.err.println("["+l+"/"+attestationArr.size()+"] Processing Attestation\t\'" + obj.getNumeroAttestation()+"\'");
					}

					String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.ATTESTATION_ABBREV + "_" + obj.getAid();
					JournalObject jnrObj = new JournalObject();
					jnrObj.setReference(journalRef);

					ArrayList errorArr = new ArrayList();
					if (isValidAttestation(obj, errorArr))
					{
						jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
						//LogMaster.logActivities("VALID ENTRY\tATTESTATION\t"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getDateEmission()+"\'\t\'" + obj.getCouleur()+"\'");
						//System.err.println("VALID ENTRY\tATTESTATION\t"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getDateEmission()+"\'\t\'" + obj.getCouleur()+"\'");
					}
					else
					{
						if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
							jnrObj.setDescription(obj.getComments());
							// LogMaster.logActivities("INVALID ENTRY\tATTESTATION\t"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getDateEmission()+"\'\t\'" + obj.getCouleur()+"\'\t\'" + obj.getComments()+"\'");
						}
					}

					jnrObj.setDateInscription(Calendar.getInstance().getTime());
					obj.setDateMiseJour(Calendar.getInstance().getTime());
					int retval = asacSvc.updateAttestation(obj);
					if (retval != VINStat.SUCCESS)
					{
						// LogMaster.logActivities("UPDATE FAILED FOR ATTESTATION ["+obj.getCodeAssure()+"]");
					}
					else
					{
						//System.err.println("UPDATE SUCCEEDED FOR ATTESTATION ["+obj.getCodeAssure()+"]");
					}

					if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournal(jnrObj);
				}
			}
		}
		else
		{
			//LogMaster.logActivities("[" + qstrng + "] : NO UNCONTROLLED ATTESTATION AVAILABLE");
			System.err.println("NO PENDING ATTESTATION AVAILABLE");
		}
	}
	private void controlIntermediaire()
	{
		//String qstrng = "Select * from intermediaire where c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\'";
		String qstrng = "Select * from intermediaire where "; 
		qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
		List<IntermediaireObject> intermediaireArr = asacSvc.queryIntermediaire(qstrng);
		if (intermediaireArr != null && intermediaireArr.size() > 0)
		{
			for(int l=0; l<intermediaireArr.size(); l++)
			{
				IntermediaireObject obj = (IntermediaireObject)intermediaireArr.get(l);
				if (obj != null)
				{
					if (l == 0 || (l > 20 == true && l%20 == 0))
					{
						System.err.println("["+l+"/"+intermediaireArr.size()+"] Processing Intermediare\t\'" + obj.getNumContribuable()+"\'");
					}

					String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.INTERMEDIAIRE_ABBREV + "_" + obj.getIid();
					JournalObject jnrObj = new JournalObject();
					jnrObj.setReference(journalRef);

					ArrayList errorArr = new ArrayList();
					if (isValidIntermediaire(obj, errorArr))
					{
						jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
						//LogMaster.logActivities("VALID ENTRY\tINTERMEDIAIRE\t"+obj.getCodeIntermediaireDNA()+ "\'\t\'" + obj.getNumContribuable()+"\'\t\'" + obj.getNomIntermediaire()+"\'");
						//System.err.println("VALID ENTRY\tINTERMEDIAIRE\t"+obj.getCodeIntermediaireDNA()+ "\'\t\'" + obj.getNumContribuable()+"\'\t\'" + obj.getNomIntermediaire()+"\'");
					}
					else
					{
						if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);

							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
							jnrObj.setDescription(obj.getComments());
							//LogMaster.logActivities("INVALID ENTRY\tINTERMEDIAIRE\t"+obj.getCodeIntermediaireDNA()+ "\'\t\'" + obj.getNumContribuable()+"\'\t\'" + obj.getNomIntermediaire()+"\'\t\'" + obj.getComments() + "\'");
						}
					}

					obj.setDateMiseJour(Calendar.getInstance().getTime());
					int retval = asacSvc.updateIntermediaire(obj);
					if (retval != VINStat.SUCCESS)
					{
						LogMaster.logActivities("UPDATE FAILED FOR INTERMEDIAIRE ["+obj.getNumContribuable()+"]");
					}
					else
					{
						//System.err.println("UPDATE SUCCEEDED FOR INTERMEDIAIRE ["+obj.getNumContribuable()+"]");
					}

					//asacSvc.addJournal(jnrObj);
					if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournal(jnrObj);
				}
			}
		}
		else
		{
			//LogMaster.logActivities("[" + qstrng + "] : NO UNCONTROLLED INTERMEDIAIRE AVAILABLE");
			System.err.println("NO PENDING INTERMEDIAIRE AVAILABLE");
		}
	}
	private void controlVente()
	{
		String qstrng = "Select * from vente where "; 
		qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
		List<VenteObject> venteArr = asacSvc.queryVente(qstrng);
		if (venteArr != null && venteArr.size() > 0)
		{
			for(int l=0; l<venteArr.size(); l++)
			{
				VenteObject obj = (VenteObject)venteArr.get(l);
				if (obj != null)
				{
					if (l == 0 || (l > 20 == true && l%20 == 0))
					{
						System.err.println("["+l+"/"+venteArr.size()+"] Processing Vente\t\'" + obj.getNumeroAttestation()+"\'");
					}

					String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.VENTE_ABBREV + "_" + obj.getVid();
					JournalObject jnrObj = new JournalObject();
					jnrObj.setReference(journalRef);

					ArrayList errorArr = new ArrayList();
					if (isValidVente(obj, errorArr))
					{
						jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
					//	LogMaster.logActivities("VALID ENTRY\tVENTE\t\'"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getImmatriculation()+"\'\t\'" + obj.getCodeAssure()+"\'\t\'" + obj.getCodeAssureur() + "\'");
						//System.err.println("VALID ENTRY\tVENTE\t\'"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getImmatriculation()+"\'\t\'" + obj.getCodeAssure()+"\'\t\'" + obj.getCodeAssureur() + "\'");
					}
					else
					{
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
						Object error = errorArr.get(0);	
						if (error != null) obj.setComments(error.toString());
						//LogMaster.logActivities("INVALID ENTRY\tVENTE\t\'"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getImmatriculation()+"\'\t\'" + obj.getCodeAssure()+"\'\t\'" + obj.getCodeAssureur() + "\'\t\'"+obj.getComments()+"\'");
						jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
						jnrObj.setDescription(obj.getComments());
					}

					jnrObj.setDateInscription(Calendar.getInstance().getTime());
					obj.setDateMiseJour(Calendar.getInstance().getTime());
					int retval = asacSvc.updateVente(obj);
					if (retval != VINStat.SUCCESS)
					{
						//LogMaster.logActivities("UPDATE FAILED FOR VENTE ["+obj.getCodeAssure()+"]");
					}
					else
					{
						//System.err.println("UPDATE SUCCEEDED FOR VENTE ["+obj.getCodeAssure()+"]");
					}

					asacSvc.addJournal(jnrObj);
				}
			}
		}
		else
		{
			//LogMaster.logActivities("[" + qstrng + "] : NO UNCONTROLLED VENTE AVAILABLE");
			System.err.println("NO PENDING VENTE AVAILABLE");
		}
	}
	private void controlSinistre()
	{
		String qstrng = "Select * from sinistre where "; 
		qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
		List<SinistreObject> sinistreArr = asacSvc.querySinistre(qstrng);
		if (sinistreArr != null && sinistreArr.size() > 0)
		{
			for(int l=0; l<sinistreArr.size(); l++)
			{
				SinistreObject obj = (SinistreObject)sinistreArr.get(l);
				if (obj != null)
				{
					if (l == 0 || (l > 20 == true && l%20 == 0))
					{
						System.err.println("["+l+"/"+sinistreArr.size()+"] Processing Sinistre\t\'" + obj.getNumeroAttestation()+"\'");
					}

					String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.SINISTRE_ABBREV + "_" + obj.getSid();
					JournalObject jnrObj = new JournalObject();
					jnrObj.setReference(journalRef);

					ArrayList errorArr = new ArrayList();
					if (isValidSinistre(obj, errorArr))
					{
						jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
						//LogMaster.logActivities("VALID ENTRY\tSINISTRE\t"+obj.getReference()+ "\'\t\'" + obj.getNumeroAttestation()+"\'\t\'" + obj.getDateSurvenance()+"\'");
						//System.err.println("VALID ENTRY\tSINISTRE\t"+obj.getReference()+ "\'\t\'" + obj.getNumeroAttestation()+"\'\t\'" + obj.getDateSurvenance()+"\'");
					}
					else
					{
						if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							//LogMaster.logActivities("INVALID ENTRY\tSINISTRE\t"+obj.getReference()+ "\'\t\'" + obj.getNumeroAttestation()+"\'\t\'" + obj.getDateSurvenance()+"\'\t\'"+obj.getComments()+"\'");
							jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
							jnrObj.setDescription(obj.getComments());
						}
					}

					jnrObj.setDateInscription(Calendar.getInstance().getTime());
					obj.setDateMiseJour(Calendar.getInstance().getTime());
					int retval = asacSvc.updateSinistre(obj);
					if (retval != VINStat.SUCCESS)
					{
						//LogMaster.logActivities("UPDATE FAILED FOR SINISTRE ["+obj.getReference()+"]");
					}
					else
					{
						//System.err.println("UPDATE SUCCEEDED FOR SINISTRE ["+obj.getReference()+"]");
					}

					if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournal(jnrObj);
				}
			}
		}
		else
		{
			//LogMaster.logActivities("[" + qstrng + "] : NO UNCONTROLLED SINISTRE AVAILABLE");
			System.err.println("NO PENDING SINISTRE AVAILABLE");
		}
	}
	*/
	private void controlAttestation()
	{
		try
		{
			AttestationControlThread tst = new AttestationControlThread(asacSvc);
			tst.start();
			tst.join();
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private void controlIntermediaire()
	{
		try
		{
			IntermediaireControlThread tst = new IntermediaireControlThread(asacSvc);
			tst.start();
			tst.join();
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private void controlVente()
	{
		try
		{
			VenteControlThread tst = new VenteControlThread(asacSvc);
			tst.start();
			tst.join();
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private void controlSinistre()
	{
		try
		{
			SinistreControlThread tst = new SinistreControlThread(asacSvc);
			tst.start();
			tst.join();
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	public static void main(String [] args)
	{
		ASACEntityValidator v = new ASACEntityValidator();	
		v.startControlThread();
	}
	private int getSleepTime()
	{
		int sleep = 0;

		try
		{
			//File myfile = new File(VINStat.fileDBName);
			File myfile = new File(VINStat.fileDBName);
			if (myfile.exists() == false)
			{
				myfile = new File("./hubasactransform.properties");
			}
			
			FileInputStream infile = new FileInputStream(myfile);
			Properties prop = new Properties();
			prop.load(infile);

			String sleepTime = prop.getProperty("timeframe", "60");
			if (sleepTime != null && sleepTime.trim().length() > 0)
			{
				Integer val = new Integer(sleepTime);
				sleep = val.intValue();
			}

			infile.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return sleep;
	}
	private boolean isValidAssure(AssureObject assureObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (assureObj != null)
		{
			if (assureObj.getCodeAssureur() == null || assureObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}

			String qstrng = "Select count(a.aid) from ref_assureur a where a.code_assureur = \'" + assureObj.getCodeAssureur() + "\'";
			double availCnt  = asacSvc.getQueryCount(qstrng);
			if (availCnt == 0)
			{
				valid = false;	
				errorStrng += VINStat.INVALID_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	private boolean isValidVehicule(VehiculeObject vehiculeObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (vehiculeObj != null)
		{
			if (vehiculeObj.getCodeAssure() == null || vehiculeObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + vehiculeObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getCodeAssureur() == null || vehiculeObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from ref_assureur where code_assureur = \'" + vehiculeObj.getCodeAssureur() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	private boolean isValidRemorque(RemorqueObject remorqueObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (remorqueObj != null)
		{
			if (remorqueObj.getCodeAssure() == null || remorqueObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + remorqueObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (remorqueObj.getCodeAssureur() == null || remorqueObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from ref_assureur where code_assureur = \'" + remorqueObj.getCodeAssureur() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (remorqueObj.getImmatriculationRemorque() == null || remorqueObj.getImmatriculationRemorque().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_IMMATRICULATION_REMORQUE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(vid) from vehicule where (immatriculation_remorque = \'" + remorqueObj.getImmatriculationRemorque() + "\' or immatriculation = \'" + remorqueObj.getImmatriculationRemorque() + "\')";
				double retval = asacSvc.getQueryCount(codeValidStrng);
				if (retval <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;

					System.err.println(codeValidStrng + " ----> ["+retval+"]");
				}
			}

			if (valid == false) errorArr.add(errorStrng);
		}

		return valid;
	}
	private boolean isValidAttestation(AttestationObject attestationObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (attestationObj != null)
		{
			if (attestationObj.getImmatriculation() == null || attestationObj.getImmatriculation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_IMMATRICULATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(vid) from vehicule where immatriculation = \'" + attestationObj.getImmatriculation() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getCodeAssure() == null || attestationObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + attestationObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getCodeAssureur() == null || attestationObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from ref_assureur where code_assureur = \'" + attestationObj.getCodeAssureur() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	private boolean isValidIntermediaire(IntermediaireObject intermediaireObj, ArrayList errorArr)
	{
		return true;
	}
	/*
	private boolean isValidIntermediaireOLD(IntermediaireObject intermediaireObj, ArrayList errorArr)
	{
		boolean valid = false;
		String errorStrng = VINStat.DIVIDER;
		if (intermediaireObj != null)
		{
			if (intermediaireObj.getIdentifiantDNA() <= 0)
			{
				if (intermediaireObj.getTypeAssureur() == null  || intermediaireObj.getTypeAssureur().trim().length() == 0 || 
					(intermediaireObj.getTypeAssureur().toUpperCase().contains(VINStat.INSURANCE_TYPE_COURTIER) == false &&
					 intermediaireObj.getTypeAssureur().toUpperCase().contains(VINStat.INSURANCE_TYPE_AGENT_GENERAL) == false))
				{
					valid = true;	
					errorStrng += VINStat.MISSING_IDENTIFIANT_DNA;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (intermediaireObj.getTypeAssureur() == null || intermediaireObj.getTypeAssureur().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_TYPE_ASSUREUR;
				errorStrng += VINStat.DIVIDER;
			}

			if (intermediaireObj.getNomIntermediaire() == null || intermediaireObj.getNomIntermediaire().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_NOM_INTERMEDIAIRE;
				errorStrng += VINStat.DIVIDER;
			}

			if (intermediaireObj.getTelephone() == null || intermediaireObj.getTelephone().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_TELEPHONE;
				errorStrng += VINStat.DIVIDER;
			}

			if (intermediaireObj.getVille() == null || intermediaireObj.getVille().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_VILLE;
				errorStrng += VINStat.DIVIDER;
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	*/
	private boolean isValidVente(VenteObject venteObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (venteObj != null)
		{
			if (venteObj.getNumeroAttestation() == null || venteObj.getNumeroAttestation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_NUMERO_ATTESTATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from attestation where numero_attestation = \'" + venteObj.getNumeroAttestation() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_NUMERO_ATTESTATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (venteObj.getImmatriculation() == null || venteObj.getImmatriculation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_IMMATRICULATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(vid) from vehicule where immatriculation = \'" + venteObj.getImmatriculation() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (venteObj.getCodeAssure() == null || venteObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + venteObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (venteObj.getCodeAssureur() == null || venteObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}

			String qstrng = "Select count(a.aid) from ref_assureur a where a.code_assureur = \'" + venteObj.getCodeAssureur() + "\'";
			double availCnt  = asacSvc.getQueryCount(qstrng);
			if (availCnt == 0)
			{
				valid = false;	
				errorStrng += VINStat.INVALID_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	private boolean isValidSinistre(SinistreObject sinistreObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (sinistreObj != null)
		{
			if (sinistreObj.getNumeroAttestation() == null || sinistreObj.getNumeroAttestation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_NUMERO_ATTESTATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from attestation where numero_attestation = \'" + sinistreObj.getNumeroAttestation() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_NUMERO_ATTESTATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (sinistreObj.getCodeAssureur() == null || sinistreObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String qstrng = "Select count(a.aid) from ref_assureur a where a.code_assureur = \'" + sinistreObj.getCodeAssureur() + "\'";
				double availCnt  = asacSvc.getQueryCount(qstrng);
				if (availCnt == 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
}
