package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class AttestationControlThread extends Thread 
{
	private AsacDAOImpl asacSvc = null;
    private LogMaster logMaster = null;
	public AttestationControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
        logMaster = LogMaster.getInstance();
	}
	public void run()
	{
		//String qstrng = "Select * from attestation where c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\'";
		String qstrng = "Select * from attestation where "; 
		qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
		List<AttestationObject> attestationArr = asacSvc.queryAttestation(qstrng);
		if (attestationArr != null && attestationArr.size() > 0)
		{
			for(int l=0; l<attestationArr.size(); l++)
			{
				AttestationObject obj = (AttestationObject)attestationArr.get(l);
				if (obj != null)
				{
					String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.ATTESTATION_ABBREV + "_" + obj.getAid();
					JournalObject jnrObj = new JournalObject();
					jnrObj.setReference(journalRef);

					ArrayList errorArr = new ArrayList();
					if (isValid(obj, errorArr))
					{
						jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
						obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
						obj.setComments("");
						//LogMaster.logActivities("VALID ENTRY\tATTESTATION\t"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getDateEmission()+"\'\t\'" + obj.getCouleur()+"\'");
					}
					else
					{
						if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
							jnrObj.setDescription(obj.getComments());
							//LogMaster.logActivities("INVALID ENTRY\tATTESTATION\t"+obj.getNumeroAttestation()+ "\'\t\'" + obj.getDateEmission()+"\'\t\'" + obj.getCouleur()+"\'\t\'" + obj.getComments()+"\'");
						}
					}

					jnrObj.setDateInscription(Calendar.getInstance().getTime());
					obj.setDateMiseJour(Calendar.getInstance().getTime());
					int retval = asacSvc.updateAttestation(obj);
					if (retval != VINStat.SUCCESS)
					{
						LogMaster.logActivities("UPDATE FAILED FOR ATTESTATION ["+obj.getCodeAssure()+"]");
					}
					else
					{
						if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournalAttestation(obj);
						//LogMaster.logActivities("UPDATE SUCCEEDED FOR ATTESTATION ["+obj.getCodeAssure()+"]");
					}

				}
			}
		}
	}
	private boolean isValid(AttestationObject attestationObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (attestationObj != null)
		{
			if (attestationObj.getImmatriculation() == null || attestationObj.getImmatriculation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_IMMATRICULATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String vehiculeQStrng = "Select count(vid) from vehicule where immatriculation = \'" + attestationObj.getImmatriculation() + "\'";
				String remorqueQStrng = "Select count(rid) from remorque where immatriculation_remorque = \'" + attestationObj.getImmatriculation() + "\'";
				if (asacSvc.getQueryCount(vehiculeQStrng) <= 0 && asacSvc.getQueryCount(remorqueQStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getCodeAssure() == null || attestationObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + attestationObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getCodeAssureur() == null || attestationObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from ref_assureur where code_assureur = \'" + attestationObj.getCodeAssureur() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getStatut() != null && attestationObj.getStatut().equalsIgnoreCase(VINStat.STATUT_VENDUE))
			{
				List<VenteObject> attArr = asacSvc.queryVente("Select * from vente where numero_attestation = \'" + attestationObj.getNumeroAttestation() + "\'");
				if (attArr == null || attArr.size() == 0)
				{
					valid = false;	
					errorStrng += VINStat.ATTESTATION_MISSING_VENTE;
					errorStrng += VINStat.DIVIDER;
				}
				else
				{
					VenteObject venteObj = attArr.get(0);
					if (venteObj != null && venteObj.getCodeIntermediaire() != null && venteObj.getCodeIntermediaire().trim().length() > 0)
					{
						String qstrng = "Select * from intermediaire where code_intermediaire_dna = \'" + venteObj.getCodeIntermediaire() + "\'";
						List intArr  = asacSvc.queryIntermediaire(qstrng);
						if (intArr == null || intArr.size() == 0)
						{
							valid = false;	
							errorStrng += VINStat.INVALID_ASSUREUR_CODE;
							errorStrng += VINStat.DIVIDER;
						}
						else
						{
							IntermediaireObject obj = (IntermediaireObject)intArr.get(0);
							if (obj != null && (obj.getStatutIntermediaire() != null && obj.getStatutIntermediaire().toLowerCase().indexOf("autori") == -1))
							{
								if (obj.getStatutIntermediaire().toLowerCase().indexOf("caduc") != -1)
								{
									valid = false;	
									errorStrng += VINStat.INTERMEDIAIRE_STATUS_CADUC;
									errorStrng += VINStat.DIVIDER;
								}
								else
								if (obj.getStatutIntermediaire().toLowerCase().indexOf("susp") != -1)
								{
									java.util.Date susp_start = obj.getDateDebutSuspension();	
									if (susp_start == null)
									{
										valid = false;	
										errorStrng += VINStat.INTERMEDIAIRE_STATUS_SUSPENDED;
										errorStrng += VINStat.DIVIDER;
									}
									else
									{
										java.util.Date sales_date = attestationObj.getDateEmission();
										if (sales_date == null)
										{
											valid = false;	
											errorStrng += VINStat.INVALID_NUMERO_ATTESTATION;
											errorStrng += VINStat.DIVIDER;
										}
										else
										{
											if (sales_date.getTime() > susp_start.getTime())
											{
												java.util.Date susp_end = obj.getDateFinSuspension();	
												if (susp_end == null || sales_date.getTime() < susp_end.getTime())
												{	
													valid = false;	
													errorStrng += VINStat.INTERMEDIAIRE_STATUS_SUSPENDED;
													errorStrng += VINStat.DIVIDER;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	/*
	private boolean isValid(AttestationObject attestationObj, ArrayList errorArr)
	{
		boolean valid = false;
		String errorStrng = VINStat.DIVIDER;
		if (attestationObj != null)
		{
			if (attestationObj.getNumeroAttestation() == null || attestationObj.getNumeroAttestation().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_NUMERO_ATTESTATION;
				errorStrng += VINStat.DIVIDER;
			}

			if (attestationObj.getNumeroPolice() == null || attestationObj.getNumeroPolice().trim().length() == 0)
			{
				if (attestationObj.getStatut() != null && 
					(attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_VENDUE) == true || 
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_SUSPENDUE) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_REMISE_EN_VIGUEUR) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_RESILIEE) == true))
				{
					valid = true;	
					errorStrng += VINStat.MISSING_NUMERO_POLICE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getDateEmission() == null)
			{
				if (attestationObj.getStatut() != null && 
					(attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_VENDUE) == true || 
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_SUSPENDUE) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_REMISE_EN_VIGUEUR) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_RESILIEE) == true))
				{
					valid = true;	
					errorStrng += VINStat.MISSING_DATE_EMISSION;
					errorStrng += VINStat.DIVIDER;
				}
			}
			else
			{
				Calendar today = Calendar.getInstance();
				if (attestationObj.getDateEmission().getTime() > today.getTimeInMillis())
				{
					valid = true;	
					errorStrng += VINStat.FUTURE_DATE_EMISSION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getDateEffet() == null)
			{
				if (attestationObj.getStatut() != null && 
					(attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_VENDUE) == true || 
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_SUSPENDUE) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_REMISE_EN_VIGUEUR) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_RESILIEE) == true))
				{
					valid = true;	
					errorStrng += VINStat.MISSING_DATE_EFFET;
					errorStrng += VINStat.DIVIDER;
				}
			}
			else
			{
				Calendar today = Calendar.getInstance();
				if (attestationObj.getDateEffet().getTime() > today.getTimeInMillis())
				{
					valid = true;	
					errorStrng += VINStat.FUTURE_DATE_EFFET;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getDateEcheance() == null)
			{
				if (attestationObj.getStatut() != null && 
					(attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_VENDUE) == true || 
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_SUSPENDUE) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_REMISE_EN_VIGUEUR) == true ||
					 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_RESILIEE) == true))
				{
					valid = true;	
					errorStrng += VINStat.MISSING_DATE_ECHEANCE;
					errorStrng += VINStat.DIVIDER;
				}
			}
			else
			{
				Calendar today = Calendar.getInstance();
				if (attestationObj.getDateEcheance().getTime() > today.getTimeInMillis())
				{
					valid = true;	
					errorStrng += VINStat.FUTURE_DATE_ECHEANCE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getCouleur() == null || 
				attestationObj.getCouleur().trim().length() == 0 ||
				( attestationObj.getCouleur().toUpperCase().contains(VINStat.COULEUR_MARRON) == false &&
				  attestationObj.getCouleur().toUpperCase().contains(VINStat.COULEUR_VERTE) == false &&
				  attestationObj.getCouleur().toUpperCase().contains(VINStat.COULEUR_BLEUE) == false))
			{
				valid = true;	
				errorStrng += VINStat.MISSING_COULEUR;
				errorStrng += VINStat.DIVIDER;
			}

			if (attestationObj.getStatut() == null || 
				(attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_VENDUE) == false && 
				 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_EN_STOCK) == false &&
				 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_MISE_AU_REBUS) == false &&
				 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_ANNULEE) == false && 
				 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_RESILIEE) == false && 
				 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_SUSPENDUE) == false && 
				 attestationObj.getStatut().toUpperCase().contains(VINStat.STATUT_REMISE_EN_VIGUEUR) == false))
			{
				valid = true;	
				errorStrng += VINStat.MISSING_STATUT;
				errorStrng += VINStat.DIVIDER;
			}

			if (attestationObj.getZoneCirculation() == null || 
				(attestationObj.getStatut().toUpperCase().contains("A") == false && 
				 attestationObj.getStatut().toUpperCase().contains("B") == false &&
				 attestationObj.getStatut().toUpperCase().contains("C") == false))
			{
				valid = true;	
				errorStrng += VINStat.MISSING_ZONE_CIRCULATION;
				errorStrng += VINStat.DIVIDER;
			}

			if (attestationObj.getImmatriculation() == null || attestationObj.getImmatriculation().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_IMMATRICULATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(vid) from vehicule where immatriculation = \'" + attestationObj.getImmatriculation() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = true;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (attestationObj.getCodeAssure() == null || attestationObj.getCodeAssure().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + attestationObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = true;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}


			errorArr.add(errorStrng);
		}

		return valid;
	}
*/
}


