package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class VehiculeControlThread extends Thread 
{
	private AsacDAOImpl asacSvc = null;
    private LogMaster logMaster = null;
	private Connection myConn   = null;
	public VehiculeControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
	}
	public void run()
	{
		try
		{
			myConn = ConnectionPool.newConnection();

			String qstrng = "Select * from vehicule where "; 
			qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
			List<VehiculeObject> vehiculeArr = asacSvc.queryVehicule(qstrng, myConn);
			if (vehiculeArr != null && vehiculeArr.size() > 0)
			{
				for(int l=0; l<vehiculeArr.size(); l++)
				{
					VehiculeObject obj = (VehiculeObject)vehiculeArr.get(l);
					if (obj != null)
					{
						String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.VEHICLE_ABBREV + "_" + obj.getVid();
						JournalObject jnrObj = new JournalObject();
						jnrObj.setReference(journalRef);

						ArrayList errorArr = new ArrayList();
						if (isValid(obj, errorArr))
						{
							jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
							obj.setComments("");
							//LogMaster.logActivities("VALID ENTRY\tVEHICULES\t"+obj.getCodeAssure()+ "\'\t\'" + obj.getImmatriculation()+"\'");
						}
						else
						{
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);

							Object error = errorArr.get(0);	
							if (error != null) obj.setComments(error.toString());
							//LogMaster.logActivities("INVALID ENTRY\tVEHICULES\t"+obj.getCodeAssure()+ "\'\t\'" + obj.getImmatriculation()+"\'\t\'"+obj.getComments()+"\'");
							jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
							jnrObj.setDescription(obj.getComments());
						}

						jnrObj.setDateInscription(Calendar.getInstance().getTime());
						obj.setDateMiseJour(Calendar.getInstance().getTime());
						int retval = asacSvc.updateVehicule(obj, myConn);
						if (retval != VINStat.SUCCESS)
						{
							LogMaster.logActivities("UPDATE FAILED FOR VEHICLE ["+obj.getCodeAssure()+"]");
						}
						else
						{
							//LogMaster.logActivities("UPDATE SUCCEEDED FOR VEHICLE ["+obj.getCodeAssure()+"]");
						}

						if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournalVehicule(obj);
					}
				}
			}
			else
			{
				LogMaster.logActivities("[" + qstrng + "] : NO UNCONTROLLED VEHICULES AVAILABLE");
			}
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private boolean isValid(VehiculeObject vehiculeObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (vehiculeObj != null)
		{
			if (vehiculeObj.getCodeAssure() == null || vehiculeObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + vehiculeObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng, myConn) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getCodeAssureur() == null || vehiculeObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from ref_assureur where code_assureur = \'" + vehiculeObj.getCodeAssureur() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng, myConn) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	/*
	private boolean isValidOLD(VehiculeObject vehiculeObj, ArrayList errorArr)
	{
		boolean valid = false;
		String errorStrng = VINStat.DIVIDER;
		if (vehiculeObj != null)
		{
			if (vehiculeObj.getCodeAssure() == null || vehiculeObj.getCodeAssure().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(cid) from assure where code_assure = \'" + vehiculeObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = true;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() == null || vehiculeObj.getUsage().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_USAGE;
				errorStrng += VINStat.DIVIDER;
			}

			if (vehiculeObj.getUsage() != null && vehiculeObj.getUsage().contains("6") == false)
			{
				if (vehiculeObj.getPremiereDateMiseCirculation() == null)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_MISE_CIRCULATION;
					errorStrng += VINStat.DIVIDER;
				}

				if (vehiculeObj.getChassis() == null || vehiculeObj.getChassis().trim().length() == 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_CHASSIS;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() != null && 
				( vehiculeObj.getUsage().contains("1")  == true || 
				  vehiculeObj.getUsage().contains("2")  == true || 
				  vehiculeObj.getUsage().contains("3")  == true || 
				  vehiculeObj.getUsage().contains("4a") == true || 
				  vehiculeObj.getUsage().contains("4b") == true || 
				  vehiculeObj.getUsage().contains("4c") == true || 
				  vehiculeObj.getUsage().contains("7a") == true || 
				  vehiculeObj.getUsage().contains("7b") == true || 
				  vehiculeObj.getUsage().contains("8")  == true || 
				  vehiculeObj.getUsage().contains("9")  == true || 
				  vehiculeObj.getUsage().contains("10") == true))
			{
				if (vehiculeObj.getSourceEnergie() == null || vehiculeObj.getSourceEnergie().trim().length() == 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_SOURCE_ENERGIE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() != null && 
				( vehiculeObj.getUsage().contains("4a")  == true || 
				  vehiculeObj.getUsage().contains("4b") == true))
			{
				if (vehiculeObj.getNombreDePlaces() <= 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_NUMBER_PLACES;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() != null && vehiculeObj.getUsage().contains("7b") == true)
			{
				if (vehiculeObj.getDoubleCommande() < 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_DOUBLE_COMMANDES;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() != null && vehiculeObj.getUsage().contains("6") == true)
			{
				if (vehiculeObj.getResponsabiliteCivile() < 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_RESPONSABILITE_CIVILE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() != null && vehiculeObj.getUsage().contains("8") == true)
			{
				if (vehiculeObj.getUtilitaire() < 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_UTILITAIRE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() != null && vehiculeObj.getUsage().contains("9") == true)
			{
				if (vehiculeObj.getTypeEngin() < 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_TYPE_ENGIN;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getUsage() != null && 
				(vehiculeObj.getUsage().contains("2") == true || 
				 vehiculeObj.getUsage().contains("3") == true))
			{
				if (vehiculeObj.getPoids() < 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_POIDS;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getImmatriculation() == null || vehiculeObj.getImmatriculation().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_IMMATRICULATION;
				errorStrng += VINStat.DIVIDER;
			}

			if (vehiculeObj.getPuissanceFiscale() <= 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_PUISSANCE_FISCALE;
				errorStrng += VINStat.DIVIDER;
			}

			if (vehiculeObj.getSemiRemorque() == null || vehiculeObj.getSemiRemorque().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_SEMI_REMORQUE;
				errorStrng += VINStat.DIVIDER;
			}

			if (vehiculeObj.getPoids() > 3.5)
			{
				if (vehiculeObj.getChargeUtile() == null || vehiculeObj.getChargeUtile().trim().length() == 0)
				{
					valid = true;	
					errorStrng += VINStat.MISSING_CHARGE_UTILE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (vehiculeObj.getImmatriculationSemiRemorque() == null || vehiculeObj.getImmatriculationSemiRemorque().trim().length() == 0)
			{
				if (vehiculeObj.getSemiRemorque() != null && 
				   (vehiculeObj.getSemiRemorque().contains("1") == true || vehiculeObj.getSemiRemorque().contains("2") == true))
				{
					valid = true;	
					errorStrng += VINStat.MISSING_IMMATRICULATION_SEMI_REMORQUE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
*/
}
