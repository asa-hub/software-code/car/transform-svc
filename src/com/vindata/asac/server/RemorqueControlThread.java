package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class RemorqueControlThread extends Thread 
{
	private AsacDAOImpl asacSvc = null;
    private LogMaster logMaster = null;
	private Connection myConn   = null;
	public RemorqueControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
        logMaster = LogMaster.getInstance();
	}
	public void run()
	{
		try
		{
			myConn = ConnectionPool.newConnection();

			//String qstrng = "Select * from remorque where c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\'";
			String qstrng = "Select * from remorque where "; 
			qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
			List<RemorqueObject> remorqueArr = asacSvc.queryRemorque(qstrng, myConn);
			if (remorqueArr != null && remorqueArr.size() > 0)
			{
				for(int l=0; l<remorqueArr.size(); l++)
				{
					RemorqueObject obj = (RemorqueObject)remorqueArr.get(l);
					if (obj != null)
					{
						String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.REMORQUE_ABBREV + "_" + obj.getRid();
						JournalObject jnrObj = new JournalObject();
						jnrObj.setReference(journalRef);

						ArrayList errorArr = new ArrayList();
						if (isValid(obj, errorArr))
						{
							jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
							obj.setComments("");
							//LogMaster.logActivities("VALID ENTRY\tREMORQUE\t"+obj.getCodeAssure()+ "\'\t\'" + obj.getImmatriculationRemorque()+"\'");
						}
						else
						{
							if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
							{
								obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
								Object error = errorArr.get(0);	
								if (error != null) obj.setComments(error.toString());
								jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
								jnrObj.setDescription(obj.getComments());
								//LogMaster.logActivities("INVALID ENTRY\tREMORQUE\t"+obj.getCodeAssure()+ "\'\t\'" + obj.getImmatriculationRemorque()+"\'\t\'"+obj.getComments()+"\'");
							}
						}

						jnrObj.setDateInscription(Calendar.getInstance().getTime());
						obj.setDateMiseJour(Calendar.getInstance().getTime());
						int retval = asacSvc.updateRemorque(obj, myConn);
						if (retval != VINStat.SUCCESS)
						{
							LogMaster.logActivities("UPDATE FAILED FOR REMORQUE ["+obj.getCodeAssure()+"]");
						}
						else
						{
							if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournalRemorque(obj);
							//LogMaster.logActivities("UPDATE SUCCEEDED FOR REMORQUE ["+obj.getCodeAssure()+"]");
						}

					}
				}
			}
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private boolean isValid(RemorqueObject remorqueObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (remorqueObj != null)
		{
			if (remorqueObj.getCodeAssure() == null || remorqueObj.getCodeAssure().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from assure where code_assure = \'" + remorqueObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng, myConn) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (remorqueObj.getCodeAssureur() == null || remorqueObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from ref_assureur where code_assureur = \'" + remorqueObj.getCodeAssureur() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng, myConn) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (remorqueObj.getImmatriculationRemorque() == null || remorqueObj.getImmatriculationRemorque().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_IMMATRICULATION_REMORQUE;
				errorStrng += VINStat.DIVIDER;
			}
			/*
			 * CAN EXIST ALONE APPARENTLY
			 *
			else
			{
				String codeValidStrng = "Select count(vid) from vehicule where (immatriculation_remorque = \'" + remorqueObj.getImmatriculationRemorque() + "\' or immatriculation = \'" + remorqueObj.getImmatriculationRemorque() + "\')";
				double retval = asacSvc.getQueryCount(codeValidStrng, myConn);
				if (retval <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;

					System.err.println(codeValidStrng + " ----> ["+retval+"]");
				}
			}
			*/

			if (valid == false) errorArr.add(errorStrng);
		}

		return valid;
	}
	/*
	private boolean isValid(RemorqueObject remorqueObj, ArrayList errorArr)
	{
		boolean valid = false;
		String errorStrng = VINStat.DIVIDER;
		if (remorqueObj != null)
		{
			if (remorqueObj.getCodeAssure() == null || remorqueObj.getCodeAssure().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_ASSURE_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(cid) from client where code_assure = \'" + remorqueObj.getCodeAssure() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = true;	
					errorStrng += VINStat.INVALID_ASSURE_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (remorqueObj.getImmatriculationRemorque() == null || remorqueObj.getImmatriculationRemorque().trim().length() == 0)
			{
				valid = true;	
				errorStrng += VINStat.MISSING_IMMATRICULATION_REMORQUE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(vid) from vehicule where immatriculation_semi_remorque = \'" + remorqueObj.getImmatriculationRemorque() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng) <= 0)
				{
					valid = true;	
					errorStrng += VINStat.INVALID_IMMATRICULATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
	*/
}

