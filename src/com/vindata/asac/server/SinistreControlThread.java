package com.vindata.asac.server;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class SinistreControlThread extends Thread 
{
	private AsacDAOImpl asacSvc = null;
    private LogMaster logMaster = null;
	private Connection myConn   = null;
	public SinistreControlThread(AsacDAOImpl asacSvc)
	{
		this.asacSvc = asacSvc;
	}
	public void run()
	{
		try
		{
			myConn = ConnectionPool.newConnection();

			String qstrng = "Select * from sinistre where "; 
			qstrng += "(c_status = \'" + VINStat.CONTROL_STATUS_INITIALIZATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_TRANSFORMATION + "\' or c_status = \'" + VINStat.CONTROL_STATUS_PENDING_VALIDATION + "\')";
			List<SinistreObject> sinistreArr = asacSvc.querySinistre(qstrng, myConn);
			if (sinistreArr != null && sinistreArr.size() > 0)
			{
				for(int l=0; l<sinistreArr.size(); l++)
				{
					SinistreObject obj = (SinistreObject)sinistreArr.get(l);
					if (obj != null)
					{
						String journalRef = VINStat.TRANSFORMATION_ABBREV + "_" + VINStat.SINISTRE_ABBREV + "_" + obj.getSid();
						JournalObject jnrObj = new JournalObject();
						jnrObj.setReference(journalRef);

						ArrayList errorArr = new ArrayList();
						if (isValid(obj, errorArr))
						{
							jnrObj.setStatus(VINStat.STATUS_VALID_STRING);
							obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_TRANSFER);
							obj.setComments("");
							//LogMaster.logActivities("VALID ENTRY\tSINISTRE\t"+obj.getReference()+ "\'\t\'" + obj.getNumeroAttestation()+"\'\t\'" + obj.getDateSurvenance()+"\'");
						}
						else
						{
							if (obj.getControlStatus() != VINStat.CONTROL_STATUS_PENDING_VALIDATION)
							{
								obj.setControlStatus(VINStat.CONTROL_STATUS_PENDING_VALIDATION);
								Object error = errorArr.get(0);	
								if (error != null) obj.setComments(error.toString());
								//LogMaster.logActivities("INVALID ENTRY\tSINISTRE\t"+obj.getReference()+ "\'\t\'" + obj.getNumeroAttestation()+"\'\t\'" + obj.getDateSurvenance()+"\'\t\'"+obj.getComments()+"\'");
								jnrObj.setStatus(VINStat.STATUS_PENDING_STRING);
								jnrObj.setDescription(obj.getComments());
							}
						}

						jnrObj.setDateInscription(Calendar.getInstance().getTime());
						obj.setDateMiseJour(Calendar.getInstance().getTime());
						int retval = asacSvc.updateSinistre(obj, myConn);
						if (retval != VINStat.SUCCESS)
						{
							LogMaster.logActivities("UPDATE FAILED FOR SINISTRE ["+obj.getReference()+"]");
						}
						else
						{
							//LogMaster.logActivities("UPDATE SUCCEEDED FOR SINISTRE ["+obj.getReference()+"]");
						}

						if (jnrObj.getStatus() != null && jnrObj.getStatus().trim().length() > 0) asacSvc.addJournalSinistre(obj);
					}
				}
			}
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
	private boolean isValid(SinistreObject sinistreObj, ArrayList errorArr)
	{
		boolean valid = true;
		String errorStrng = VINStat.DIVIDER;
		if (sinistreObj != null)
		{
			if (sinistreObj.getNumeroAttestation() == null || sinistreObj.getNumeroAttestation().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_NUMERO_ATTESTATION;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String codeValidStrng = "Select count(aid) from attestation where numero_attestation = \'" + sinistreObj.getNumeroAttestation() + "\'";
				if (asacSvc.getQueryCount(codeValidStrng, myConn) <= 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_NUMERO_ATTESTATION;
					errorStrng += VINStat.DIVIDER;
				}
			}

			if (sinistreObj.getCodeAssureur() == null || sinistreObj.getCodeAssureur().trim().length() == 0)
			{
				valid = false;	
				errorStrng += VINStat.MISSING_ASSUREUR_CODE;
				errorStrng += VINStat.DIVIDER;
			}
			else
			{
				String qstrng = "Select count(a.aid) from ref_assureur a where a.code_assureur = \'" + sinistreObj.getCodeAssureur() + "\'";
				double availCnt  = asacSvc.getQueryCount(qstrng, myConn);
				if (availCnt == 0)
				{
					valid = false;	
					errorStrng += VINStat.INVALID_ASSUREUR_CODE;
					errorStrng += VINStat.DIVIDER;
				}
			}

			errorArr.add(errorStrng);
		}

		return valid;
	}
}
