package com.vindata.asac.test;

import java.io.*;
import java.rmi.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import com.vindata.asac.dao.*;
import com.vindata.asac.klib.*;
import com.vindata.asac.model.*;

public class PopulateData 
{
	private AsacDAOImpl asacSvc = null;

	public PopulateData(String filename, String fileType)
	{
		this.asacSvc = new AsacDAOImpl();
		if (fileType != null && fileType.toLowerCase().contains("c")) initAssureFile(filename);
		else if (fileType != null && fileType.toLowerCase().contains("ve")) initVehiculeFile(filename);
		else if (fileType != null && fileType.toLowerCase().contains("r")) initRemorqueFile(filename);
		else if (fileType != null && fileType.toLowerCase().contains("a")) initAttestationFile(filename);
		else if (fileType != null && fileType.toLowerCase().contains("vt")) initVenteFile(filename);
		else if (fileType != null && fileType.toLowerCase().contains("s")) initSinistreFile(filename);
	}
	private void initAssureFile(String filename)
	{
		try
		{
			// use these parameters for the client information
			// from the client file. Overwrite the default values...
			FileInputStream infile      = new FileInputStream(filename);
			InputStreamReader inReader  = new InputStreamReader(infile);
			BufferedReader inbuffer     = new BufferedReader(inReader);

			String line = inbuffer.readLine();
			while (line != null)
			{
		//		System.err.println(line);
				if (line.toLowerCase().indexOf("code_assure") == -1) addAssureItem(line.trim().split("\t"));
				line = inbuffer.readLine();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void addAssureItem(String [] params)
	{
		try
		{
			String code_assure 		= null;
			String qualite 			= null;
			String nom 				= null;
			String prenom			= null; 
			String profession		= null; 
			String date_naissance	= null; 
			String num_contribuable	= null; 
			String ville			= null; 
			String rue				= null; 
			String code_postal		= null; 
			String telephone		= null; 
			String email			= null; 
			String numero_permis		= null; 
			String categorie_permis		= null; 
			String date_deliverance		= null; 
			String permis_delivre_par		= null; 
			String nom_conducteur		= null; 
			String naissance_conducteur	= null; 

			for (int i=0; i<params.length; i++)
			{
				String value = params[i];
				switch (i) 
				{
					case 0:
						code_assure = value;
						break;
					case 1:
						qualite = value;
						break;
					case 2:
						nom = value;
						break;
					case 3:
						prenom = value;
						break;
					case 4:
						profession = value;
						break;
					case 5:
						date_naissance = value;
						break;
					case 6:
						num_contribuable = value;
						break;
					case 7:
						ville = value;
						break;
					case 8:
						rue = value;
						break;
					case 9:
						code_postal = value;
						break;
					case 10:
						telephone = value;
						break;
					case 11:
						email = value;
						break;
					case 12:
						numero_permis = value;
						break;
					case 13:
						categorie_permis = value;
						break;
					case 14:
						date_deliverance = value;
						break;
					case 15:
						permis_delivre_par = value;
						break;
					case 16:
						nom_conducteur 	= value;
						break;
					case 17:
						naissance_conducteur = value;
						break;
					default:
						break;
				}
			}

			AssureObject clientObj = new AssureObject();
			if (code_assure != null && code_assure.trim().length() > 0) clientObj.setCodeAssure(code_assure);
			if (qualite != null && qualite.trim().length() > 0) clientObj.setQualite(qualite);
			if (nom != null && nom.trim().length() > 0) clientObj.setNom(nom);
			if (prenom != null && prenom.trim().length() > 0) clientObj.setPrenom(prenom);
			if (profession != null && profession.trim().length() > 0) clientObj.setProfession(profession);
			if (date_naissance != null && date_naissance.trim().length() > 0)
			{
				clientObj.setDateNaissance(VINStat.fileDateFormat.parse(date_naissance, new ParsePosition(0)));
			}
			if (num_contribuable != null && num_contribuable.trim().length() > 0) clientObj.setNumContribuable(num_contribuable);
			if (ville != null && ville.trim().length() > 0) clientObj.setVille(ville);
			if (rue != null && rue.trim().length() > 0) clientObj.setRue(rue);
			if (code_postal != null && code_postal.trim().length() > 0) clientObj.setBoitePostale(code_postal);
			if (telephone != null && telephone.trim().length() > 0) clientObj.setTelephone(telephone);
			if (email != null && email.trim().length() > 0) clientObj.setEmail(email);
			if (numero_permis != null && numero_permis.trim().length() > 0) clientObj.setNumeroPermis(numero_permis);
			if (categorie_permis != null && categorie_permis.trim().length() > 0) clientObj.setCategoriePermis(categorie_permis);
			if (date_deliverance != null && date_deliverance.trim().length() > 0) 
			{
				clientObj.setDateDeliverance(VINStat.fileDateFormat.parse(date_deliverance, new ParsePosition(0)));
			}
			if (permis_delivre_par != null && permis_delivre_par.trim().length() > 0) clientObj.setPermisDelivrePar(permis_delivre_par);
			if (nom_conducteur != null && nom_conducteur.trim().length() > 0) clientObj.setNomPrenomConducteur(nom_conducteur);
			if (naissance_conducteur != null && naissance_conducteur.trim().length() > 0) 
			{
				clientObj.setDateNaissanceConducteur(VINStat.fileDateFormat.parse(naissance_conducteur, new ParsePosition(0)));
			}

		//	printAssure(clientObj);
			int retval = asacSvc.addAssure(clientObj);
			if (retval != VINStat.SUCCESS)
			{
				System.out.println("Add Assure FAILED for [" + code_assure + "]");	
			}
			else			
			{
				System.out.println("Add Assure SUCCEEDED for [" + code_assure + "]");	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void printAssure(AssureObject obj)
	{
		System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.err.println("Code Assurance \t = ["+obj.getCodeAssure()+"]");
		System.err.println("Quantite \t = ["+obj.getQualite()+"]");
		System.err.println("Nom \t = ["+obj.getNom()+"]");
		System.err.println("Prenom \t = ["+obj.getPrenom()+"]");
		System.err.println("Profession \t = ["+obj.getProfession()+"]");
		if (obj.getDateNaissance() != null) System.err.println("Naissance \t = ["+obj.getDateNaissance()+"]");
		else System.err.println("Naissance \t = [NULL]");
		System.err.println("Contribuable \t = ["+obj.getNumContribuable()+"]");
		System.err.println("Ville \t = ["+obj.getVille()+"]");
		System.err.println("Rue \t = ["+obj.getRue()+"]");
		System.err.println("Code postal \t = ["+obj.getBoitePostale()+"]");
		System.err.println("Telephone \t = ["+obj.getTelephone()+"]");
		System.err.println("Email \t = ["+obj.getEmail()+"]");
		System.err.println("Numero permis \t = ["+obj.getNumeroPermis()+"]");
		System.err.println("Categorie permis \t = ["+obj.getCategoriePermis()+"]");
		if (obj.getDateDeliverance() != null) System.err.println("Date delivrance permis \t = ["+obj.getDateDeliverance()+"]");
		else System.err.println("Date delivrance permis \t = [NULL]");
		System.err.println("Permis delivre par : \t = ["+obj.getPermisDelivrePar()+"]");
	}
	private void initVehiculeFile(String filename)
	{
		try
		{
			// use these parameters for the client information
			// from the client file. Overwrite the default values...
			FileInputStream infile      = new FileInputStream(filename);
			InputStreamReader inReader  = new InputStreamReader(infile);
			BufferedReader inbuffer     = new BufferedReader(inReader);

			String line = inbuffer.readLine();
			while (line != null)
			{
				if (line.toLowerCase().indexOf("code_assure") == -1) addVehiculeItem(line.trim().split("\t"));
				line = inbuffer.readLine();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void addVehiculeItem(String [] params)
	{
		try
		{
			String code_assure 		= null;
			String marque 			= null;
			String model 			= null;
			String mise_circ 		= null;
			String immatricule 		= null;
			String chassis 			= null;
			String usage 			= null;
			String charge 			= null;
			String puissance 		= null;
			String semi_remorque 	= null;
			String num_portes 		= null;
			String imma_remorque 	= null;
			String source_energie 	= null;
			String num_places 		= null;
			String cylindre 		= null;
			String double_commande 	= null;
			String responsabilite 	= null;
			String utilitaire 		= null;
			String type_engin 		= null;
			String poids 			= null;

			for (int i=0; i<params.length; i++)
			{
				String value = params[i];
				switch (i) 
				{
					case 0:
						code_assure = value;
						break;
					case 1:
						marque = value;
						break;
					case 2:
						model = value;
						break;
					case 3:
						mise_circ = value;
						break;
					case 4:
						immatricule = value;
						break;
					case 5:
						chassis = value;
						break;
					case 6:
						usage = value;
						break;
					case 7:
						charge = value;
						break;
					case 8:
						puissance = value;
						break;
					case 9:
						semi_remorque = value;
						break;
					case 10:
						num_portes = value;
						break;
					case 11:
						imma_remorque = value;
						break;
					case 12:
						source_energie = value;
						break;
					case 13:
						num_places = value;
						break;
					case 14:
						cylindre = value;
						break;
					case 15:
						double_commande = value;
						break;
					case 16:
						responsabilite = value;
						break;
					case 17:
						utilitaire = value;
						break;
					case 18:
						type_engin = value;
						break;
					case 19:
						poids = value;
						break;
					default:
						break;
				}
			}

			VehiculeObject obj = new VehiculeObject();
			if (code_assure != null && code_assure.trim().length() > 0) obj.setCodeAssure(code_assure);
			if (marque != null && marque.trim().length() > 0) obj.setMarque(marque);
			if (model != null && model.trim().length() > 0) obj.setModel(model);
			if (mise_circ != null && mise_circ.trim().length() > 0) 
			{
				obj.setPremiereDateMiseCirculation(VINStat.fileDateFormat.parse(mise_circ, new ParsePosition(0)));
			}
			if (immatricule != null && immatricule.trim().length() > 0) obj.setImmatriculation(immatricule);
			if (chassis != null && chassis.trim().length() > 0) obj.setChassis(chassis);
			if (usage != null && usage.trim().length() > 0) obj.setUsage(usage);
			if (charge != null && charge.trim().length() > 0) obj.setChargeUtile(charge);
			if (puissance != null && puissance.trim().length() > 0) 
			{
				try
				{
					Double val = new Double(puissance);
					obj.setPuissanceFiscale(val.doubleValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (semi_remorque != null && semi_remorque.trim().length() > 0) obj.setRemorque(semi_remorque);
			if (num_portes != null && num_portes.trim().length() > 0) 
			{
				try
				{
					Integer val = new Integer(num_portes);
					obj.setNombrePortes(val.intValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (imma_remorque != null && imma_remorque.trim().length() > 0) obj.setImmatriculationRemorque(imma_remorque);
			if (source_energie != null && source_energie.trim().length() > 0) obj.setSourceEnergie(source_energie);
			if (num_places != null && num_places.trim().length() > 0) 
			{
				try
				{
					Integer val = new Integer(num_places);
					obj.setNombreDePlaces(val.intValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (cylindre != null && cylindre.trim().length() > 0) 
			{
				try
				{
					Integer val = new Integer(cylindre);
					obj.setCylindre(val.intValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (double_commande != null && double_commande.trim().length() > 0) 
			{
				try
				{
					Integer val = new Integer(double_commande);
					obj.setDoubleCommande(val.intValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (responsabilite != null && responsabilite.trim().length() > 0) 
			{
				try
				{
					Integer val = new Integer(responsabilite);
					obj.setResponsabiliteCivile(val.intValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (utilitaire != null && utilitaire.trim().length() > 0) 
			{
				try
				{
					Integer val = new Integer(utilitaire);
					obj.setUtilitaire(val.intValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (type_engin != null && type_engin.trim().length() > 0) 
			{
				try
				{
					Integer val = new Integer(type_engin);
					obj.setTypeEngin(val.intValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}
			if (poids != null && poids.trim().length() > 0) 
			{
				try
				{
					Double val = new Double(poids);
					obj.setPoids(val.doubleValue());
				}
				catch(Exception ee)
				{
					ee.printStackTrace();
				}
			}

			printVehicule(obj);
			int retval = asacSvc.addVehicule(obj);
			if (retval != VINStat.SUCCESS)
			{
				System.out.println("Add Vehicule FAILED for [" + code_assure + "]");	
			}
			else			
			{
				System.out.println("Add Vehicule SUCCEEDED for [" + code_assure + "]");	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void printVehicule(VehiculeObject obj)
	{
		System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.err.println("Code Assurance \t = ["+obj.getCodeAssure()+"]");
		System.err.println("Marque \t = ["+obj.getMarque()+"]");
		System.err.println("Model \t = ["+obj.getModel()+"]");
		if (obj.getPremiereDateMiseCirculation() != null) System.err.println("Premiere mise en circulation \t = ["+obj.getPremiereDateMiseCirculation()+"]");
		System.err.println("Immatriculation \t = ["+obj.getImmatriculation()+"]");
		System.err.println("Chassis \t = ["+obj.getChassis()+"]");
		System.err.println("Usage \t = ["+obj.getUsage()+"]");
		System.err.println("Charge utile \t = ["+obj.getChargeUtile()+"]");
		System.err.println("Puissance fiscale \t = ["+obj.getPuissanceFiscale()+"]");
		System.err.println("remorque \t = ["+obj.getRemorque()+"]");
		System.err.println("Nombre portes \t = ["+obj.getNombrePortes()+"]");
		System.err.println("Immatriculation remorque\t = ["+obj.getImmatriculationRemorque()+"]");
		System.err.println("Source Energie \t = ["+obj.getSourceEnergie()+"]");
		System.err.println("Nombre places \t = ["+obj.getNombreDePlaces()+"]");
		System.err.println("Cylindre \t = ["+obj.getCylindre()+"]");
		System.err.println("Double commande \t = ["+obj.getDoubleCommande()+"]");
		System.err.println("Responsabilite civile \t = ["+obj.getResponsabiliteCivile()+"]");
		System.err.println("Utilitaire \t = ["+obj.getUtilitaire()+"]");
		System.err.println("Type engin \t = ["+obj.getTypeEngin()+"]");
		System.err.println("Poids \t = ["+obj.getPoids()+"]");

	}
	private void initRemorqueFile(String filename)
	{
		try
		{
			// use these parameters for the client information
			// from the client file. Overwrite the default values...
			FileInputStream infile      = new FileInputStream(filename);
			InputStreamReader inReader  = new InputStreamReader(infile);
			BufferedReader inbuffer     = new BufferedReader(inReader);

			String line = inbuffer.readLine();
			while (line != null)
			{
				if (line.toLowerCase().indexOf("code_assure") == -1) addRemorqueItem(line.trim().split("\t"));
				line = inbuffer.readLine();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void addRemorqueItem(String [] params)
	{
		try
		{
			String code_assure 		= null;
			String immatriculation	= null; 

			for (int i=0; i<params.length; i++)
			{
				String value = params[i];
				switch (i) 
				{
					case 0:
						code_assure = value;
						break;
					case 1:
						immatriculation = value;
						break;
					default:
						break;
				}
			}

			RemorqueObject obj = new RemorqueObject();
			if (code_assure != null && code_assure.trim().length() > 0) obj.setCodeAssure(code_assure);
			if (immatriculation != null && immatriculation.trim().length() > 0) obj.setImmatriculationRemorque(immatriculation);

			printRemorque(obj);
			int retval = asacSvc.addRemorque(obj);
			if (retval != VINStat.SUCCESS)
			{
				System.out.println("Add Remorque FAILED for [" + code_assure + "]");	
			}
			else			
			{
				System.out.println("Add Remorque SUCCEEDED for [" + code_assure + "]");	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void printRemorque(RemorqueObject obj)
	{
		System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.err.println("Code Assurance \t = ["+obj.getCodeAssure()+"]");
		System.err.println("Immatriculation \t = ["+obj.getImmatriculationRemorque()+"]");
	}
	private void initAttestationFile(String filename)
	{
		try
		{
			// use these parameters for the client information
			// from the client file. Overwrite the default values...
			FileInputStream infile      = new FileInputStream(filename);
			InputStreamReader inReader  = new InputStreamReader(infile);
			BufferedReader inbuffer     = new BufferedReader(inReader);

			String line = inbuffer.readLine();
			while (line != null)
			{
				if (line.toLowerCase().indexOf("numero_attestation") == -1) addAttestationItem(line.trim().split("\t"));
				line = inbuffer.readLine();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void addAttestationItem(String [] params)
	{
		try
		{
			String numero_attestation 		= null;
			String numero_police			= null; 
			String date_emission			= null; 
			String date_effet				= null; 
			String date_echeance			= null; 
			String couleur					= null; 
			String statut					= null; 
			String zone_circulation			= null; 
			String immatriculation			= null; 
			String code_assure				= null; 
			String code_assureur			= null; 

			for (int i=0; i<params.length; i++)
			{
				String value = params[i];
				switch (i) 
				{
					case 0:
						numero_attestation = value;
						break;
					case 1:
						numero_police = value;
						break;
					case 2:
						date_emission = value;
						break;
					case 3:
						date_effet = value;
						break;
					case 4:
						date_echeance = value;
						break;
					case 5:
						couleur = value;
						break;
					case 6:
						statut = value;
						break;
					case 7:
						zone_circulation = value;
						break;
					case 8:
						immatriculation = value;
						break;
					case 9:
						code_assure = value;
						break;
					case 10:
						code_assureur = value;
						break;
					default:
						break;
				}
			}

			AttestationObject obj = new AttestationObject();
			if (numero_attestation != null && numero_attestation.trim().length() > 0) obj.setNumeroAttestation(numero_attestation);
			if (numero_police != null && numero_police.trim().length() > 0) obj.setNumeroPolice(numero_police);
			if (date_emission != null && date_emission.trim().length() > 0) 
			{
				obj.setDateEmission(VINStat.fileDateFormat.parse(date_emission, new ParsePosition(0)));
			}
			if (date_effet != null && date_effet.trim().length() > 0) 
			{
				obj.setDateEffet(VINStat.fileDateFormat.parse(date_effet, new ParsePosition(0)));
			}
			if (date_echeance != null && date_echeance.trim().length() > 0) 
			{
				obj.setDateEcheance(VINStat.fileDateFormat.parse(date_echeance, new ParsePosition(0)));
			}
			if (couleur != null && couleur.trim().length() > 0) obj.setCouleur(couleur);
			if (statut != null && statut.trim().length() > 0) obj.setStatut(statut);
			if (zone_circulation != null && zone_circulation.trim().length() > 0) obj.setZoneCirculation(zone_circulation);
			if (immatriculation != null && immatriculation.trim().length() > 0) obj.setImmatriculation(immatriculation);
			if (code_assure != null && code_assure.trim().length() > 0) obj.setCodeAssure(code_assure);
			if (code_assureur != null && code_assureur.trim().length() > 0) obj.setCodeAssureur(code_assureur);

			printAttestation(obj);
			int retval = asacSvc.addAttestation(obj);
			if (retval != VINStat.SUCCESS)
			{
				System.out.println("Add Attestation FAILED for [" + code_assure + "]");	
			}
			else			
			{
				System.out.println("Add Attestation SUCCEEDED for [" + code_assure + "]");	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void printAttestation(AttestationObject obj)
	{
		System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.err.println("Numero attestation \t = ["+obj.getNumeroAttestation()+"]");
		System.err.println("Numero police \t = ["+obj.getNumeroPolice()+"]");
		System.err.println("Date emission \t = ["+obj.getDateEmission()+"]");
		System.err.println("Date effet \t = ["+obj.getDateEffet()+"]");
		System.err.println("Date echeance \t = ["+obj.getDateEcheance()+"]");
		System.err.println("Couleur \t = ["+obj.getCouleur()+"]");
		System.err.println("Statut \t = ["+obj.getStatut()+"]");
		System.err.println("Zone circulation \t = ["+obj.getZoneCirculation()+"]");
		System.err.println("Immatriculation \t = ["+obj.getImmatriculation()+"]");
		System.err.println("Code assure \t = ["+obj.getCodeAssure()+"]");
		System.err.println("Code assureur \t = ["+obj.getCodeAssureur()+"]");
	}
	private void initVenteFile(String filename)
	{
		try
		{
			// use these parameters for the client information
			// from the client file. Overwrite the default values...
			FileInputStream infile      = new FileInputStream(filename);
			InputStreamReader inReader  = new InputStreamReader(infile);
			BufferedReader inbuffer     = new BufferedReader(inReader);

			String line = inbuffer.readLine();
			while (line != null)
			{
				if (line.toLowerCase().indexOf("numero_attestation") == -1) addVenteItem(line.trim().split("\t"));
				line = inbuffer.readLine();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void addVenteItem(String [] params)
	{
		try
		{
			String numero_attestation 		= null;
			String immatriculation			= null; 
			String code_assure				= null; 
			String code_intermediaire		= null; 
			String prime_nette				= null; 
			String dta						= null; 


			for (int i=0; i<params.length; i++)
			{
				String value = params[i];
				switch (i) 
				{
					case 0:
						numero_attestation = value;
						break;
					case 1:
						immatriculation = value;
						break;
					case 2:
						code_assure = value;
						break;
					case 3:
						code_intermediaire = value;
						break;
					case 4:
						prime_nette = value;
						break;
					case 5:
						dta = value;
						break;
					default:
						break;
				}
			}

			VenteObject obj = new VenteObject();
			if (numero_attestation != null && numero_attestation.trim().length() > 0) obj.setNumeroAttestation(numero_attestation);
			if (immatriculation != null && immatriculation.trim().length() > 0) obj.setImmatriculation(immatriculation);
			if (code_assure != null && code_assure.trim().length() > 0) obj.setCodeAssure(code_assure);
			if (code_intermediaire != null && code_intermediaire.trim().length() > 0) obj.setCodeIntermediaire(code_intermediaire);
			if (prime_nette != null && prime_nette.trim().length() > 0) 
			{
				Integer val = new Integer(prime_nette);
				obj.setPrimeNetteRC(val.intValue());
			}
			if (dta != null && dta.trim().length() > 0) 
			{
				Integer val = new Integer(dta);
				obj.setDta(val.intValue());
			}

			printVente(obj);
			int retval = asacSvc.addVente(obj);
			if (retval != VINStat.SUCCESS)
			{
				System.out.println("Add Vente FAILED for [" + code_assure + "]");	
			}
			else			
			{
				System.out.println("Add Vente SUCCEEDED for [" + code_assure + "]");	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void printVente(VenteObject obj)
	{
		System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.err.println("Numero attestation \t = ["+obj.getNumeroAttestation()+"]");
		System.err.println("Immatriculation \t = ["+obj.getImmatriculation()+"]");
		System.err.println("Code assure \t = ["+obj.getCodeAssure()+"]");
		System.err.println("Code intermediaire \t = ["+obj.getCodeIntermediaire()+"]");
		System.err.println("Prime nette \t = ["+obj.getPrimeNetteRC()+"]");
		System.err.println("Dta \t = ["+obj.getDta()+"]");
	}
	private void initSinistreFile(String filename)
	{
		try
		{
			// use these parameters for the client information
			// from the client file. Overwrite the default values...
			FileInputStream infile      = new FileInputStream(filename);
			InputStreamReader inReader  = new InputStreamReader(infile);
			BufferedReader inbuffer     = new BufferedReader(inReader);

			String line = inbuffer.readLine();
			while (line != null)
			{
				if (line.toLowerCase().indexOf("reference") == -1) addSinistreItem(line.trim().split("\t"));
				line = inbuffer.readLine();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void addSinistreItem(String [] params)
	{
		try
		{
			String reference 				= null;
			String type_dommage				= null; 
			String cause_sinistre			= null; 
			String numero_attestation		= null; 
			String date_survenue			= null; 
			String date_declaration			= null; 

			for (int i=0; i<params.length; i++)
			{
				String value = params[i];
				switch (i) 
				{
					case 0:
						reference = value;
						break;
					case 1:
						type_dommage = value;
						break;
					case 2:
						cause_sinistre = value;
						break;
					case 3:
						numero_attestation = value;
						break;
					case 4:
						date_survenue = value;
						break;
					case 5:
						date_declaration = value;
						break;
					default:
						break;
				}
			}

			SinistreObject obj = new SinistreObject();
			if (reference != null && reference.trim().length() > 0) obj.setReference(reference);
			if (type_dommage != null && type_dommage.trim().length() > 0) obj.setTypeDommage(type_dommage);
			if (cause_sinistre != null && cause_sinistre.trim().length() > 0) obj.setCauseSinistre(cause_sinistre);
			if (numero_attestation != null && numero_attestation.trim().length() > 0) obj.setNumeroAttestation(numero_attestation);
			if (date_survenue != null && date_survenue.trim().length() > 0) 
			{
				obj.setDateSurvenance(VINStat.fileDateFormat.parse(date_survenue, new ParsePosition(0)));
			}
			if (date_declaration != null && date_declaration.trim().length() > 0) 
			{
				obj.setDateDeclaration(VINStat.fileDateFormat.parse(date_declaration, new ParsePosition(0)));
			}

			printSinistre(obj);
			int retval = asacSvc.addSinistre(obj);
			if (retval != VINStat.SUCCESS)
			{
				System.out.println("Add Sinistre FAILED for [" + numero_attestation + "]");	
			}
			else			
			{
				System.out.println("Add Sinistre SUCCEEDED for [" + numero_attestation + "]");	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void printSinistre(SinistreObject obj)
	{
		System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.err.println("Reference \t = ["+obj.getReference()+"]");
		System.err.println("Type dommage \t = ["+obj.getTypeDommage()+"]");
		System.err.println("Cause sinistre \t = ["+obj.getCauseSinistre()+"]");
		System.err.println("Numero attestation \t = ["+obj.getNumeroAttestation()+"]");
		System.err.println("Date survenue \t = ["+obj.getDateSurvenance()+"]");
		System.err.println("Date declaration \t = ["+obj.getDateDeclaration()+"]");
	}
	public static void main(String [] args)
	{
		String filename  = null;
		String fileType  = null;

		int i = 0;	
		do
		{
			if (i < args.length && args[i] != null && args[i].equals("-t"))
			{
				int pos = i+1;
				fileType = args[pos];	
			}
			else
			if (i < args.length && args[i] != null && args[i].equals("-f"))
			{
				int pos = i+1;
				filename = args[pos];
			}
		} while (i++ < args.length);

		if (fileType == null || filename == null)
		{
			printHelp();
		}

		new PopulateData(filename, fileType);
	}
	private static void printHelp()
	{
		System.err.println("Enter -f <filename> -t <file-type> [ c=client, ve=vehicule, a=attestation s=sinistre r=remorque i=intermediate vt=vente]");
		System.exit(0);	
	}
}

