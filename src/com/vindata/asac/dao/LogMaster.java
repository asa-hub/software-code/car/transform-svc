package com.vindata.asac.dao;

import java.io.*;
import java.text.*;

import java.sql.*;
import java.util.*;
import java.nio.charset.*;

import com.vindata.asac.klib.*;

public class LogMaster 
{

	private String logdir  = "/var/log/asac/";
	private String debugfile = "debug.log";
	private String activitiesFile = "activities.log";

	private static FileWriter logDebug;
	private static FileWriter logActivity;
	private static LogMaster masterObj = null;

	public static LogMaster getInstance()
	{
		if (masterObj == null) masterObj = new LogMaster();
		return masterObj;
	}
	protected LogMaster() 
	{
		File logDir = new File(logdir);
		if (logDir.exists() == false)
		{
            try
            {
                logDir.mkdirs();
            }
            catch(Exception ee)
            {
                System.err.println("makedir exceptioned ");
                ee.printStackTrace();
            }
		}

		File debugFile = new File(logdir + debugfile);
		if (debugFile.exists() == false)
		{
			try 
			{
				debugFile.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		// activities log
		File logFile = new File(logdir + activitiesFile);
		if (logFile.exists() == false)
		{
			try 
			{
				logFile.createNewFile();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		try
		{
			logDebug = new FileWriter(debugFile, true);
		}
		catch(Exception r)
		{
			r.printStackTrace();
		}

		try
		{
			logActivity = new FileWriter(logFile, true);
		}
		catch(Exception r)
		{
			r.printStackTrace();
		}
	}
	public synchronized static void logDebug(String mesg, Exception ex)
	{
		try
		{
			String now = VINStat.logDateFormat.format(new java.util.Date());
			if (mesg != null)
			{
				logDebug.write(now);
				logDebug.write("\t");
				logDebug.write(mesg);
				logDebug.write("\n");
				logDebug.flush();
			}

			if (ex != null)
			{			
				ex.printStackTrace(new PrintWriter(logDebug, true));
			}
		}
		catch(Exception r)
		{
			r.printStackTrace();
		}
	}
	public synchronized static void logActivities(String mesg)
	{
		try
		{
			String now = VINStat.logDateFormat.format(new java.util.Date());
			if (mesg != null)
			{
				logActivity.write(now);
				logActivity.write("\t");
				logActivity.write(mesg);
				logActivity.write("\n");
				logActivity.flush();
			}
		}
		catch(Exception r)
		{
			r.printStackTrace();
		}
	}
}
