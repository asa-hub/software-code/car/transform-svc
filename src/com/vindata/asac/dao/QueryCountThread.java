package com.vindata.asac.dao;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.vindata.asac.klib.*;

public class QueryCountThread extends Thread 
{
	private String searchString = null;
	private HashMap resultsHash = null;
	private String hashKey = null;

	public QueryCountThread(String queryString, HashMap resultsHash)
	{
		this.searchString = queryString;
		this.resultsHash = resultsHash;
	}
	public void run()
	{
		ResultSet rs = null;
		Statement stmt = null;

		Connection myConn = ConnectionPool.newConnection();

		double resultValue = 0;

		try
		{
			stmt = myConn.createStatement();
			rs = stmt.executeQuery(searchString);
			while(rs != null && rs.next())
			{
				resultValue = rs.getDouble(1);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("getQueryCount: exceptioned for ["+searchString+"]", e);
		}
		finally
		{
			try
			{
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
				if (myConn != null) myConn.close();
			}
			catch(Exception e)
			{
			}
		}

		resultsHash.put(searchString, new Double(resultValue));
	}
}

