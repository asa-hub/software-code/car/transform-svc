package com.vindata.asac.dao;

import java.io.*;
import java.rmi.*;
import java.rmi.server.*;
import java.net.URL;
import java.net.URLEncoder;

import java.text.*;
import java.sql.*;
import java.util.*;
import com.vindata.asac.klib.*;

public class ConnectionPool 
{
	public static int  ALARM_TRIGGER_LIMIT = 50;
	private static int maxConns = 20;

	//private String filename = "/usr/vindata/config/asacdb.properties";
	private String filename = "/srv/hubasac/integration/transform/hubasactransform.properties";

	private String driver 	= null;
	private String dbserver = null;
	private String dbname 	= null;
	private String dbtype 	= null;
	private String dbpname 	= null;
	private String dbpserver = null;
	private static String username = null;
	private static String passwd 	= null;
	private static String url 		= null;

	private static int currPos		= 0;
	private static Vector freeConnections = null;

	private static Properties pgProp;
	private static Connection queryConnection = null;
	private static ConnectionPool myConnPool = null;
	public static ConnectionPool getInstance()
	{
		if (myConnPool == null) myConnPool = new ConnectionPool();
		return myConnPool;
	}
	protected ConnectionPool()
	{
		try
		{
			File myfile = new File(VINStat.fileDBName);
			if (myfile.exists() == false)
			{
				myfile = new File("./hubasactransform.properties");
			}
			
			FileInputStream infile = new FileInputStream(myfile);
			Properties prop = new Properties();
			prop.load(infile);

			username= prop.getProperty("user", "postgres");
			passwd	= prop.getProperty("password", "postgres");
			dbname  = prop.getProperty("dbname", "hubasacint");
			dbpserver= prop.getProperty("dbserver", "//localhost:5432");

			pgProp = new Properties();
			pgProp.setProperty("user", username);
			pgProp.setProperty("password", passwd);
			pgProp.setProperty("sslmode", "disable");
			driver = "jdbc:postgresql:";
			Class.forName("org.postgresql.Driver");
			url	= driver+dbpserver+"/"+dbname;

			System.err.println("Connecting to DB = ["+url+"]");
			infile.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		initConnections();
	}
	private void initConnections()
	{
		freeConnections = new Vector();
		for (int i=0; i < maxConns; i++)
		{
			//freeConnections.add(newConnection());
			freeConnections.addElement(newConnection());
		}

		queryConnection = newConnection();
		System.err.println("Connected to Database : ["+url+"] ["+maxConns+"] connections initialized + queryConnections");
	}
	public static Connection newConnection()
	{
		try
		{
			return DriverManager.getConnection(url, username, passwd);
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}

		return null;
	}
	public static Connection getQueryConnection()
	{
		if (queryConnection != null) return queryConnection;
		else return newConnection();
	}
	public static synchronized Connection getConnection()
	{
		Connection con = null;

		if (freeConnections != null && freeConnections.size() > 0)
		{
			con = (Connection)freeConnections.get(currPos++);	
			if (currPos == freeConnections.size()) currPos = 0;

			try
			{
				if (con.isClosed())
				{
					// try again recursively
					con = getConnection();
				}
			}
			catch(SQLException e)
			{
				// try again recursively
				con = getConnection();
			}
		}
		else
		{
			// recycle the current position... come back to the beginning
			currPos = 0;
			LogMaster.logDebug("All free connections exhausted! Adding a new one. Total # connections = ["+maxConns+"]", null);
			System.err.println("All free connections exhausted! Adding a new one. Total # connections = ["+maxConns+"]");
		}

		try
		{
			if (!con.getAutoCommit())
			{
				con.setAutoCommit(true);
			}
		}
		catch(Exception e)
		{
		}

		if (con != null) return con;

		return null;
	}
	public static synchronized void freeConnection(Connection con)
	{
		freeConnection(con, null);
	}
	public static synchronized void freeConnection(Connection con, Object classObj)
	{
/*
		try
		{
			if (!con.getAutoCommit())
			{
				LogMaster.logDebug("Closing a not autoCommitted connection !", null);
				con.setAutoCommit(true);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		//freeConnections.addElement(con);
		freeConnections.add(con);

		//System.err.println("Freeing connections... # connections = ["+freeConnections.size()+"]");
*/
	} 
}
