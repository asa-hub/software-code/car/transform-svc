package com.vindata.asac.dao;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.vindata.asac.klib.VINStat;
import com.vindata.asac.model.*;

public class AsacDAOImpl implements AsacDAO
{
    private Connection myConn = null;
    private LogMaster logMaster = null;
    public AsacDAOImpl()
    {
        myConn = ConnectionPool.getInstance().getConnection();
        logMaster = LogMaster.getInstance();
    }
	public int addAssure(AssureObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String code_assure  		= obj.getCodeAssure();
		String code_assureur  		= obj.getCodeAssureur();
		String qualite  			= obj.getQualite();
		String nom  				= obj.getNom();
		String profession  			= obj.getProfession();
		String num_contribuable  	= obj.getNumContribuable();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\', \'" + qualite + 
                               "\', \'" + nom + 
                               "\', \'" + profession + 
                               "\',\'" + num_contribuable + "\')";

			insStrng = "INSERT INTO assure (code_assure, code_assureur, qualite, nom, " + 
                                           "profession, num_contribuable) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addAssure Exceptioned for : ["+insStrng+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setAid(retval);
		return updateAssure(obj);
	}
	public int updateAssure(AssureObject obj)
	{
		return updateAssure(obj, myConn);	
	}
	public int updateAssure(AssureObject obj, Connection mConn)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

        int aid          				= obj.getAid();
		String commentaires				= obj.getComments();
		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		try
		{
			java.sql.Timestamp mts = null;
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());

			//String nameCheck = "update assure set c_status = \'" + control_status + "\', c_date_mis_a_jour = \'" + mts + "\', c_date_transfer = \'" + tts + "\', commentaires = \'" + commentaires + "\'  where aid = \'" + aid + "\'";
			String nameCheck = "update assure set c_status = \'" + control_status + "\' " ;
			if (mts != null) nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null) nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where aid = \'" + aid + "\'";
        	stmt = mConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateAssure", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryAssure(String queryString)
	{
		return this.queryAssure(queryString, myConn);
	}
	public List queryAssure(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);

			while (rs.next())
			{
				AssureObject obj = new AssureObject();
				obj.setAid(rs.getInt("aid"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setQualite(rs.getString("qualite"));
				obj.setNom(rs.getString("nom"));
				obj.setPrenom(rs.getString("prenom"));
				obj.setProfession(rs.getString("profession"));
				java.sql.Date tim = rs.getDate("date_naissance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateNaissance(cal.getTime());
				}
				obj.setNumContribuable(rs.getString("num_contribuable"));
				obj.setVille(rs.getString("ville"));
				obj.setRue(rs.getString("rue"));
				obj.setBoitePostale(rs.getString("boite_postale"));
				obj.setTelephone(rs.getString("telephone"));
				obj.setEmail(rs.getString("email"));
				obj.setNumeroPermis(rs.getString("numero_permis"));
				obj.setCategoriePermis(rs.getString("categorie_permis"));
				tim = rs.getDate("date_delivrance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateDeliverance(cal.getTime());
				}
				obj.setPermisDelivrePar(rs.getString("permis_delivre_par"));
				obj.setNomPrenomConducteur(rs.getString("nom_prenom_conducteur"));
				tim = rs.getDate("date_naissance_conducteur");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateNaissanceConducteur(cal.getTime());
				}

				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

				obj.setComments(rs.getString("commentaires"));

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryAssure", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public int deleteAssure(int id)
	{
		int ret = 0;
		PreparedStatement checkStmt = null;

		try
		{
			String delete_string = "Delete from assure where aid = ?";
			checkStmt = myConn.prepareStatement(delete_string);
			checkStmt.setInt(1, id);
			ret = checkStmt.executeUpdate();
		}
		catch(Exception e)
		{
			LogMaster.logDebug("DrugImpl:deleteAssure", e);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (checkStmt != null) checkStmt.close();
            }
            catch(Exception f) 
            {
                f.printStackTrace();
            }
        }
	
		return VINStat.SUCCESS;
	}
	public int addJournalAssure(AssureObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String code_assure  		= obj.getCodeAssure();
		String code_assureur  		= obj.getCodeAssureur();
		String qualite  			= obj.getQualite();
		String nom  				= obj.getNom();
		String profession  			= obj.getProfession();
		String num_contribuable  	= obj.getNumContribuable();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\', \'" + qualite + 
                               "\', \'" + nom + 
                               "\', \'" + profession + 
                               "\',\'" + num_contribuable + "\')";

			insStrng = "INSERT INTO journal_assure (code_assure, code_assureur, qualite, nom, " + 
                                           			"profession, num_contribuable) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addJournalAssure Exceptioned for : ["+insStrng+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setAid(retval);
		return updateJournalAssure(obj);
	}
	public int updateJournalAssure(AssureObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

        int aid          			= obj.getAid();
		String code_assure  		= obj.getCodeAssure();
		String code_assureur  		= obj.getCodeAssureur();
		String qualite  			= obj.getQualite();
		String nom  				= obj.getNom();
		String prenom  				= obj.getPrenom();
		String profession  				= obj.getProfession();
		java.util.Date date_naissance	= obj.getDateNaissance();
		String num_contribuable 	= obj.getNumContribuable();
		String ville  				= obj.getVille();
		String rue  				= obj.getRue();
		String boite_postale  		= obj.getBoitePostale();
		String telephone  			= obj.getTelephone();
		String email  				= obj.getEmail();
		String numero_permis  		= obj.getNumeroPermis();
		String categorie_permis 	= obj.getCategoriePermis();
		java.util.Date date_deliverance   		= obj.getDateDeliverance();
		String permis_delivre_par   	= obj.getPermisDelivrePar();
		String nom_prenom_conducteur   	= obj.getNomPrenomConducteur();
		java.util.Date date_naissance_conducteur  = obj.getDateNaissanceConducteur();

		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();
		String commentaires				= obj.getComments();

		try
		{
			String nameCheck = "Select * from journal_assure where jaid = \'" + aid + "\' FOR  UPDATE";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs   = stmt.executeQuery(nameCheck);
			if ((rs != null) && (rs.next() == true))
			{
				rs.last();
				rs.updateString("code_assure", code_assure);
				rs.updateString("code_assureur", code_assureur);
				rs.updateString("qualite", qualite);
				rs.updateString("nom", nom);
				rs.updateString("prenom", prenom);
				rs.updateString("profession", profession);

				if (date_naissance != null) 
				{
					java.sql.Date ndate = new java.sql.Date(date_naissance.getTime());
					rs.updateDate("date_naissance", ndate);
				}
				else
				{
					rs.updateDate("date_naissance", null);
				}

				rs.updateString("num_contribuable", num_contribuable);
				rs.updateString("ville", ville);
				rs.updateString("rue", rue);
				rs.updateString("boite_postale", boite_postale);
				rs.updateString("telephone", telephone);
				rs.updateString("email", email);
				rs.updateString("numero_permis", numero_permis);
				rs.updateString("categorie_permis", categorie_permis);
				if (date_deliverance != null) 
				{
					java.sql.Date ddate = new java.sql.Date(date_deliverance.getTime());
					rs.updateDate("date_delivrance", ddate);
				}
				else
				{
					rs.updateDate("date_delivrance", null);
				}

				rs.updateString("permis_delivre_par", permis_delivre_par);
				rs.updateString("nom_prenom_conducteur", nom_prenom_conducteur);
				if (date_naissance_conducteur != null) 
				{
					java.sql.Date ndate = new java.sql.Date(date_naissance_conducteur.getTime());
					rs.updateDate("date_naissance_conducteur", ndate);
				}
				else
				{
					rs.updateDate("date_naissance_conducteur", null);
				}

				rs.updateInt("c_status", control_status);

				if (date_creation != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_creation.getTime());
					rs.updateTimestamp("c_date_creation", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_creation", null);
				}

				if (date_mise_jour != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_mise_jour.getTime());
					rs.updateTimestamp("c_date_mis_a_jour", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_mis_a_jour", null);
				}

				if (date_transfer != null) 
				{

					java.sql.Timestamp sts = new java.sql.Timestamp(date_transfer.getTime());
					rs.updateTimestamp("c_date_transfer", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_transfer", null);
				}

				rs.updateString("commentaires", commentaires);

				rs.updateRow();
            }
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateAssure", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryJournalAssure(String queryString)
	{
		return this.queryJournalAssure(queryString, myConn);
	}
	public List queryJournalAssure(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				AssureObject obj = new AssureObject();
				obj.setAid(rs.getInt("jaid"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setQualite(rs.getString("qualite"));
				obj.setNom(rs.getString("nom"));
				obj.setPrenom(rs.getString("prenom"));
				obj.setProfession(rs.getString("profession"));
				java.sql.Date tim = rs.getDate("date_naissance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateNaissance(cal.getTime());
				}
				obj.setNumContribuable(rs.getString("num_contribuable"));
				obj.setVille(rs.getString("ville"));
				obj.setRue(rs.getString("rue"));
				obj.setBoitePostale(rs.getString("boite_postale"));
				obj.setTelephone(rs.getString("telephone"));
				obj.setEmail(rs.getString("email"));
				obj.setNumeroPermis(rs.getString("numero_permis"));
				obj.setCategoriePermis(rs.getString("categorie_permis"));
				tim = rs.getDate("date_delivrance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateDeliverance(cal.getTime());
				}
				obj.setPermisDelivrePar(rs.getString("permis_delivre_par"));
				obj.setNomPrenomConducteur(rs.getString("nom_prenom_conducteur"));
				tim = rs.getDate("date_naissance_conducteur");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateNaissanceConducteur(cal.getTime());
				}

				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

				obj.setComments(rs.getString("commentaires"));

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryAssure", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}

	// vehicule manipulation
	public int addVehicule(VehiculeObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String code_assure  		= obj.getCodeAssure();
		String code_assureur  		= obj.getCodeAssureur();
		String immatriculation  	= obj.getImmatriculation();
		String usage  				= obj.getUsage();
		double puissance_fiscale  	= obj.getPuissanceFiscale();
		String remorque  			= obj.getRemorque();
		double poids  				= obj.getPoids();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\', \'" + immatriculation + 
                               "\', \'" + usage + 
                               "\', \'" + puissance_fiscale + 
                               "\', \'" + remorque + 
                               "\',\'" + poids + "\')";

			insStrng = "INSERT INTO vehicule (code_assure, code_assureur, immatriculation, usage, puissance_fiscale, remorque, poids_total_autorise_en_charge) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addVehicle Exceptioned for : ["+code_assure+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setVid(retval);
		return updateVehicule(obj);
	}
	public int updateVehicule(VehiculeObject obj)
	{
		return this.updateVehicule(obj, myConn);
	}
	public int updateVehicule(VehiculeObject obj, Connection mConn)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int vid  						= obj.getVid();
		String commentaires				= obj.getComments();

		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try
		{
			java.sql.Timestamp mts = null;
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());

			String nameCheck = "update vehicule set c_status = \'" + control_status + "\' " ;
			if (mts != null) nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null) nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where vid = \'" + vid + "\'";
        	stmt = mConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateVehicule", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;

	}
	public List queryVehicule(String queryString)
	{
		return this.queryVehicule(queryString, myConn);
	}
	public List queryVehicule(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			//pstmt = myConn.prepareStatement(queryString, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//rs = pstmt.executeQuery();
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				VehiculeObject obj = new VehiculeObject();
				obj.setVid(rs.getInt("vid"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setMarque(rs.getString("marque"));
				obj.setModel(rs.getString("modele"));
				java.sql.Date tim = rs.getDate("date_premiere_mise_circulation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setPremiereDateMiseCirculation(cal.getTime());
				}
				obj.setImmatriculation(rs.getString("immatriculation"));
				obj.setChassis(rs.getString("chassis"));
				obj.setUsage(rs.getString("usage"));
				obj.setChargeUtile(rs.getString("charge_utile"));
				obj.setPuissanceFiscale(rs.getDouble("puissance_fiscale"));
				obj.setRemorque(rs.getString("remorque"));
				obj.setNombrePortes(rs.getInt("nombre_portes"));
				obj.setImmatriculationRemorque(rs.getString("immatriculation_remorque"));
				obj.setSourceEnergie(rs.getString("source_energie"));
				obj.setNombreDePlaces(rs.getInt("nombre_de_places"));
				obj.setCylindre(rs.getInt("cylindree"));
				obj.setDoubleCommande(rs.getInt("double_commande"));
				obj.setResponsabiliteCivile(rs.getInt("responsabilite_civile"));
				obj.setUtilitaire(rs.getInt("utilitaire"));
				obj.setTypeEngin(rs.getInt("type_engin"));
				obj.setPoids(rs.getDouble("poids_total_autorise_en_charge"));
				obj.setComments(rs.getString("commentaires"));

				obj.setControlStatus(rs.getInt("c_status"));
				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryVehicules", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public int deleteVehicule(int id)
	{
		int ret = 0;
		PreparedStatement checkStmt = null;

		try
		{
			String delete_string = "Delete from vehicule where vid = ?";
			checkStmt = myConn.prepareStatement(delete_string);
			checkStmt.setInt(1, id);
			ret = checkStmt.executeUpdate();
		}
		catch(Exception e)
		{
			LogMaster.logDebug("DrugImpl:deleteVehicule", e);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (checkStmt != null) checkStmt.close();
            }
            catch(Exception f) 
            {
                f.printStackTrace();
            }
        }
	
		return VINStat.SUCCESS;
	}
	// add journal vehicule 
	public int addJournalVehicule(VehiculeObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String code_assure  		= obj.getCodeAssure();
		String code_assureur  		= obj.getCodeAssureur();
		String immatriculation  	= obj.getImmatriculation();
		String usage  				= obj.getUsage();
		double puissance_fiscale  	= obj.getPuissanceFiscale();
		String remorque  			= obj.getRemorque();
		double poids  				= obj.getPoids();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\', \'" + immatriculation + 
                               "\', \'" + usage + 
                               "\', \'" + puissance_fiscale + 
                               "\', \'" + remorque + 
                               "\',\'" + poids + "\')";

			insStrng = "INSERT INTO journal_vehicule (code_assure, code_assureur, immatriculation, usage, puissance_fiscale, remorque, poids_total_autorise_en_charge) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addJournalVehicle Exceptioned for : ["+code_assure+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setVid(retval);
		return updateJournalVehicule(obj);
	}
	public int updateJournalVehicule(VehiculeObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int vid  				= obj.getVid();
		String code_assure  	= obj.getCodeAssure();
		String code_assureur  	= obj.getCodeAssureur();
		String marque  			= obj.getMarque();
		String model  			= obj.getModel();
		java.util.Date mise_circ = obj.getPremiereDateMiseCirculation();
		String immatriculation  = obj.getImmatriculation();
		String chassis  		= obj.getChassis();
		String usage  			= obj.getUsage();
		String charge_utile  	= obj.getChargeUtile();
		double puissance_fiscale  	= obj.getPuissanceFiscale();
		String remorque  			= obj.getRemorque();
		int nombre_portes  			= obj.getNombrePortes();
		String imm_remorque  		= obj.getImmatriculationRemorque();
		String source_energie  	= obj.getSourceEnergie();
		int nombre_places  		= obj.getNombreDePlaces();
		int cylindre  			= obj.getCylindre();
		int double_commande  	= obj.getDoubleCommande();
		int resp_civile  		= obj.getResponsabiliteCivile();
		int utilitaire  		= obj.getUtilitaire();
		int type_engin  		= obj.getTypeEngin();
		double poids  				= obj.getPoids();

		String commentaires					= obj.getComments();

		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try
		{
			String nameCheck = "Select * from journal_vehicule where jvid = \'" + vid + "\' FOR UPDATE";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs   = stmt.executeQuery(nameCheck);
			if ((rs != null) && (rs.next() == true))
			{
				rs.last();
				rs.updateString("code_assure", code_assure);
				rs.updateString("code_assureur", code_assureur);
				rs.updateString("marque", marque);
				rs.updateString("modele", model);
				if (mise_circ != null)
				{
					java.sql.Date ndate = new java.sql.Date(mise_circ.getTime());
					rs.updateDate("date_premiere_mise_circulation", ndate);
				}
				else
				{
					rs.updateDate("date_premiere_mise_circulation", null);
				}

				rs.updateString("immatriculation", 			immatriculation);
				rs.updateString("chassis", 					chassis);
				rs.updateString("usage", 					usage);
				rs.updateString("charge_utile", 			charge_utile);
				rs.updateDouble("puissance_fiscale", 		puissance_fiscale);
				rs.updateString("remorque", 				remorque);
				rs.updateInt("nombre_portes", 				nombre_portes);
				rs.updateString("immatriculation_remorque", imm_remorque);
				rs.updateString("source_energie", 			source_energie);
				rs.updateInt("nombre_de_places", 			nombre_places);
				rs.updateInt("cylindree", 					cylindre);
				rs.updateInt("double_commande", 			double_commande);
				rs.updateInt("responsabilite_civile", 		resp_civile);
				rs.updateInt("utilitaire", 					utilitaire);
				rs.updateInt("type_engin", 					type_engin);
				rs.updateDouble("poids_total_autorise_en_charge", poids);

				rs.updateInt("c_status", control_status);

				if (date_creation != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_creation.getTime());
					rs.updateTimestamp("c_date_creation", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_creation", null);
				}

				if (date_mise_jour != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_mise_jour.getTime());
					rs.updateTimestamp("c_date_mis_a_jour", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_mis_a_jour", null);
				}

				if (date_transfer != null) 
				{

					java.sql.Timestamp sts = new java.sql.Timestamp(date_transfer.getTime());
					rs.updateTimestamp("c_date_transfer", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_transfer", null);
				}

				rs.updateString("commentaires", commentaires);
				rs.updateRow();
            }
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateVehicule", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;

	}
	public List queryJournalVehicule(String queryString)
	{
		return this.queryJournalVehicule(queryString, myConn);
	}
	public List queryJournalVehicule(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			//pstmt = myConn.prepareStatement(queryString, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//rs = pstmt.executeQuery();
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				VehiculeObject obj = new VehiculeObject();
				obj.setVid(rs.getInt("jvid"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setMarque(rs.getString("marque"));
				obj.setModel(rs.getString("modele"));
				java.sql.Date tim = rs.getDate("date_premiere_mise_circulation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setPremiereDateMiseCirculation(cal.getTime());
				}
				obj.setImmatriculation(rs.getString("immatriculation"));
				obj.setChassis(rs.getString("chassis"));
				obj.setUsage(rs.getString("usage"));
				obj.setChargeUtile(rs.getString("charge_utile"));
				obj.setPuissanceFiscale(rs.getDouble("puissance_fiscale"));
				obj.setRemorque(rs.getString("remorque"));
				obj.setNombrePortes(rs.getInt("nombre_portes"));
				obj.setImmatriculationRemorque(rs.getString("immatriculation_remorque"));
				obj.setSourceEnergie(rs.getString("source_energie"));
				obj.setNombreDePlaces(rs.getInt("nombre_de_places"));
				obj.setCylindre(rs.getInt("cylindree"));
				obj.setDoubleCommande(rs.getInt("double_commande"));
				obj.setResponsabiliteCivile(rs.getInt("responsabilite_civile"));
				obj.setUtilitaire(rs.getInt("utilitaire"));
				obj.setTypeEngin(rs.getInt("type_engin"));
				obj.setPoids(rs.getDouble("poids_total_autorise_en_charge"));
				obj.setComments(rs.getString("commentaires"));

				obj.setControlStatus(rs.getInt("c_status"));
				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryVehicules", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	// remorque manipulation
	public int addRemorque(RemorqueObject obj)
	{
		ResultSet rs = null;
		Statement stmt = null;
		int retval = -1;

		String code_assure  			= obj.getCodeAssure();
		String code_assureur  			= obj.getCodeAssureur();
		String immatriculation_remorque  = obj.getImmatriculationRemorque();

		try 
		{
			String dtStrng = " VALUES (\'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\',\'" + immatriculation_remorque + "\')";

			String insStrng = "INSERT INTO remorque (code_assure, code_assureur, immatriculation_remorque) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addRemorque Exceptioned for : ["+code_assure+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setRid(retval);
		return updateRemorque(obj);
	}
	public int updateRemorque(RemorqueObject obj)
	{
		return this.updateRemorque(obj, myConn);
	}
	public int updateRemorque(RemorqueObject obj, Connection mConn)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

        int rid          				= obj.getRid();
		String code_assure  			= obj.getCodeAssure();
		String code_assureur 			= obj.getCodeAssureur();
		String imma_remorque   			= obj.getImmatriculationRemorque();

		String commentaires				= obj.getComments();
		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		try
		{
			java.sql.Timestamp mts = new java.sql.Timestamp(date_mise_jour.getTime());
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());

			String nameCheck = "update remorque set c_status = \'" + control_status + "\' " ;
			if (mts != null) nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null) nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where rid = \'" + rid + "\'";

        	stmt = mConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateRemorque", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryRemorque(String queryString)
	{
		return this.queryRemorque(queryString, myConn);
	}
	public List queryRemorque(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				RemorqueObject obj = new RemorqueObject();
				obj.setRid(rs.getInt("rid"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setImmatriculationRemorque(rs.getString("immatriculation_remorque"));

				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));
				java.sql.Timestamp tim = rs.getTimestamp("c_date_creation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateCreation(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_mis_a_jour");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_transfer");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryImmatriculationRemorque", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public int deleteRemorque(int id)
	{
		int ret = 0;
		PreparedStatement checkStmt = null;

		try
		{
			String delete_string = "Delete from remorque where rid = ?";
			checkStmt = myConn.prepareStatement(delete_string);
			checkStmt.setInt(1, id);
			ret = checkStmt.executeUpdate();
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:deleteRemorque", e);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (checkStmt != null) checkStmt.close();
            }
            catch(Exception f) 
            {
                f.printStackTrace();
            }
        }
	
		return VINStat.SUCCESS;
	}
	// remorque manipulation
	public int addJournalRemorque(RemorqueObject obj)
	{
		ResultSet rs = null;
		Statement stmt = null;
		int retval = -1;

		String code_assure  			= obj.getCodeAssure();
		String code_assureur  			= obj.getCodeAssureur();
		String immatriculation_remorque  = obj.getImmatriculationRemorque();

		try 
		{
			String dtStrng = " VALUES (\'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\',\'" + immatriculation_remorque + "\')";

			String insStrng = "INSERT INTO journal_remorque (code_assure, code_assureur, immatriculation_remorque) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addJournalRemorque Exceptioned for : ["+code_assure+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setRid(retval);
		return updateJournalRemorque(obj);
	}
	public int updateJournalRemorque(RemorqueObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

        int rid          				= obj.getRid();
		String code_assure  			= obj.getCodeAssure();
		String code_assureur 			= obj.getCodeAssureur();
		String imma_remorque   			= obj.getImmatriculationRemorque();

		String commentaires				= obj.getComments();
		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		try
		{
			java.sql.Timestamp mts = new java.sql.Timestamp(date_mise_jour.getTime());
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());

			String nameCheck = "update journal_remorque set c_status = \'" + control_status + "\' " ;
			if (code_assure != null) nameCheck += ", code_assure = \'" + code_assure + "\'";
			if (code_assureur != null) nameCheck += ", code_assureur = \'" + code_assureur + "\'";
			if (imma_remorque != null) nameCheck += ", immatriculation_remorque = \'" + imma_remorque + "\'";
			if (mts != null) nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null) nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where jrid = \'" + rid + "\'";

        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateJournalRemorque", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryJournalRemorque(String queryString)
	{
		return this.queryJournalRemorque(queryString, myConn);
	}
	public List queryJournalRemorque(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				RemorqueObject obj = new RemorqueObject();
				obj.setRid(rs.getInt("jrid"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setImmatriculationRemorque(rs.getString("immatriculation_remorque"));

				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));
				java.sql.Timestamp tim = rs.getTimestamp("c_date_creation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateCreation(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_mis_a_jour");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_transfer");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryImmatriculationRemorque", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}

	// attestation manipulation
	public int addAttestation(AttestationObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String numero_attestation  		= obj.getNumeroAttestation();
		String numero_police  	   		= obj.getNumeroPolice();
		java.util.Date date_emission  	= obj.getDateEmission();
		java.util.Date date_effet  		= obj.getDateEffet();
		java.util.Date date_echeance  	= obj.getDateEcheance();
		String couleur  				= obj.getCouleur();
		String statut  					= obj.getStatut();
		String zone_circulation  		= obj.getZoneCirculation();
		String immatriculation  		= obj.getImmatriculation();
		String code_assure  			= obj.getCodeAssure();
		String code_assureur  			= obj.getCodeAssureur();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + numero_attestation + 
                               "\', \'" + couleur + 
                               "\', \'" + statut + 
                               "\', \'" + zone_circulation + 
                               "\', \'" + immatriculation + 
                               "\',\'" + code_assure + 
                               "\',\'" + code_assureur + "\')";

			insStrng = "INSERT INTO attestation (numero_attestation, couleur, statut, " + 
                                                		"zone_circulation, immatriculation, code_assure, code_assureur) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addAttestation Exceptioned for : ["+numero_attestation+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setAid(retval);
		return updateAttestation(obj);
	}
	public int updateAttestation(AttestationObject obj)
	{
		return this.updateAttestation(obj, myConn);
	}
	public int updateAttestation(AttestationObject obj, Connection mConn)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int aid  						= obj.getAid();

		String commentaires				= obj.getComments();
		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try
		{
			java.sql.Timestamp mts = null;
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());

			String nameCheck = "update attestation set c_status = \'" + control_status + "\' " ;
			if (mts != null) nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null) nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where aid = \'" + aid + "\'";

        	stmt = mConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("AsacDAOImpl:updateAttestation", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	// attestation manipulation
	public int addJournalAttestation(AttestationObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String numero_attestation  		= obj.getNumeroAttestation();
		String numero_police  	   		= obj.getNumeroPolice();
		java.util.Date date_emission  	= obj.getDateEmission();
		java.util.Date date_effet  		= obj.getDateEffet();
		java.util.Date date_echeance  	= obj.getDateEcheance();
		String couleur  				= obj.getCouleur();
		String statut  					= obj.getStatut();
		String zone_circulation  		= obj.getZoneCirculation();
		String immatriculation  		= obj.getImmatriculation();
		String code_assure  			= obj.getCodeAssure();
		String code_assureur  			= obj.getCodeAssureur();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + numero_attestation + 
                               "\', \'" + couleur + 
                               "\', \'" + statut + 
                               "\', \'" + zone_circulation + 
                               "\', \'" + immatriculation + 
                               "\',\'" + code_assure + 
                               "\',\'" + code_assureur + "\')";

			insStrng = "INSERT INTO journal_attestation (numero_attestation, couleur, statut, " + 
                                                		"zone_circulation, immatriculation, code_assure, code_assureur) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addJournalAttestation Exceptioned for : ["+numero_attestation+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setAid(retval);
		return updateJournalAttestation(obj);
	}
	public int updateJournalAttestation(AttestationObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int aid  						= obj.getAid();
		String numero_attestation  		= obj.getNumeroAttestation();
		String numero_police  	   		= obj.getNumeroPolice();
		java.util.Date date_emission  	= obj.getDateEmission();
		java.util.Date date_effet  		= obj.getDateEffet();
		java.util.Date date_echeance  	= obj.getDateEcheance();
		String couleur  				= obj.getCouleur();
		String statut  					= obj.getStatut();
		String zone_circulation  		= obj.getZoneCirculation();
		String immatriculation  		= obj.getImmatriculation();
		String remorque  				= obj.getRemorque();
		String code_assure  			= obj.getCodeAssure();
		String code_assureur  			= obj.getCodeAssureur();

		String commentaires					= obj.getComments();
		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try
		{
			String nameCheck = "Select * from journal_attestation where jaid = \'" + aid + "\' FOR  UPDATE";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs   = stmt.executeQuery(nameCheck);
			if ((rs != null) && (rs.next() == true))
			{
				rs.last();
				rs.updateString("numero_attestation", numero_attestation);
				rs.updateString("numero_police", numero_police);
				if (date_emission != null) 
				{
					java.sql.Date ndate = new java.sql.Date(date_emission.getTime());
					rs.updateDate("date_emission", ndate);
				}
				else
				{
					rs.updateDate("date_emission", null);
				}

				if (date_effet != null) 
				{
					java.sql.Date ndate = new java.sql.Date(date_effet.getTime());
					rs.updateDate("date_effet", ndate);
				}
				else
				{
					rs.updateDate("date_effet", null);
				}

				if (date_echeance != null) 
				{
					java.sql.Date ndate = new java.sql.Date(date_echeance.getTime());
					rs.updateDate("date_echeance", ndate);
				}
				else
				{
					rs.updateDate("date_echeance", null);
				}

				rs.updateString("couleur", couleur);
				rs.updateString("statut", statut);
				rs.updateString("zone_circulation", zone_circulation);
				rs.updateString("immatriculation", immatriculation);
				rs.updateString("remorque", remorque);
				rs.updateString("code_assure", code_assure);
				rs.updateString("code_assureur", code_assureur);

				rs.updateString("commentaires", commentaires);
				rs.updateInt("c_status", control_status);

				if (date_creation != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_creation.getTime());
					rs.updateTimestamp("c_date_creation", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_creation", null);
				}

				if (date_mise_jour != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_mise_jour.getTime());
					rs.updateTimestamp("c_date_mis_a_jour", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_mis_a_jour", null);
				}

				if (date_transfer != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_transfer.getTime());
					rs.updateTimestamp("c_date_transfer", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_transfer", null);
				}

				rs.updateRow();
            }
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("AsacDAOImpl:updateJournalAttestation", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryJournalAttestation(String queryString)
	{
		return this.queryJournalAttestation(queryString, myConn);
	}
	public List queryJournalAttestation(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				AttestationObject obj = new AttestationObject();
				obj.setAid(rs.getInt("jaid"));
				obj.setNumeroAttestation(rs.getString("numero_attestation"));
				obj.setNumeroPolice(rs.getString("numero_police"));

				java.sql.Date tim = rs.getDate("date_emission");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateEmission(cal.getTime());
				}

				tim = rs.getDate("date_effet");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateEffet(cal.getTime());
				}

				tim = rs.getDate("date_echeance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateEcheance(cal.getTime());
				}

				obj.setCouleur(rs.getString("couleur"));
				obj.setStatut(rs.getString("statut"));
				obj.setZoneCirculation(rs.getString("zone_circulation"));
				obj.setImmatriculation(rs.getString("immatriculation"));
				obj.setRemorque(rs.getString("remorque"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryJournalAttestation", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public List queryAttestation(String queryString)
	{
		return this.queryAttestation(queryString, myConn);
	}
	public List queryAttestation(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				AttestationObject obj = new AttestationObject();
				obj.setAid(rs.getInt("aid"));
				obj.setNumeroAttestation(rs.getString("numero_attestation"));
				obj.setNumeroPolice(rs.getString("numero_police"));

				java.sql.Date tim = rs.getDate("date_emission");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateEmission(cal.getTime());
				}

				tim = rs.getDate("date_effet");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateEffet(cal.getTime());
				}

				tim = rs.getDate("date_echeance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateEcheance(cal.getTime());
				}

				obj.setCouleur(rs.getString("couleur"));
				obj.setStatut(rs.getString("statut"));
				obj.setZoneCirculation(rs.getString("zone_circulation"));
				obj.setImmatriculation(rs.getString("immatriculation"));
				obj.setRemorque(rs.getString("remorque"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryAttestation", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public int deleteAttestation(int id)
	{
		int ret = 0;
		PreparedStatement checkStmt = null;

		try
		{
			String delete_string = "Delete from attestation where aid = ?";
			checkStmt = myConn.prepareStatement(delete_string);
			checkStmt.setInt(1, id);
			ret = checkStmt.executeUpdate();
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:deleteAttestation", e);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (checkStmt != null) checkStmt.close();
            }
            catch(Exception f) 
            {
                f.printStackTrace();
            }
        }
	
		return VINStat.SUCCESS;
	}
	// intermediare manipulation
	public int addIntermediaire(IntermediaireObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String code_int_dna  		= obj.getCodeIntermediaireDNA();
		String num_contribuable  	= obj.getNumContribuable();
		String type_intermediaire  		= obj.getTypeIntermediaire();
		String nom_intermediaire  	= obj.getNomIntermediaire();
		String telephone  			= obj.getTelephone();
		String ville  				= obj.getVille();
		String boite_postale  		= obj.getBoitePostale();
		String statut_intermediaire  		= obj.getStatutIntermediaire();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + code_int_dna + 
                               "\', \'" + num_contribuable + 
                               "\', \'" + type_intermediaire + 
                               "\', \'" + nom_intermediaire + 
                               "\', \'" + statut_intermediaire + 
                               "\', \'" + telephone + 
                               "\', \'" + ville + 
                               "\',\'" + boite_postale + "\')";

			insStrng = "INSERT INTO intermediaire (code_intermediaire_dna, num_contribuable, type_intermediaire, " + 
                                                  "nom_intermediaire, statut_intermediaire, telephone, ville, boite_postale) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addIntermediare Exceptioned for : ["+num_contribuable+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setIid(retval);
		return updateIntermediaire(obj);
	}
	public int updateIntermediaire(IntermediaireObject obj)
	{
		return this.updateIntermediaire(obj, myConn);
	}
	public int updateIntermediaire(IntermediaireObject obj, Connection mConn)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int iid  						= obj.getIid();
		String statut_intermediaire		= obj.getStatutIntermediaire();
		int control_status				= obj.getControlStatus();
		String commentaires				= obj.getComments();

		java.util.Date date_debut_susp  = obj.getDateDebutSuspension();
		java.util.Date date_fin_susp    = obj.getDateFinSuspension();

		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		try
		{
			java.sql.Timestamp ddsp = null;
			if (date_debut_susp != null) ddsp= new java.sql.Timestamp(date_debut_susp.getTime());

			java.sql.Timestamp dfsp = null;
			if (date_fin_susp != null) dfsp= new java.sql.Timestamp(date_fin_susp.getTime());

			java.sql.Timestamp mts = null;
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());

			String nameCheck = "update intermediaire set c_status = \'" + control_status + "\' " ;
			if (ddsp != null) nameCheck += ", date_debut_suspension = \'" + ddsp + "\' ";
			if (dfsp != null) nameCheck += ", date_fin_suspension = \'" + dfsp + "\' ";
			if (mts != null)  nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null)  nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where iid = \'" + iid + "\'";

        	stmt = mConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateIntermediaire", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryIntermediaire(String queryString)
	{
		return this.queryIntermediaire(queryString, myConn);
	}
	public List queryIntermediaire(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				IntermediaireObject obj = new IntermediaireObject();
				obj.setIid(rs.getInt("iid"));
				obj.setCodeIntermediaireDNA(rs.getString("code_intermediaire_dna"));
				obj.setNumContribuable(rs.getString("num_contribuable"));
				obj.setTypeIntermediaire(rs.getString("type_intermediaire"));
				obj.setNomIntermediaire(rs.getString("nom_intermediaire"));
				obj.setStatutIntermediaire(rs.getString("statut_intermediaire"));
				obj.setTelephone(rs.getString("telephone"));
				obj.setVille(rs.getString("ville"));
				obj.setBoitePostale(rs.getString("boite_postale"));

				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp tim = rs.getTimestamp("date_debut_suspension");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateDebutSuspension(cal.getTime());
				}

				tim = rs.getTimestamp("date_fin_suspension");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateFinSuspension(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_creation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateCreation(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_mis_a_jour");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_transfer");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryIntermediaire", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public int deleteIntermediaire(int id)
	{
		int ret = 0;
		PreparedStatement checkStmt = null;

		try
		{
			String delete_string = "Delete from intermediaire where iid = ?";
			checkStmt = myConn.prepareStatement(delete_string);
			checkStmt.setInt(1, id);
			ret = checkStmt.executeUpdate();
		}
		catch(Exception e)
		{
			LogMaster.logDebug("DrugImpl:deleteIntermediaire", e);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (checkStmt != null) checkStmt.close();
            }
            catch(Exception f) 
            {
                f.printStackTrace();
            }
        }
	
		return VINStat.SUCCESS;
	}
	// intermediare manipulation
	public int addJournalIntermediaire(IntermediaireObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String code_int_dna  		= obj.getCodeIntermediaireDNA();
		String num_contribuable  	= obj.getNumContribuable();
		String type_intermediaire  		= obj.getTypeIntermediaire();
		String nom_intermediaire  	= obj.getNomIntermediaire();
		String telephone  			= obj.getTelephone();
		String ville  				= obj.getVille();
		String boite_postale  		= obj.getBoitePostale();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + code_int_dna + 
                               "\', \'" + num_contribuable + 
                               "\', \'" + type_intermediaire + 
                               "\', \'" + nom_intermediaire + 
                               "\', \'" + telephone + 
                               "\', \'" + ville + 
                               "\',\'" + boite_postale + "\')";

			insStrng = "INSERT INTO journal_intermediaire (code_intermediaire_dna, num_contribuable, type_assureur, " + 
                                                		 "nom_intermediaire, telephone, ville, boite_postale) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addJournalIntermediare Exceptioned for : ["+num_contribuable+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setIid(retval);
		return updateJournalIntermediaire(obj);
	}
	public int updateJournalIntermediaire(IntermediaireObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int iid  					= obj.getIid();
		String code_int_dna  		= obj.getCodeIntermediaireDNA();
		String num_contribuable  	= obj.getNumContribuable();
		String type_intermediaire  		= obj.getTypeIntermediaire();
		String nom_intermediaire  	= obj.getNomIntermediaire();
		String telephone  			= obj.getTelephone();
		String ville  				= obj.getVille();
		String boite_postale  		= obj.getBoitePostale();

		int control_status				= obj.getControlStatus();
		String commentaires				= obj.getComments();

		java.util.Date date_debut_susp  = obj.getDateDebutSuspension();
		java.util.Date date_fin_susp    = obj.getDateFinSuspension();

		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		try
		{
			String nameCheck = "Select * from journal_intermediaire where jiid = \'" + iid + "\' FOR  UPDATE";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs   = stmt.executeQuery(nameCheck);
			if ((rs != null) && (rs.next() == true))
			{
				rs.last();
				rs.updateString("code_intermediaire_dna", code_int_dna);
				rs.updateString("num_contribuable", num_contribuable);
				rs.updateString("type_assureur", type_intermediaire);
				rs.updateString("nom_intermediaire", nom_intermediaire);
				rs.updateString("telephone", telephone);
				rs.updateString("ville", ville);
				rs.updateString("boite_postale", boite_postale);

				rs.updateString("commentaires", commentaires);
				rs.updateInt("c_status", control_status);

				if (date_debut_susp != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_debut_susp.getTime());
					rs.updateTimestamp("date_debut_suspension", sts);
				}
				else
				{
					rs.updateTimestamp("date_debut_suspension", null);
				}

				if (date_fin_susp != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_fin_susp.getTime());
					rs.updateTimestamp("date_fin_suspension", sts);
				}
				else
				{
					rs.updateTimestamp("date_fin_suspension", null);
				}

				if (date_creation != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_creation.getTime());
					rs.updateTimestamp("c_date_creation", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_creation", null);
				}

				if (date_mise_jour != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_mise_jour.getTime());
					rs.updateTimestamp("c_date_mis_a_jour", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_mis_a_jour", null);
				}

				if (date_transfer != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_transfer.getTime());
					rs.updateTimestamp("c_date_transfer", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_transfer", null);
				}

				rs.updateRow();
            }
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("DrugImpl:updateJournalIntermediaire", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryJournalIntermediaire(String queryString)
	{
		return this.queryJournalIntermediaire(queryString, myConn);
	}
	public List queryJournalIntermediaire(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				IntermediaireObject obj = new IntermediaireObject();
				obj.setIid(rs.getInt("jiid"));
				obj.setCodeIntermediaireDNA(rs.getString("code_intermediaire_dna"));
				obj.setNumContribuable(rs.getString("num_contribuable"));
				// unfortunate error 
				obj.setTypeIntermediaire(rs.getString("type_assureur"));
				obj.setNomIntermediaire(rs.getString("nom_intermediaire"));
				obj.setTelephone(rs.getString("telephone"));
				obj.setVille(rs.getString("ville"));
				obj.setBoitePostale(rs.getString("boite_postale"));

				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp tim = rs.getTimestamp("date_debut_suspension");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateDebutSuspension(cal.getTime());
				}

				tim = rs.getTimestamp("date_fin_suspension");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateFinSuspension(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_creation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateCreation(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_mis_a_jour");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_transfer");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryJournalIntermediaire", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	// vente manipulation
	public int addVente(VenteObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String numero_attestation   = obj.getNumeroAttestation();
		String immatriculation  	= obj.getImmatriculation();
		String code_assure  		= obj.getCodeAssure();
		String code_assureur  		= obj.getCodeAssureur();
		String code_intermediaire  	= obj.getCodeIntermediaire();
		int prime_nette_rc  		= obj.getPrimeNetteRC();
		int dta  				= obj.getDta();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + numero_attestation + 
                               "\', \'" + immatriculation + 
                               "\', \'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\', \'" + code_intermediaire + 
                               "\', \'" + prime_nette_rc + 
                               "\',\'" + dta + "\')";

			insStrng = "INSERT INTO vente (numero_attestation, immatriculation, code_assure, " + 
                                          "code_assureur, code_intermediaire_dna, prime_nette_rc, dta) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addVente Exceptioned for : ["+numero_attestation+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setVid(retval);
		return updateVente(obj);
	}
	public int updateVente(VenteObject obj)
	{
		return this.updateVente(obj, myConn);
	}
	public int updateVente(VenteObject obj, Connection mConn)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int vid  						= obj.getVid();

		int control_status				= obj.getControlStatus();
		String commentaires					= obj.getComments();

		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		String insStrng 				= null;
		int retval						= VINStat.FAILURE;

		try
		{
			java.sql.Timestamp mts = new java.sql.Timestamp(date_mise_jour.getTime());
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());

			String nameCheck = "update vente set c_status = \'" + control_status + "\' " ;
			if (mts != null) nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null) nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where vid = \'" + vid + "\'";

        	stmt = mConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("AsacDAOImpl:updateVente", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryVente(String queryString)
	{
		return this.queryVente(queryString, myConn);
	}
	public List queryVente(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				VenteObject obj = new VenteObject();
				obj.setVid(rs.getInt("vid"));
				obj.setNumeroAttestation(rs.getString("numero_attestation"));
				obj.setImmatriculation(rs.getString("immatriculation"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setCodeIntermediaire(rs.getString("code_intermediaire_dna"));
				obj.setPrimeNetteRC(rs.getInt("prime_nette_rc"));
				obj.setDta(rs.getInt("dta"));

				obj.setComments(rs.getString("commentaires"));

				obj.setControlStatus(rs.getInt("c_status"));
				java.sql.Timestamp tim = rs.getTimestamp("c_date_creation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateCreation(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_mis_a_jour");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_transfer");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryVentes", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public int deleteVente(int id)
	{
		int ret = 0;
		PreparedStatement checkStmt = null;

		try
		{
			String delete_string = "Delete from vente where vid = ?";
			checkStmt = myConn.prepareStatement(delete_string);
			checkStmt.setInt(1, id);
			ret = checkStmt.executeUpdate();
		}
		catch(Exception e)
		{
			LogMaster.logDebug("DrugImpl:deleteIntermediaire", e);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (checkStmt != null) checkStmt.close();
            }
            catch(Exception f) 
            {
                f.printStackTrace();
            }
        }
	
		return VINStat.SUCCESS;
	}
	public int addJournalVente(VenteObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		String numero_attestation   = obj.getNumeroAttestation();
		String immatriculation  	= obj.getImmatriculation();
		String code_assure  		= obj.getCodeAssure();
		String code_assureur  		= obj.getCodeAssureur();
		String code_intermediaire  	= obj.getCodeIntermediaire();
		int prime_nette_rc  		= obj.getPrimeNetteRC();
		int dta  				= obj.getDta();

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + numero_attestation + 
                               "\', \'" + immatriculation + 
                               "\', \'" + code_assure + 
                               "\', \'" + code_assureur + 
                               "\', \'" + code_intermediaire + 
                               "\', \'" + prime_nette_rc + 
                               "\',\'" + dta + "\')";

			insStrng = "INSERT INTO journal_vente (numero_attestation, immatriculation, code_assure, " + 
                                          "code_assureur, code_intermediaire_dna, prime_nette_rc, dta) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addJournalVente Exceptioned for : ["+numero_attestation+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setVid(retval);
		return updateJournalVente(obj);
	}
	public int updateJournalVente(VenteObject obj)
	{
		Timestamp ts = null;
		ResultSet rs = null;
		Statement stmt = null;

		int vid  						= obj.getVid();
		String numero_attestation   	= obj.getNumeroAttestation();
		String immatriculation  		= obj.getImmatriculation();
		String code_assure  			= obj.getCodeAssure();
		String code_assureur  			= obj.getCodeAssureur();
		String code_intermediaire  		= obj.getCodeIntermediaire();
		int prime_nette_rc  			= obj.getPrimeNetteRC();
		int dta  						= obj.getDta();

		int control_status				= obj.getControlStatus();
		String commentaires				= obj.getComments();

		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		String insStrng 				= null;
		int retval						= VINStat.FAILURE;

		try
		{
			String nameCheck = "Select * from journal_vente where jvid = \'" + vid + "\' FOR  UPDATE";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs   = stmt.executeQuery(nameCheck);
			if ((rs != null) && (rs.next() == true))
			{
				rs.last();
				rs.updateString("numero_attestation", numero_attestation);
				rs.updateString("immatriculation", immatriculation);
				rs.updateString("code_assure", code_assure);
				rs.updateString("code_assureur", code_assureur);
				rs.updateString("code_intermediaire_dna", code_intermediaire);
				rs.updateInt("prime_nette_rc", prime_nette_rc);
				rs.updateInt("dta", dta);

				rs.updateString("commentaires", commentaires);
				rs.updateInt("c_status", control_status);

				if (date_creation != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_creation.getTime());
					rs.updateTimestamp("c_date_creation", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_creation", null);
				}

				if (date_mise_jour != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_mise_jour.getTime());
					rs.updateTimestamp("c_date_mis_a_jour", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_mis_a_jour", null);
				}

				if (date_transfer != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_transfer.getTime());
					rs.updateTimestamp("c_date_transfer", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_transfer", null);
				}

				rs.updateRow();
            }
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("AsacDAOImpl:updateJournalVente", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryJournalVente(String queryString)
	{
		return this.queryJournalVente(queryString, myConn);
	}
	public List queryJournalVente(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				VenteObject obj = new VenteObject();
				obj.setVid(rs.getInt("jvid"));
				obj.setNumeroAttestation(rs.getString("numero_attestation"));
				obj.setImmatriculation(rs.getString("immatriculation"));
				obj.setCodeAssure(rs.getString("code_assure"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setCodeIntermediaire(rs.getString("code_intermediaire_dna"));
				obj.setPrimeNetteRC(rs.getInt("prime_nette_rc"));
				obj.setDta(rs.getInt("dta"));

				obj.setComments(rs.getString("commentaires"));

				obj.setControlStatus(rs.getInt("c_status"));
				java.sql.Timestamp tim = rs.getTimestamp("c_date_creation");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateCreation(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_mis_a_jour");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				tim = rs.getTimestamp("c_date_transfer");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryJournalVentes", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	// sinistre manipulation
	public int addSinistre(SinistreObject obj)
	{
		ResultSet rs = null;
		Statement stmt = null;

		String reference   			= obj.getReference();
		String code_assureur   		= obj.getCodeAssureur();
		String type_dommage  		= obj.getTypeDommageDB();
		String cause_sinistre  		= obj.getCauseSinistreDB();
		String numero_attestation  	= obj.getNumeroAttestation();

		Timestamp sts = null;
		java.util.Date date_survenance  		= obj.getDateSurvenance();
		if (date_survenance != null) sts = new java.sql.Timestamp(date_survenance.getTime());

		Timestamp dts = null;
		java.util.Date date_declaration  		= obj.getDateDeclaration();
		if (date_declaration != null) dts = new java.sql.Timestamp(date_declaration.getTime());

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + reference + 
                               "\', \'" + code_assureur + 
                               "\', \'" + type_dommage + 
                               "\', \'" + cause_sinistre + 
                               "\', \'" + numero_attestation + 
                               "\', \'" + sts + 
                               "\',\'" + dts + "\')";

			insStrng = "INSERT INTO sinistre (reference, code_assureur, type_dommage, cause_sinistre, " + 
                                             "numero_attestation, date_survenance, date_declaration) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addSinistre Exceptioned for : ["+reference+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setSid(retval);
		return updateSinistre(obj);
	}
	public int updateSinistre(SinistreObject obj)
	{
		return updateSinistre(obj, myConn);
	}
	public int updateSinistre(SinistreObject obj, Connection mConn)
	{
		ResultSet rs = null;
		Statement stmt = null;

		int sid   						= obj.getSid();
		String commentaires				= obj.getComments();
		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		try
		{
			java.sql.Timestamp mts = null;
			if (date_mise_jour != null) mts = new java.sql.Timestamp(date_mise_jour.getTime());

			java.sql.Timestamp tts = null;
			if (date_transfer != null) tts = new java.sql.Timestamp(date_transfer.getTime());
/*
			String nameCheck = "update sinistre set c_status = \'" + control_status + "\', c_date_mis_a_jour = \'" + mts + "\', c_date_transfer = \'" + tts + "\', commentaires = \'" + commentaires + "\'  where sid = \'" + sid + "\'";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
*/

			String nameCheck = "update sinistre set c_status = \'" + control_status + "\' " ;
			if (mts != null) nameCheck += ", c_date_mis_a_jour = \'" + mts + "\' ";
			if (tts != null) nameCheck += ", c_date_transfer = \'" + tts + "\' "; 
			if (commentaires != null) nameCheck += ", commentaires = \'" + commentaires + "\'";
		  	nameCheck += " where sid = \'" + sid + "\'";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			stmt.executeUpdate(nameCheck);
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("AsacDAOImpl:updateSinistre", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List querySinistre(String queryString)
	{
		return this.querySinistre(queryString, myConn);
	}
	public List querySinistre(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);

			while (rs.next())
			{
				SinistreObject obj = new SinistreObject();
				obj.setSid(rs.getInt("sid"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setReference(rs.getString("reference"));
				obj.setTypeDommage(rs.getString("type_dommage"));
				obj.setCauseSinistre(rs.getString("cause_sinistre"));
				obj.setNumeroAttestation(rs.getString("numero_attestation"));
				
				java.sql.Timestamp tim = rs.getTimestamp("date_survenance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateSurvenance(cal.getTime());
				}

				java.sql.Date dt = rs.getDate("date_declaration");
				if (dt != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(dt.getTime());
					obj.setDateDeclaration(cal.getTime());
				}

				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:querySinistres", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public int deleteSinistre(int id)
	{
		int ret = 0;
		PreparedStatement checkStmt = null;

		try
		{
			String delete_string = "Delete from sinistre where sid = ?";
			checkStmt = myConn.prepareStatement(delete_string);
			checkStmt.setInt(1, id);
			ret = checkStmt.executeUpdate();
		}
		catch(Exception e)
		{
			LogMaster.logDebug("DrugImpl:deleteSinistre", e);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (checkStmt != null) checkStmt.close();
            }
            catch(Exception f) 
            {
                f.printStackTrace();
            }
        }
	
		return VINStat.SUCCESS;
	}
	public int addJournalSinistre(SinistreObject obj)
	{
		ResultSet rs = null;
		Statement stmt = null;

		String reference   			= obj.getReference();
		String code_assureur   		= obj.getCodeAssureur();
		String type_dommage  		= obj.getTypeDommageDB();
		String cause_sinistre  		= obj.getCauseSinistreDB();
		String numero_attestation  	= obj.getNumeroAttestation();

		Timestamp sts = null;
		java.util.Date date_survenance  		= obj.getDateSurvenance();
		if (date_survenance != null) sts = new java.sql.Timestamp(date_survenance.getTime());

		Timestamp dts = null;
		java.util.Date date_declaration  		= obj.getDateDeclaration();
		if (date_declaration != null) dts = new java.sql.Timestamp(date_declaration.getTime());

		String insStrng = null;
		int retval			= VINStat.FAILURE;

		try 
		{
			String dtStrng = " VALUES (\'" + reference + 
                               "\', \'" + code_assureur + 
                               "\', \'" + type_dommage + 
                               "\', \'" + cause_sinistre + 
                               "\', \'" + numero_attestation + 
                               "\', \'" + sts + 
                               "\',\'" + dts + "\')";

			insStrng = "INSERT INTO journal_sinistre (reference, code_assureur, type_dommage, cause_sinistre, " + 
                                             		 "numero_attestation, date_survenance, date_declaration) " + dtStrng;

			stmt = ConnectionPool.getConnection().createStatement();
			stmt.executeUpdate(insStrng, Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			while (rs.next())
			{
				retval = rs.getInt(1);
			}
		}
		catch(Exception r)
		{
			LogMaster.logDebug("AsacDAOImpl:addJournalSinistre Exceptioned for : ["+reference+"]", r);
			return VINStat.FAILURE;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
                e.printStackTrace();
			}
		}

		if (retval != VINStat.FAILURE) obj.setSid(retval);
		return updateJournalSinistre(obj);
	}
	public int updateJournalSinistre(SinistreObject obj)
	{
		ResultSet rs = null;
		Statement stmt = null;

		int sid   					= obj.getSid();
		String reference   			= obj.getReference();
		String code_assureur   		= obj.getCodeAssureur();
		String type_dommage  		= obj.getTypeDommageDB();
		String cause_sinistre  		= obj.getCauseSinistreDB();
		String numero_attestation  	= obj.getNumeroAttestation();

		java.util.Date date_survenance  = obj.getDateSurvenance();
		java.util.Date date_declaration = obj.getDateDeclaration();

		String commentaires					= obj.getComments();
		int control_status				= obj.getControlStatus();
		java.util.Date date_creation    = obj.getDateCreation();
		java.util.Date date_mise_jour   = obj.getDateMiseJour();
		java.util.Date date_transfer    = obj.getDateTransfer();

		try
		{
			String nameCheck = "Select * from journal_sinistre where jsid = \'" + sid + "\' FOR  UPDATE";
        	stmt = myConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			rs   = stmt.executeQuery(nameCheck);
			if ((rs != null) && (rs.next() == true))
			{
				rs.last();
				rs.updateString("reference", reference);
				rs.updateString("code_assureur", code_assureur);
				rs.updateString("type_dommage", type_dommage);
				rs.updateString("cause_sinistre", cause_sinistre);
				rs.updateString("numero_attestation", numero_attestation);
				if (date_survenance != null)
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_survenance.getTime());
					rs.updateTimestamp("date_survenance", sts);
				}

				if (date_declaration != null)
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_declaration.getTime());
					rs.updateTimestamp("date_declaration", sts);
				}

				rs.updateString("commentaires", commentaires);
				rs.updateInt("c_status", control_status);

				if (date_creation != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_creation.getTime());
					rs.updateTimestamp("c_date_creation", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_creation", null);
				}

				if (date_mise_jour != null) 
				{
					java.sql.Timestamp sts = new java.sql.Timestamp(date_mise_jour.getTime());
					rs.updateTimestamp("c_date_mis_a_jour", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_mis_a_jour", null);
				}

				if (date_transfer != null) 
				{

					java.sql.Timestamp sts = new java.sql.Timestamp(date_transfer.getTime());
					rs.updateTimestamp("c_date_transfer", sts);
				}
				else
				{
					rs.updateTimestamp("c_date_transfer", null);
				}

				rs.updateRow();
            }
		}	
		catch (Exception ex) 
		{
			LogMaster.logDebug("AsacDAOImpl:updateJournalSinistre", ex);
			return VINStat.FAILURE;
		}
        finally
        {
            try
            {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            }
            catch(Exception f) {}
        }

		return VINStat.SUCCESS;
	}
	public List queryJournalSinistre(String queryString)
	{
		return this.queryJournalSinistre(queryString, myConn);
	}
	public List queryJournalSinistre(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;

        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);

			while (rs.next())
			{
				SinistreObject obj = new SinistreObject();
				obj.setSid(rs.getInt("jsid"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setReference(rs.getString("reference"));
				obj.setTypeDommage(rs.getString("type_dommage"));
				obj.setCauseSinistre(rs.getString("cause_sinistre"));
				obj.setNumeroAttestation(rs.getString("numero_attestation"));
				
				java.sql.Timestamp tim = rs.getTimestamp("date_survenance");
				if (tim != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(tim.getTime());
					obj.setDateSurvenance(cal.getTime());
				}

				java.sql.Date dt = rs.getDate("date_declaration");
				if (dt != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(dt.getTime());
					obj.setDateDeclaration(cal.getTime());
				}

				obj.setComments(rs.getString("commentaires"));
				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryJournalSinistres", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
	public double getQueryCount(String qstrng)
	{
		return this.getQueryCount(qstrng, myConn);
	}
	public double getQueryCount(String qstrng, Connection mConn)
	{
		ResultSet rs = null;
		Statement stmt = null;

		double resultValue = 0;

		try
		{
			stmt = mConn.createStatement();
			rs = stmt.executeQuery(qstrng);
			while(rs != null && rs.next())
			{
				resultValue = rs.getDouble(1);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("getQueryCount: exceptioned for ["+qstrng+"]", e);
		}
		finally
		{
			try
			{
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
			}
			catch(Exception e)
			{
			}
		}
		
		return resultValue;
	}
	public double getQueryCountOLD(String qstrng)
	{
		double retval = 0;
		HashMap totalSalesHash 	= new HashMap();

		try
		{
			QueryCountThread tst = new QueryCountThread(qstrng, totalSalesHash);
			tst.start();
			tst.join();

			Object returnObj = (Object)totalSalesHash.get(qstrng);	
			if (returnObj != null)
			{
				Double value = new Double(returnObj.toString());
				retval = value.doubleValue();
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:getQueryCount", e);
			return VINStat.FAILURE;
		}

		return retval;
	}
	public List queryAddress(String queryString)
	{
		return this.queryAddress(queryString, myConn);
	}
	public List queryAddress(String queryString, Connection mConn)
	{
		ResultSet rs = null;
		Statement pstmt = null;
        ArrayList arrList = new ArrayList();

		try
		{
			pstmt = mConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery(queryString);
			while (rs.next())
			{
				AddressObject obj = new AddressObject();
				obj.setAid(rs.getInt("aid"));
				obj.setCodeAssureur(rs.getString("code_assureur"));
				obj.setBoitePostale(rs.getString("boite_postale"));
				obj.setRue(rs.getString("rue"));
				obj.setVille(rs.getString("ville"));
				obj.setPays(rs.getString("pays"));
				obj.setTelephone(rs.getString("telephone"));
				obj.setEmail(rs.getString("email"));
				obj.setWeb(rs.getString("web"));

				obj.setControlStatus(rs.getInt("c_status"));

				java.sql.Timestamp sts = rs.getTimestamp("c_date_creation");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateCreation(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_mis_a_jour");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateMiseJour(cal.getTime());
				}

				sts = rs.getTimestamp("c_date_transfer");
				if (sts != null)
				{
					Calendar cal = new GregorianCalendar();
					cal.setTimeInMillis(sts.getTime());
					obj.setDateTransfer(cal.getTime());
				}

				obj.setComments(rs.getString("commentaires"));

                arrList.add(obj);
			}
		}
		catch(Exception e)
		{
			LogMaster.logDebug("AsacDAOImpl:queryAddress", e);
			return null;
		}
		finally
		{
			try
			{
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
			}
			catch(Exception f) {}
		}

		return arrList;	
	}
}
