package com.vindata.asac.dao;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;

import com.vindata.asac.model.*;

public interface AsacDAO 
{
	// client manipulation
	public int addAssure(AssureObject obj);	
	public int updateAssure(AssureObject oObj);	
	public List queryAssure(String queryString);	
	public int deleteAssure(int id);	

	public int addJournalAssure(AssureObject obj);	
	public int updateJournalAssure(AssureObject obj);	
	public List queryJournalAssure(String queryString);	

	// vehicule manipulation
	public int addVehicule(VehiculeObject obj);	
	public int updateVehicule(VehiculeObject oObj);	
	public List queryVehicule(String queryString);	
	public int deleteVehicule(int id);	
	public int addJournalVehicule(VehiculeObject obj);	
	public int updateJournalVehicule(VehiculeObject obj);	
	public List queryJournalVehicule(String queryString);	

	// remorque manipulation
	public int addRemorque(RemorqueObject obj);	
	public int updateRemorque(RemorqueObject oObj);	
	public List queryRemorque(String queryString);	
	public int deleteRemorque(int id);	
	public int addJournalRemorque(RemorqueObject obj);	
	public int updateJournalRemorque(RemorqueObject obj);	
	public List queryJournalRemorque(String queryString);	

	// attestation manipulation
	public int addAttestation(AttestationObject obj);	
	public int updateAttestation(AttestationObject oObj);	
	public List queryAttestation(String queryString);	
	public int deleteAttestation(int id);	
	public int addJournalAttestation(AttestationObject obj);	
	public int updateJournalAttestation(AttestationObject obj);	
	public List queryJournalAttestation(String queryString);	

	// intermediare manipulation
	public int addIntermediaire(IntermediaireObject obj);	
	public int updateIntermediaire(IntermediaireObject oObj);	
	public List queryIntermediaire(String queryString);	
	public int deleteIntermediaire(int id);	
	public int addJournalIntermediaire(IntermediaireObject obj);	
	public int updateJournalIntermediaire(IntermediaireObject obj);	
	public List queryJournalIntermediaire(String queryString);	

	// vente manipulation
	public int addVente(VenteObject obj);	
	public int updateVente(VenteObject oObj);	
	public List queryVente(String queryString);	
	public int deleteVente(int id);	
	public int addJournalVente(VenteObject obj);	
	public int updateJournalVente(VenteObject obj);	
	public List queryJournalVente(String queryString);	

	// sinistre manipulation
	public int addSinistre(SinistreObject obj);	
	public int updateSinistre(SinistreObject oObj);	
	public List querySinistre(String queryString);	
	public int deleteSinistre(int id);	
	public int addJournalSinistre(SinistreObject obj);	
	public int updateJournalSinistre(SinistreObject obj);	
	public List queryJournalSinistre(String queryString);	

	public double getQueryCount(String qstrng);	

	public List queryAddress(String queryString);	
}
