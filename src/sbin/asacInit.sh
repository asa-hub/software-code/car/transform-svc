#!/bin/sh

user=postgres
password=postgres
PGUSER=postgres
PGPASSWORD=postgres
PSQL_HOME=/usr/pgsql-14
dbname=hubasacint
dbloc=LOCAL
create_db=1

create_database()
{
	# create the database
	$PSQL_HOME/bin/createdb -h localhost -U ${user} $dbname 
	if [ $? -eq 0 ] 
	then
		echo "Create $dbname database succeeded \n"
	#	$PSQL_HOME/bin/psql -h localhost -U ${user}  << EOFGRANT
#GRANT ALL PRIVILEGES ON ${dbname}.* to user ${user};
#EOFGRANT
else
	echo "Could not create $dbname database .\n"
 	exit 1
fi
}

echo "SELECT NOW();" | $PSQL_HOME/bin/psql -h localhost -U ${user} $dbname >& /dev/null
if [ $? -eq 0 ] 
then
	# Database already exists... Do you wanna drop it ?
	echo ""
	echo "WARNING: [$dbname] database already exists !"
	echo "WARNING: Running $0 will erase your database."
	echo 
	echo "WARNING: If you had saved the database using vindataDump"
	echo "WARNING: you can reinitialize the database and later restore "
	echo "WARNING: using vindataRestore"
	echo -n "Do you want to initialize existing [$dbname] database ? [y/n] ";
	read answer
	if [ x"${answer}" = x"y" -o x"${answer}" = x"Y" ]
	then
		$PSQL_HOME/bin/dropdb -h localhost -U ${user} -e $dbname;
		if [  $? -ne 0 ] 
		then
			echo "Could not drop $dbname database.\n"
			exit 1
		fi
	elif [ x"${answer}" = x"n" -o x"${answer}" = x"N" ]
	then
		echo "Database is unchanged..."
		create_db=0
	else
		echo "Could not understand your answer, Enter [y/n] ! "
		exit 1
	fi
fi

if [ x"${create_db}" = x"1" ]
then
	create_database;

	$PSQL_HOME/bin/psql -h localhost -U ${user} $dbname < ${PWD}/db/hubasacDbCreateScript-v8.3.sql >& /tmp/dbproc.$$
	if [ ! $? -eq 0 ]
	then
		echo "Failed while creating the mysql Schema!!! Please see /tmp/dbproc.$$ "
    	exit 1
	fi 
fi

